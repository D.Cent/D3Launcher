/****************************************************************************
** Filename: unzip_p.h
** Last updated [dd/mm/yyyy]: 23/10/2006
**
** pkzip 2.0 decompression.
**
** Some of the code has been inspired by other open source projects,
** (mainly Info-Zip).
** Compression and decompression actually uses the zlib library.
**
** Copyright (C) 2006 Angius Fabrizio. All rights reserved.
**
** This file is part of the OSDaB project (http://osdab.sourceforge.net/).
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the file LICENSE.GPL that came with this software distribution or
** visit http://www.gnu.org/copyleft/gpl.html for GPL licensing information.
**
**********************************************************************/

#ifndef OSDAB_UNZIP_P__H
#define OSDAB_UNZIP_P__H

#include <map>
using namespace std;

#include <dirent.h>

#include <odf.h>
using namespace odf;

#include "unzip.h"


// zLib authors suggest using larger buffers (128K or 256K) for (de)compression (especially for inflate())
// we use a 256K buffer here - if you want to use this code on a pre-iceage mainframe please change it ;)
#define UNZIP_READ_BUFFER 262144

class UnzipPrivate
{

public:
	UnzipPrivate();
	virtual ~UnzipPrivate();

	// Replace this with whatever else you use to store/retrieve the password.
	string password;

	bool skipAllEncrypted;

	map<string,ZipEntry>* headers;

	FILE* device;

	char buffer1[UNZIP_READ_BUFFER];
	char buffer2[UNZIP_READ_BUFFER];

	unsigned char* uBuffer;

	const uint4* crcTable;

	UnZip::ErrorCode openArchive(FILE* device);

	bool parseLocalHeader();
	bool parseCentralDirectory();
	bool parseCentralDirectoryEnd();
	uint4 getNextSignature(bool* ok);

	UnZip::ErrorCode closeArchive();

	UnZip::ErrorCode extractFile(map<string,ZipEntry>::iterator& itr, const char* dir);
	UnZip::ErrorCode extractFile(map<string,ZipEntry>::iterator& itr, FILE* device);

	UnZip::ErrorCode testPassword(uint4* keys, const string& file, const ZipEntry& header);
	bool testKeys(const ZipEntry& header, uint4* keys);

	bool createDirectory(const char* path);

	inline void decryptBytes(uint4* keys, char* buffer, sint8 read);

	inline uint4 getULong(const unsigned char* data, uint4 offset) const;
	inline uint8 getULLong(const unsigned char* data, uint4 offset) const;
	inline uint2 getUShort(const unsigned char* data, uint4 offset) const;
	inline int decryptByte(uint4 key2) const;
	inline void updateKeys(uint4* keys, int c) const;
	inline void initKeys(const string& pwd, uint4* keys) const;
};

#endif // OSDAB_UNZIP_P__H
