/****************************************************************************
** Filename: unzip.h
** Last updated [dd/mm/yyyy]: 23/10/2006
**
** pkzip 2.0 decompression.
**
** Some of the code has been inspired by other open source projects,
** (mainly Info-Zip).
** Compression and decompression actually uses the zlib library.
**
** Copyright (C) 2006 Angius Fabrizio. All rights reserved.
**
** This file is part of the OSDaB project (http://osdab.sourceforge.net/).
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the file LICENSE.GPL that came with this software distribution or
** visit http://www.gnu.org/copyleft/gpl.html for GPL licensing information.
**
**********************************************************************/

#ifndef OSDAB_UNZIP__H
#define OSDAB_UNZIP__H

#include "zipentry.h"

#include <vector>
#include <string>
using namespace std;

/*#include <qglobal.h>
#include <QObject>
#include <qmap.h>
*/

#include <dirent.h>

#include <zlib.h>

#include <odf.h>
using namespace odf;

class UnzipPrivate;
//class QIODevice;
//class QFile;
//class QDir;
//class QStringList;
//class QString;


class UnZip
{

public:
	enum ErrorCode
	{
		Ok,
		ZlibInit,
		ZlibError,
		OpenFailed,
		EndCorrupted,
		Corrupted,
		WrongPassword,
		NoOpenArchive,
		NoSuchFile,
		ReadFailed,
		WriteFailed,
		SeekFailed,
		CreateDirFailed,
		InvalidDevice,

		Skip, SkipAll // internal use only
	};

	UnZip();
	virtual ~UnZip();

	bool isOpen() const;

	ErrorCode openArchive(const char* filename);
	ErrorCode openArchive(FILE* device);
	ErrorCode closeArchive();

	bool contains(const string& file) const;

	vector<string> getFileList() const;

	ErrorCode extractAll(const char* dirname);
	ErrorCode extractAll(const DIR* dir);

	ErrorCode extractFile(const string& filename, const char* dirname);
	ErrorCode extractFile(const string& filename, const string& dir);
	ErrorCode extractFile(const string& filename, FILE* device);

	ErrorCode extractFiles(const vector<string>& filenames, const char* dirname);
	ErrorCode extractFiles(const vector<string>& filenames, const string& dir);

	void setPassword(const string& pwd);

private:
	UnzipPrivate* d;
};

#endif // OSDAB_UNZIP__H
