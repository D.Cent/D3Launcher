/****************************************************************************
** Filename: zipentry.h
** Last updated [dd/mm/yyyy]: 30/09/2006
**
** Wrapper for a ZIP local header.
**
** Some of the code has been inspired by other open source projects,
** (mainly Info-Zip).
** Compression and decompression actually uses the zlib library.
**
** Copyright (C) 2006 Angius Fabrizio. All rights reserved.
**
** This file is part of the OSDaB project (http://osdab.sourceforge.net/).
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the file LICENSE.GPL that came with this software distribution or
** visit http://www.gnu.org/copyleft/gpl.html for GPL licensing information.
**
**********************************************************************/

#ifndef OSDAB_ZIPENTRY__H
#define OSDAB_ZIPENTRY__H

#include <odf.h>
using namespace odf;

class ZipEntry
{
public:
	ZipEntry() {};

	uint4 zoffset; // offset (relative to zip file) for compressed data
	unsigned char gpFlag[2]; // general purpose flag (2 bytes)
	uint2 compMethod; // compression method (2 bytes)
	unsigned char modTime[2]; // last modified time (2 bytes)
	unsigned char modDate[2]; // last modified date (2 bytes)
	uint4 crc; // 32 bit CRC (4 bytes)
	uint4 szComp; // compressed file size (4 bytes)
	uint4 szUncomp; // uncompressed file size (4 bytes)
	unsigned char extraLen[2]; // extra field length (2 bytes)
};

#endif // OSDAB_ZIPENTRY__H
