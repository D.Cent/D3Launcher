/****************************************************************************
** Filename: unzip.cpp
** Last updated [dd/mm/yyyy]: 23/10/2006
**
** pkzip 2.0 decompression.
**
** Some of the code has been inspired by other open source projects,
** (mainly Info-Zip).
** Compression and decompression actually uses the zlib library.
**
** Copyright (C) 2006 Angius Fabrizio. All rights reserved.
**
** This file is part of the OSDaB project (http://osdab.sourceforge.net/).
**
** This file may be distributed and/or modified under the terms of the
** GNU General Public License version 2 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** See the file LICENSE.GPL that came with this software distribution or
** visit http://www.gnu.org/copyleft/gpl.html for GPL licensing information.
**
**********************************************************************/

#include "unzip.h"
#include "unzip_p.h"
#include "zipentry.h"

/*#include <qstring.h>
#include <qstringlist.h>
#include <qdir.h>
#include <qfile.h> */

#include <map>
#include <cassert>// line 358, assert()
using namespace std;

#include <dirent.h>

// You can remove this #include if you replace the qDebug() statements.
// #include <qdebug.h>

/*!
	\class UnZip unzip.h

	\brief Zip file decompression.
*/

/*! \enum UnZip::ErrorCode The result of a decompression operation.
	\value UnZip::Ok No error occurred.
	\value UnZip::ZlibInit Failed to init or load the zlib library.
	\value UnZip::ZlibError The zlib library returned some error.
	\value UnZip::OpenFailed Unable to create or open a device.
	\value UnZip::EndCorrupted Corrupted zip archive - some files could be extracted.
	\value UnZip::Corrupted Corrupted or invalid zip archive.
	\value UnZip::WrongPassword Unable to decrypt a password protected file.
	\value UnZip::NoOpenArchive No archive has been opened yet.
	\value UnZip::NoSuchFile Unable to find the requested file in the archive.
	\value UnZip::ReadFailed Reading of a file failed.
	\value UnZip::WriteFailed Writing of a file failed.
	\value UnZip::SeekFailed Seek failed.
	\value UnZip::CreateDirFailed Could not create a directory.
	\value UnZip::InvalidDevice A null device has been passed as parameter.
	\value UnZip::Skip Internal use only.
	\value UnZip::SkipAll Internal use only.
*/


//! Fixed size header fields (excluding signature)
#define UNZIP_H_FIXSIZE 26
//! Fixed size central header fields (excluding signature)
#define UNZIP_CH_FIXSIZE 42
//! Fixed size central directory end fields (excluding signature)
#define UNZIP_CEND_FIXSIZE 0
//! Data descriptor size
#define UNZIP_H_DD_SIZE 16
/*!
	Max version handled by this API.
	0x1B = 2.7 --> full compatibility only up to version 2.0 (0x14)
	versions from 2.1 to 2.7 may use unsupported compression methods
	versions after 2.7 may have an incompatible header format
*/
#define UNZIP_VERSION 0x1B
//! Full compatibility granted until this version
#define UNZIP_VERSION_STRICT 0x14

//! CRC32 routine
#define CRC32(c, b) crcTable[((int)c^b) & 0xff] ^ (c >> 8)

//! Checks if some file has been already extracted.
#define UNZIP_CHECK_FOR_VALID_DATA \
	{\
		if (headers != 0)\
		{\
			/*qDebug() << tr("Corrupted zip archive");*/\
			puts("Corrupted zip archive");\
			return UnZip::EndCorrupted;\
		}\
		else\
		{\
			delete device;\
			device = 0;\
			/*qDebug() << tr("Corrupted or invalid zip archive");*/\
			puts("Corrupted or invalid zip archive");\
			return UnZip::Corrupted;\
		}\
	}


/************************************************************************
 Public interface
*************************************************************************/

/*!
	Creates a new decompressor.
*/
UnZip::UnZip()
{
	d = new UnzipPrivate();
}

/*!
	Closes any open archive and releases any used resources.
*/
UnZip::~UnZip()
{
	delete d;
}

/*!
	Returns true if there is an open archive.
*/
bool UnZip::isOpen() const
{
	return d->device != 0;
}

/*!
	Opens a zip archive and reads the entries list. Closes any previously opened archive.
*/
UnZip::ErrorCode UnZip::openArchive(const char* filename)
{
	FILE* file = NULL;
	
	if (!t_access(filename, F_OK))
	 return UnZip::NoSuchFile;
	
	file = t_fopen(filename, "rb");
	if (!file)
	 return UnZip::OpenFailed;

	return openArchive(file);
	t_fclose(file);
}

/*!
	Opens a zip archive and reads the entries list.
	Closes any previously opened archive.
	\warning The class takes ownership of the device!
*/
UnZip::ErrorCode UnZip::openArchive(FILE* file)
{
	if (file == NULL)
	 return UnZip::OpenFailed;

	return d->openArchive(file);
}

/*!
	Closes the archive and releases all the used resources (like cached passwords).
*/
UnZip::ErrorCode UnZip::closeArchive()
{
	return d->closeArchive();
}

/*!
	Returns true if the archive contains a file with the given path and name.
*/
bool UnZip::contains(const string& file) const
{
	if (d->headers == 0)
		return false;

	return (d->headers->find(file)!=d->headers->end()); // FIXME does it work??
}

/*!
	Returns the paths of the files in this archive.
*/

vector<string> UnZip::getFileList() const
{
	vector<string> files;

	if (d->headers == 0)
		return files;

	for (map<string,ZipEntry>::iterator itr = d->headers->begin(); itr != d->headers->end(); ++itr)
		files.push_back(itr->first);
		//files.append(itr.key());

	return files;
}

/*!
	Extracts the whole archive to a directory.
*/

/* UnZip::ErrorCode UnZip::extractAll(const char* dirname)
{
	DIR* d;
	d=opendir(dirname);
	UnZip::ErrorCode e = extractAll(d);
	closedir(d);
	return e;	

} */

/*!
	Extracts the whole archive to a directory.
*/
UnZip::ErrorCode UnZip::extractAll(const char* dir)
{
	// this should only happen if we didn't call openArchive() yet
	if (d->device == NULL)
		return NoOpenArchive;

	if (d->headers == NULL)
		return Ok;

	for (map<string,ZipEntry>::iterator itr = d->headers->begin(); itr != d->headers->end(); ++itr)
	{
		if ((itr->second.gpFlag[0] & 1) && d->skipAllEncrypted)
			continue;

		switch (d->extractFile(itr, dir))
		{
		case Corrupted:
				d->headers->erase(itr);
			break;
		case CreateDirFailed:

			break;
		case Skip:
			break;
		case SkipAll:
			d->skipAllEncrypted = true;
			break;
		default:
			;
		}
	}

	return Ok;
}
// TODO ?
/*!
	Extracts a single file to a directory.
*/
UnZip::ErrorCode UnZip::extractFile(const string& filename, const string& dirname)
{
	return extractFile(filename, dirname.c_str());
}
// TODO ?
/*!
	Extracts a single file to a directory.
*/
UnZip::ErrorCode UnZip::extractFile(const string& filename, const char* dir)
{
	map<string,ZipEntry>::iterator itr = d->headers->find(filename);
	if (itr != d->headers->end())
		return d->extractFile(itr, dir);

	return NoSuchFile;
}

/*!
	Extracts a single file to a directory.
*/
UnZip::ErrorCode UnZip::extractFile(const string& filename, FILE* dev)
{
	if (dev == 0)
		return InvalidDevice;

	map<string,ZipEntry>::iterator itr = d->headers->find(filename);
		if (itr != d->headers->end())
			return d->extractFile(itr, dev);

	return NoSuchFile;
}

/*!
	Extracts a list of files.
	Stops extraction at first error (continues if a file does not exist in archive).
 */
UnZip::ErrorCode UnZip::extractFiles(const vector<string>& filenames, const char* dirname)
{
	ErrorCode ec;

	for (vector<string>::const_iterator itr = filenames.begin(); itr != filenames.end(); ++itr)
	{
		ec = extractFile(*itr, dirname);
		if (ec == NoSuchFile)
			continue;
		if (ec != Ok)
			return ec;
	}

	return Ok;
}

/*!
	Extracts a list of files.
	Stops extraction at first error (continues if a file does not exist in archive).
 */
UnZip::ErrorCode UnZip::extractFiles(const vector<string>& filenames, const string& dir)
{
	return extractFiles(filenames, dir.c_str());
}

/*!
	Remove/replace this method to add your own password retrieval routine.
*/
void UnZip::setPassword(const string& pwd)
{
	d->password = pwd;
}


/************************************************************************
 Private interface
*************************************************************************/

//! \internal
UnzipPrivate::UnzipPrivate():
skipAllEncrypted(false), headers(0), device(0)
{
	uBuffer = (unsigned char*) buffer1;
	crcTable = (uint4*) get_crc_table();
}

//! \internal
UnzipPrivate::~UnzipPrivate()
{
	if (device != 0)
		closeArchive();
}

//! \internal
UnZip::ErrorCode UnzipPrivate::openArchive(FILE* dev)
{
try{
	assert(dev != 0);

	if (device != 0)
		closeArchive();

	device = dev;

	if (device==NULL)
	{
		return UnZip::OpenFailed;
	}

	bool centralDirEndFound = false;

	while (!centralDirEndFound)
	{
		t_fread(buffer1, 1, 4, dev);

		if (buffer1[0] != 'P' || buffer1[1] != 'K')
			UNZIP_CHECK_FOR_VALID_DATA

		/* PKZIP signatures:
			PK\3\4 local file header
			PK\6\8 archive extra data record
			PK\1\2 central file header
			PK\5\5 digital signature header
			PK\6\6 ZIP64 end of central directory record
			PK\6\7 ZIP64 end of central directory locator
			PK\5\6 end of central directory record
		 */

		bool signatureMatch = true;

		switch (buffer1[2])
		{
		case 0x01:
			{
				if (buffer1[3] == 0x02)
				{
					if (!parseCentralDirectory())
						UNZIP_CHECK_FOR_VALID_DATA
				}
				else signatureMatch = false;
				break;
			}
		case 0x03:
			{
				if (buffer1[3] == 0x04)
				{
					if (!parseLocalHeader())
						UNZIP_CHECK_FOR_VALID_DATA
				}
				else signatureMatch = false;
				break;
			}
		case 0x05:
			{
				if (buffer1[3] == 0x06)
				{
					if (!parseCentralDirectoryEnd())
						UNZIP_CHECK_FOR_VALID_DATA

					centralDirEndFound = true;
					break;
				}
			}
		case 0x06:
			{
				switch (buffer1[3])
				{
				case 0x06:
					{
						// skip ZIP64 end of central directory record
						t_fseek(device,(long int)getULLong(uBuffer, 4),SEEK_CUR);
						break;
					}
				case 0x07:
					{
						// skip ZIP64 end of central directory locator
						t_fseek(device,16,SEEK_CUR);
						break;
					}
				default: signatureMatch = false;
				}
			}
		default:
			;
		}

		if (!signatureMatch)
		{
			//qDebug() << tr("Corrupted zip archive");
			puts("Corrupted zip archive");
			return UnZip::Corrupted;
		}
	}
} // try
catch(...) {
	UNZIP_CHECK_FOR_VALID_DATA
}

	return UnZip::Ok;
}

//! \internal
bool UnzipPrivate::parseLocalHeader()
{
	//if (device->read(buffer1, UNZIP_H_FIXSIZE) != UNZIP_H_FIXSIZE)
	//	return false;
	t_fread(buffer1,1,UNZIP_H_FIXSIZE,device);

	ZipEntry* h = 0;
	string* name = 0;
	uint4 offset = 0;

	uint2 compMethod = getUShort(uBuffer, 4);

	bool skipEntry = false;

	// header parsing may be a problem if version is bigger than UNZIP_VERSION
	if (buffer1[0] <= UNZIP_VERSION)
	{
		h = new ZipEntry();

		h->compMethod = compMethod;

		h->gpFlag[0] = buffer1[2];
		h->gpFlag[1] = buffer1[3];

		h->modTime[0] = buffer1[6];
		h->modTime[1] = buffer1[7];

		h->modDate[0] = buffer1[8];
		h->modDate[1] = buffer1[9];

		h->crc = getULong(uBuffer, 10);
		h->szComp = getULong(uBuffer, 14);
		h->szUncomp = getULong(uBuffer, 18);
	}
	else puts("Unsupported ZIP file version. Attempting to find next header.");

	if (buffer1[0] > UNZIP_VERSION_STRICT)
		puts("This ZIP file version is not fully supported.");

 	if ((compMethod != 0) && (compMethod != 8))
 	{
 		puts("Unsupported compression method. Skipping file.");
 		skipEntry = true;
	}

	// filename length
	unsigned int len = (unsigned int)uBuffer[22] | ((unsigned int)uBuffer[23] << 8);

	if (len == 0)
	{
		delete h;
		h = 0;
	}
	else
	{
		//if ((unsigned int)device->read(buffer2, len) != len)
		if((unsigned int)fread(buffer2,1,len,device) != len) // TODO: t_f...
		{
			delete h;
			return false;
		}
		//name = new QString(QString::fromAscii(buffer2, len));
		//name = new char[len];
		//strcpy(name,buffer2,len);
		name = new string(buffer2,0,len);
	}

	// extra field length
	len = (unsigned int)uBuffer[24] | ((unsigned int)uBuffer[25] << 8);

	// skip extra field (if present)
	if(fseek(device, len, SEEK_CUR) == -1)
	{
		delete h;
		delete name;
		return false;
	}

	if ((h != 0) && (name != 0))
	{
		h->zoffset = ftell(device);
		if ( ((h->gpFlag[0] & 0x08) == 8) && (h->szComp == 0) )
		{
			// this was a non-seekable stream
			// we need to skip the compressed file and read the data descriptor
			bool ok;
			h->szComp = getNextSignature(&ok);
			if (!ok)
			{
				delete h;
				delete name;
				return false;
			}
		}

		offset = h->zoffset + h->szComp;

		if (fseek(device,offset,SEEK_SET)==-1)
		{
			delete h;
			delete name;
			return false;
		}

		if ((h->gpFlag[0] & 0x08) == 8)
		{
			// read data descriptor
			if ( fread(device, 1, UNZIP_H_DD_SIZE, device) != UNZIP_H_DD_SIZE)
			{
				delete h;
				delete name;
				return false;
			}

			// check signature: PK\7\8
			if ((buffer1[0] != 'P') || (buffer1[1] != 'K') || (buffer1[2] != 0x07) || (buffer1[3] != 0x08))
			{
				delete h;
				delete name;
				return false;
			}

			h->crc = getULong(uBuffer, 4);
			h->szComp = getULong(uBuffer, 8);
			h->szUncomp = getULong(uBuffer, 12);
		}
	}

	if (name != 0)
	{
		if ((h != 0) && (!skipEntry))
		{
			if (headers == 0)
				headers = new map<string, ZipEntry>();
			headers->insert(pair<string, ZipEntry>(*name, *h));
		}
	}

	delete h;
	delete name;

	return true;
}

/*!
	Attempts to seek to the next signature.
*/
uint4 UnzipPrivate::getNextSignature(bool* ok)
{
	/*
	 * We need to find the data descriptor signature PK\7\8
	 * and this may be really slow :)
	 */

	int c;
	char expected = 'P';
	uint4 count = 0; //quint4

	while ((c=getc(device))!=EOF)
	{
		count++;

		if (feof(device)!=0)
		{
			*ok = false;
			return 0;
		}

		switch(c)
		{
		case 'P':
			expected = 'K'; // start of evtl. signature
			break;
		case 'K':
			if (expected == 'K')
				expected = 7;
			else expected = 'P'; // reset
			break;
		case 7:
			if (expected == 7)
				expected = 8;
			else expected = 'P'; // reset
			break;
		case 8:
			if (expected == 8)
			{
				// dd found!!
				*ok = (fseek(device,12,SEEK_CUR)!=-1);
				return count-4;
			}
			else expected = 'P'; // reset
			break;
		default:
			expected = 'P'; // reset
		}

	}

	*ok = false;
	return count;
}

/*!
	Parses a central directory entry.
*/
bool UnzipPrivate::parseCentralDirectory()
{
unsigned int len;
try{
	//if ( fread(device, 1, buffer1, UNZIP_CH_FIXSIZE) != UNZIP_CH_FIXSIZE )
	t_fread(buffer1, 1, UNZIP_CH_FIXSIZE, device);

	// first get filename, so we can check if the local header for this file has been found
	// and if it can be extracted

	string* name = 0;

	len = (unsigned int)uBuffer[24] | ((unsigned int)uBuffer[25] << 8);

	if (len == 0)
	{
		// skip this entry (need to skip extra field and comment)
		len = (unsigned int)uBuffer[26] | ((unsigned int)uBuffer[27] << 8);
		len += (unsigned int)uBuffer[28] | ((unsigned int)uBuffer[29] << 8);
		t_fseek(device,len,SEEK_CUR);
	}

	t_fread(buffer2, 1, len, device);
	name = new string(buffer2, 0, len);

	ZipEntry* header = 0;
	map<string,ZipEntry>::iterator itr;

	if (headers != 0)
	{
		itr = headers->find(*name);
		if (itr != headers->end())
			header = &(itr->second); //TODO: works?
			//header = &itr.value();
	}

	if (header == 0)
	{
		// skip this entry (need to skip extra field and comment)
		len = (unsigned int)uBuffer[26] | ((unsigned int)uBuffer[27] << 8);
		len += (unsigned int)uBuffer[28] | ((unsigned int)uBuffer[29] << 8);
		//if (!device->seek(device->pos()+len))
		if(fseek(device,len,SEEK_CUR)==-1)
		{
			delete name;
			return false;
		}
		return true;
	}

	// some minimal redundancy checks here

	if (
		// compression method
		(header->compMethod != getUShort(uBuffer, 6)) ||
		// crc
		(header->crc != getULong(uBuffer, 12)) ||
		// compressed size
		(header->szComp != getULong(uBuffer, 16)) ||
		// uncompressed size
		(header->szUncomp != getULong(uBuffer, 20))
		)
	{
		headers->erase(itr);
		//qDebug() << tr("Corrupted entry in: %1").arg(*name); // TODO: tr ? %l ?
		puts("Corrupted entry!");
	}

	// skip extra field and comment
	len = (unsigned int)uBuffer[26] | ((unsigned int)uBuffer[27] << 8);
	len += (unsigned int)uBuffer[28] | ((unsigned int)uBuffer[29] << 8);

	delete name;
}
catch(...) {
	return false;
}
	return (fseek(device,len,SEEK_CUR)!=-1);
}

/*!
	The central directory end is not parsed in this version as this is not
	of much usefulness. Always returns true.
*/
bool UnzipPrivate::parseCentralDirectoryEnd()
{
	/*	End of central directory:

		end of central dir signature    4 bytes  (0x06054b50)
		number of this disk             2 bytes
		number of the disk with the
		start of the central directory  2 bytes
		total number of entries in the
		central directory on this disk  2 bytes
		total number of entries in
		the central directory           2 bytes
		size of the central directory   4 bytes
		offset of start of central
		directory with respect to
		the starting disk number        4 bytes
		.ZIP file comment length        2 bytes
		.ZIP file comment       (variable size)
	 */

	// not used in this version
	// we could read the entry to see if the file is not corrupted at the end, but ...
	// ... who cares? :)
	return true;
}

//! \internal
UnZip::ErrorCode UnzipPrivate::closeArchive()
{
	if (device == 0)
		return UnZip::Ok;

	fclose(device); device = NULL;

	skipAllEncrypted = false;

	return UnZip::Ok;
}

//! \internal
UnZip::ErrorCode UnzipPrivate::extractFile(map<string,ZipEntry>::iterator& itr, const char* dir)
{
	string name(itr->first);

	string directory;
	
//	printf("name=%s\n",name.c_str());
	unsigned int pos = name.find_last_of('/');
	if(pos == name.npos) pos = 0; // if no / was found
	
	
	
	string dirname(name,0,pos); // copy dir part from name

	if (pos == (unsigned int)name.length()-1)
	{
        	directory = dir;//.arg(QDir::cleanPath(name)); // TODO;
		directory += '/' + name; // TODO: ACCESS()
//		printf("a mkdir: %s\n",directory.c_str());
#ifdef __LINUX__
		if (mkdir(directory.c_str(), 0777)==-1)
#else
		if (mkdir(directory.c_str())==-1)
#endif
		{
			//qDebug() << tr("Unable to create 1 directory: %1").arg(directory);
			puts("Unable to create 1 directory"); // TODO: put name of dir
			return UnZip::CreateDirFailed;
		}
		return UnZip::Ok;
	}
	else {
		directory = dir;//.arg(QDir::cleanPath(name)); // TODO;
		directory.push_back('/'); // TODO: ACCESS()
	}
	/*if (pos > 0)
	{
		// get directory part
		// dirname = name.left(pos);
        	//directory = QString("%1/%2").arg(dir.absolutePath()).arg(QDir::cleanPath(dirname));
		directory = dir;
		directory += '/' + dirname; // cleanpath not used
		printf("bmkdir: %s\n",directory.c_str());
		if (mkdir(directory.c_str(), 0777)==-1)
		{
			//qDebug() << tr("Unable to create directory: %1").arg(directory);
			puts("Unable to create 1 directory"); // TODO
			return UnZip::CreateDirFailed;
		}
		//name = name.right(name.length()-pos-1);
		name = name.substr(name.length()-pos-1);
	}*/

	//name = QString("%1").arg(directory);
	
	name = directory+name;
	
	FILE* outFile;
//	printf("try opening %s...\n",name.c_str());
	outFile = fopen(name.c_str(), "wb");
	if( outFile == NULL )
	{
		// qDebug() << tr("Unable to open %1 for writing").arg(name);
		puts("Unable to open file for writing"); // TODO
		return UnZip::OpenFailed;
	}


	UnZip::ErrorCode ec = extractFile(itr, outFile);

	t_fclose(outFile);

// TODO
/*	if (ec != UnZip::Ok)
	{
		if (!outFile.remove())
			qDebug() << tr("Unable to remove corrupted file: %1").arg(name);
	}*/

	return ec;
}

//! \internal
UnZip::ErrorCode UnzipPrivate::extractFile(map<string,ZipEntry>::iterator& itr, FILE* dev)
{
	assert(dev != NULL);

	ZipEntry& header = itr->second;
	if (fseek(device, header.zoffset, SEEK_SET)==-1)
	{
		puts("Error while reading device");
		return UnZip::SeekFailed;
	}

	string name(itr->first);

	// encryption keys
	uint4 keys[3];

	if (header.gpFlag[0] & 1)
	{
		UnZip::ErrorCode e = testPassword(keys, name, header);
		if (e != UnZip::Ok)
		{
			// qDebug() << tr("Unable to decrypt %1").arg(name);
			puts("Unable to decrypt a file"); // TODO
			return e;
		}
		header.szComp -= 12; // remove encryption header size
	}

	if (header.szComp == 0)
	{
		if (header.crc != 0)
		{
			return UnZip::Corrupted;
		}

		return UnZip::Ok;
	}

	uInt rep = header.szComp / UNZIP_READ_BUFFER;
	uInt rem = header.szComp % UNZIP_READ_BUFFER;
	uInt cur = 0;

	// extract data
	uint8 read;
	uint8 tot = 0;

	uint4 myCRC = crc32(0L, Z_NULL, 0);

	if (header.compMethod == 0)
	{
		//while ( (read = device->read(buffer1, cur < rep ? UNZIP_READ_BUFFER : rem)) > 0 )
		while ( (read = fread(buffer1, 1, cur < rep ? UNZIP_READ_BUFFER : rem,device)) > 0 )
		{
			if (header.gpFlag[0] & 1)
				decryptBytes(keys, buffer1, read);

			myCRC = crc32(myCRC, uBuffer, read);

			if (fwrite(buffer1, 1, read, dev) != read)
			{
				//qDebug() << tr("Error while writing %1").arg(name);
				puts("Error while writing a file"); // TODO
				return UnZip::WriteFailed;
			}

			cur++;
			tot += read;

			if (tot == header.szComp)
				break;
		}

		if (read < 0)
		{
			puts("Error while reading device");
			return UnZip::ReadFailed;
		}
	}
	else if (header.compMethod == 8)
	{
		// allocate inflate state
		z_stream zstr;
		zstr.zalloc = Z_NULL;
		zstr.zfree = Z_NULL;
		zstr.opaque = Z_NULL;
		zstr.next_in = Z_NULL;
		zstr.avail_in = 0;

		int zret;

		// use inflateInit2 with negative windowBits to get raw decompression
		if ( (zret = inflateInit2_(&zstr, -MAX_WBITS, ZLIB_VERSION, sizeof(z_stream))) != Z_OK )
		{
			puts("Could not initialize zlib for decompression");
			return UnZip::ZlibError;
		}

		int szDecomp;

		// decompress until deflate stream ends or end of file
		do
		{
			read = fread(buffer1, 1, cur < rep ? UNZIP_READ_BUFFER : rem, device);
			if (read == 0)
				break;
			if (read < 0)
			{
				puts("Error while reading device");
				(void)inflateEnd(&zstr);
				return UnZip::ReadFailed;
			}

			if (header.gpFlag[0] & 1)
				decryptBytes(keys, buffer1, read);

			cur++;
			tot += read;

			zstr.avail_in = (uInt)read;
			zstr.next_in = (Bytef*) buffer1;


			// run inflate() on input until output buffer not full
			do {
				zstr.avail_out = UNZIP_READ_BUFFER;
				zstr.next_out = (Bytef*) buffer2;;

				zret = inflate(&zstr, Z_NO_FLUSH);

				switch (zret) {
					case Z_NEED_DICT:
					case Z_DATA_ERROR:
					case Z_MEM_ERROR:
						inflateEnd(&zstr);
						return UnZip::WriteFailed;
					default:
						;
				}

				szDecomp = UNZIP_READ_BUFFER - zstr.avail_out;
				if ((signed int)fwrite(buffer2, 1, szDecomp, dev) != szDecomp)
				{
					inflateEnd(&zstr);
					return UnZip::ZlibError;
				}

				myCRC = crc32(myCRC, (const Bytef*) buffer2, szDecomp);

			} while (zstr.avail_out == 0);

		}
		while (zret != Z_STREAM_END);

		inflateEnd(&zstr);
	}

	if (myCRC != header.crc)
	{
		return UnZip::Corrupted;
	}

	return UnZip::Ok;
}

//! \internal Creates a new directory and all the needed parent directories.
bool UnzipPrivate::createDirectory(const char* path)
{
	/* QDir d(path);
	if (!d.exists())
	{
		int sep = path.lastIndexOf("/");
		if (sep <= 0) return true;

		if (!createDirectory(path.left(sep)))
			return false;

//		if (!d.mkdir(path))
//		{
//			qDebug() << tr("Unable to create directory: %1").arg(path);
//			return false;
//		}

	} */
	if(!t_access("",F_OK))
#ifdef __LINUX__
	 if(mkdir(path, 0777)==-1)
#else
	 if(mkdir(path)==-1)
#endif
	  return false;
	return true;
}

//TODO!

/*!
	Reads an quint4 (4 bytes) from a byte array starting at given offset.
*/
uint4 UnzipPrivate::getULong(const unsigned char* data, uint4 offset) const
{
	uint4 res = (uint4) data[offset];
	res |= (((uint4)data[offset+1]) << 8);
	res |= (((uint4)data[offset+2]) << 16);
	res |= (((uint4)data[offset+3]) << 24);

	return res;
}

/*!
	Reads an quint8 (8 bytes) from a byte array starting at given offset.
*/
uint8 UnzipPrivate::getULLong(const unsigned char* data, uint4 offset) const
{
	uint8 res = (uint8) data[offset];
	//res |= (((quint64)data[offset+1]) << 8);
	res |= (((uint8)data[offset+1]) << 8);
	res |= (((uint8)data[offset+2]) << 16);
	res |= (((uint8)data[offset+3]) << 24);
	res |= (((uint8)data[offset+1]) << 32);
	res |= (((uint8)data[offset+2]) << 40);
	res |= (((uint8)data[offset+3]) << 48);
	res |= (((uint8)data[offset+3]) << 56);

	return res;
}

/*!
	Reads an quint2 (2 bytes) from a byte array starting at given offset.
*/
uint2 UnzipPrivate::getUShort(const unsigned char* data, uint4 offset) const
{
	return (uint2) data[offset] | (((uint2)data[offset+1]) << 8);
}

/*!
	Return the next byte in the pseudo-random sequence
 */
int UnzipPrivate::decryptByte(uint4 key2) const
{
	uint2 temp = ((uint2)(key2) & 0xffff) | 2;
	return (int)(((temp * (temp ^ 1)) >> 8) & 0xff);
}

/*!
	Update the encryption keys with the next byte of plain text
 */
void UnzipPrivate::updateKeys(uint4* keys, int c) const
{
	keys[0] = CRC32(keys[0], c);
	keys[1] += keys[0] & 0xff;
	keys[1] = keys[1] * 134775813L + 1;
	keys[2] = CRC32(keys[2], ((int)keys[1]) >> 24);
}

/*!
	Initialize the encryption keys and the random header according to
	the given password.
 */
void UnzipPrivate::initKeys(const string& pwd, uint4* keys) const
{
	keys[0] = 305419896L;
	keys[1] = 591751049L;
	keys[2] = 878082192L;

	/* QByteArray pwdBytes = pwd.toAscii();
	int sz = pwdBytes.size();
	const char* ascii = pwdBytes.data();*/
	int sz = pwd.size();
	

	for (int i=0; i<sz; ++i)
		updateKeys(keys, (int)pwd[i]);
		//updateKeys(keys, (int)ascii[i]);
}

/*!
	Attempts to test a password without actually extracting a file.
	The \p file parameter can be used in the user interface or for debugging purposes
	as it is the name of the encrypted file for wich the password is being tested.
*/
UnZip::ErrorCode UnzipPrivate::testPassword(uint4* keys, const string& file, const ZipEntry& header)
{
	// Q_UNUSED(file);

	// read encryption keys
	if (fread(buffer1, 1, 12, device) != 12)
		return UnZip::Corrupted;

	// Replace this code if you want to i.e. call some dialog and ask the user for a password
	initKeys(password, keys);
	if (testKeys(header, keys))
		return UnZip::Ok;

	return UnZip::Skip;
}

/*!
	Tests a set of keys on the encryption header.
*/
bool UnzipPrivate::testKeys(const ZipEntry& header, uint4* keys)
{
	char lastByte;

	// decrypt encryption header
	for (int i=0; i<11; ++i)
		updateKeys(keys, lastByte = buffer1[i] ^ decryptByte(keys[2]));
	updateKeys(keys, lastByte = buffer1[11] ^ decryptByte(keys[2]));

	// if there is an extended header (bit in the gp flag) buffer[11] is a byte from the file time
	// with no extended header we have to check the crc high-order byte
	char c = ((header.gpFlag[0] & 0x08) == 8) ? header.modTime[1] : header.crc >> 24;

	return (lastByte == c);
}

/*!
	Decrypts an array of bytes long \p read.
*/
void UnzipPrivate::decryptBytes(uint4* keys, char* buffer, sint8 read)
{
	for (int i=0; i<(int)read; ++i)
		updateKeys(keys, buffer[i] ^= decryptByte(keys[2]));
}
