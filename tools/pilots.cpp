/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "pilots.h"

Pilots::Pilots()
{
	get_dir_entries(this,d321go_user_home.c_str(),".plt"); // TODO: end with \0 ?
}

void Pilots::add(const string& _name)// TODO: name max. 60 chars ?
{
	const string defaultplayer = d321go_main_dir + "player.txt";
	const string newfile = d321go_user_home + "pilots/" + _name;
	cp( newfile.c_str(),defaultplayer.c_str(), false); // copy default player to new player file
	push_back(_name); // add name to list
}

void Pilots::remove(const string& _name)
{
	const string remfile = d321go_user_home + "pilots/" + _name;
	remove(remfile.c_str());
	remove(_name); // add name to list
}
