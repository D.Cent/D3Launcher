/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef LANGFILE_H
#define LANGFILE_H LANGFILE_H

#include <odf.h>
using namespace odf;

#include <iostream>
#include <string>
#include <map>
using namespace std;

#include "../main/globals.h"

class LangFile
{
	private:
		OptFile of;
		map<string, string> optmap;

	public:
		LangFile(string language, string ending) :of((d321go_globals::D321GOPaths->sharepath + string("lang") + DIR_SEP + language + 
			ending).c_str(), 
			false, "rb")
		{
			of.to_map(optmap);
		}

		inline string get_help(const char* left) {
			string result;
			string tmp(left);
			for(map<string, string>::const_iterator itr = optmap.begin(); itr!=optmap.end(); itr++)
			{
				if(tmp == itr->first)
				{
					result=itr->second;
					return result;
				}
			}
			return string("Entry not found!");
		}
};

#endif // LANGFILE_H
