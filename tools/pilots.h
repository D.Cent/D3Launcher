/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef PILOTS_H
#define PILOTS_H PILOTS_H

#include <string>
#include <list>
using namespace std;

#include <odf.h>
using namespace odf;

const string d321go_user_home; // TODO!
const string d321go_main_dir;

class Pilots : public list<string> // TODO: private?
{
	
	public:
		Pilots();
		void add(const string& _name); // create new pilot (name shall contain ".plt!") and pilot file
		void remove(const string& _name); // remove pilot and his file
		~Pilots() {}
	
};

#endif // PILOTS_H
