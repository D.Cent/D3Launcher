/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

//! \file mc_string.h File containing the MultipleChoiceString class

#ifndef MC_STRING_H
#define MC_STRING_H MC_STRING_H

#include <string>
using namespace std;

#include <odf.h>
using namespace odf;

/**
\enum ChoiceType
\brief enumeration with all possible types of choice storage

For all choice types there is given,
- the storage method (what is stored in this string)
- the input method (which GUI eleement to take)
- the specification ('x.)' or none)
- whether there are multiple choices possible (only mentioned it possible)
*/
enum ChoiceType
{
	CHOICE_LINEEDIT = 0, //!< a simple text, inserted by a lineedit. no specification.
	CHOICE_FILE, //!< path to a file, inserted by a file selector. specification: 'f'.
	CHOICE_DIR, //!< directory, inserted by a directory selector. specification: 'd'.
	CHOICE_COMBO, //!< combobox selection, inserted by a combo box. specification: 'c'. multiple choices
	CHOICE_CHECKBOX //!< boolean value ('0' or '1'), inserted by a checkbox. specification: 'b'.
};

/**
	\class MultipleChoiceString
	\brief Class to store multiple choices of the user. 
	
	This class is useful to store options for your application in a specific format:
	\code
	x.)choice1|choice2|...|choicen
	\endcode
	- x specifies the type, e.g. "c.)" means combobox. See ChoiceType.
	- No x.) means CHOICE_LINEEDIT (simple text)
	- choices (choice strings) can be separated with a | (not all ChoiceTypes allow multiple choice)

*/
class MultipleChoiceString
{ // TODO!!! decide with choice type and uncomment doxygen here!!
/*	public:
		**
		\enum ChoiceType
		\brief enumeration with all possible types of choice storage
	
		For all choice types there is given,
		- the storage method (what is stored in this string)
		- the input method (which GUI eleement to take)
		- the specification ('x.)' or none)
		- whether there are multiple choices possible (only mentioned it possible)
		 *
		enum ChoiceType
		{
			CHOICE_LINEEDIT, //!< a simple text, inserted by a lineedit. no specification.
			CHOICE_FILE, //!< path to a file, inserted by a file selector. specification: 'f'.
			CHOICE_DIR, //!< directory, inserted by a directory selector. specification: 'd'.
			CHOICE_COMBO, //!< combobox selection, inserted by a combo box. specification: 'c'. multiple choices
			CHOICE_CHECKBOX //!< boolean value ('0' or '1'), inserted by a checkbox. specification: 'b'.
		}; */
		
 private:
	string str; //!< whole string in specified format
	ChoiceType type; //!< type, determined in constructor
	
	//! determines choice type by contained string
	static ChoiceType determine_type(const string& _str)
	{
		if(_str.length()<3)
			return CHOICE_LINEEDIT;
		else if(_str.compare(1,2,".)") == 0)
		{
			switch(_str[0])
			{
				case 'f': return CHOICE_FILE;
				case 'd': return CHOICE_DIR;
				case 'c': return CHOICE_COMBO;
				case 'b': return CHOICE_CHECKBOX;
				default:
					throw NEW_ERROR("Unknown type in optfile", EC_CONTINUE);
			}
		}
		else
			return CHOICE_LINEEDIT; // simple text, no x.)
	}
		
 public:
	
	//! default constructor - leaves string empty! use operator= afterwards!
	MultipleChoiceString(void) : type(CHOICE_LINEEDIT) {}
	
	/**
		construct MCS from std::string
		@note if you only want to use a part of _str, use std::string::substr()
	*/
	MultipleChoiceString(const string& _str) : str(_str), type(determine_type(str)) {}
	
	//! construct MCS from char*
	MultipleChoiceString(const char* _str) : str(_str), type(determine_type(str)) {}
	
	//! construct MCS from char* with given length
	MultipleChoiceString(const char* _str, unsigned short len) : str(_str, len), type(determine_type(str)) {}
	
	//! (re)sets MCS to @a _str - all old preferences will be overwritten
	MultipleChoiceString& operator=(const string& _str)
	{
		str = _str;
		type = determine_type(str);
		return *this;
	}
	
	/**
		\brief returns the first option of this mc string
		for example, in "c.)a1|b1|c1", this will be "a1"
		@return the first value in such a line, or, if the line does not have multiple values, the whole optstring
	*/
	string get_current(void)
	{
#ifdef D321GO_DEBUG
		printf("MCS::get_current(): str=%s, type=%d\n",str.c_str(), type);
#endif
		if (type==CHOICE_LINEEDIT)
			return str;
		else
		{
			size_t limit = str.find("|");
			return (limit==string::npos) ? str.substr(3) : str.substr(3, limit-3);
		}
	}
	
	/**
		\brief returns the first option of an mc string
		for example, in "c.)a1|b1|c1", this will be "a1"
		@param _str the option line to be parsed
		@return the first value in such a line, or, if the line does not have multiple values, the whole optstring
	*/
	static string get_current(const string& _str)
	{
/*		if(_str.compare(1,2,".)") == 0) // TODO: change: go via type here
		{
			size_t limit = _str.find("|");
			return (limit==string::npos) ? _str.substr(3) : _str.substr(3, limit-3);
		}
		else
			return _str;*/
		if(determine_type(_str) == CHOICE_LINEEDIT)
		 return _str;
		else
		{
			size_t limit = _str.find("|");
			return (limit==string::npos) ? _str.substr(3) : _str.substr(3, limit-3);
		}
	}
	
	/**
		\brief sets the first option of an mc string
		@param choice the string which shall be the first option
		@exception ErrorClass thrown when in a combo, choice was not a value of the combobox,
		 or, in a checkbox, when the value was not "0" or "1"
	*/
	void set_current(const string& choice)
	{
		switch(type)
		{
			case CHOICE_LINEEDIT:
			 str=choice;
			 break;
			case CHOICE_COMBO:
			 { // put entry choice to the front
			 string::size_type oldpos = str.find(choice);
			 if(oldpos == string::npos)
			  throw NEW_ERROR("Tried to set_current() with a value not present in combo box", EC_STOP_EXECUTION);
			 string::size_type choice_length = choice.length();
			 //for(string::size_type i=3; i<(oldpos-1); i++)
			 for(string::size_type i=oldpos-2; i>2; i--)
			  str[i+choice_length+1]=str[i]; // + 1 for one |
			 str.replace(3, choice_length, choice);
			 str[3+choice_length]='|';
			 break;
			 }
			case CHOICE_CHECKBOX:
			 {
			 if(choice != "0" && choice != "1")
			  throw NEW_ERROR("Tried to set_current() with non-boolean string", EC_STOP_EXECUTION);

			 break;
			 }
			default:
			 str.replace(3,str.size(), choice);
		}
	}
	
	/**
		pushes back a choice string \a choice to the mcs
		@exception ErrorClass thrown if choice number is to high for choice type
	*/
	void add_choice(const string& choice)
	{
		switch(type)
		{
			case CHOICE_COMBO:
				str.push_back('|');
				str += choice;
				break;
			default:
				if((type==CHOICE_LINEEDIT && str.length()>0)||str.length()>3)
					throw NEW_ERROR("Could not append another choice", EC_STOP_EXECUTION);
				str = choice;
		}
	}
	
	//! get const. reference to whole mc string, incl. 'x.)'
	const string& get_whole_string(void) {
		return str;
	}
	
	//! returns list of all choice strings
	list<string> get_list(void)
	{
		list<string> ret_list;
		string::size_type nextpos, pos=(type==CHOICE_LINEEDIT)?0:3;
		while(1)
		{
			nextpos = str.find('|', pos);
			if(nextpos == string::npos) {
				ret_list.push_back(str.substr(pos));
				break;
			}
			else {
				ret_list.push_back(str.substr(pos,nextpos-pos));
				pos=nextpos+1; // jump to begin of next choice
			}
		}
		return ret_list;
	}
	
	//! returns number of choice strings
	size_t get_num (void)
	{
		size_t pos=2, idx = 0;
		for(; pos!=string::npos; idx++, pos = str.find('|', pos+1)) ;
		return idx;
	}
	
	/**
		returns choice string with given number
		@exception ErrorClass thrown when the index was too high
	*/
	string operator[] (size_t i)
	{
		size_t pos=(type==CHOICE_LINEEDIT)?0:3, idx = 0;
		for(; idx<i; idx++)
		{
			pos = str.find('|', pos);
			if(pos == string::npos)
			 throw NEW_ERROR("Optfile index too high!");
			pos++; // jump to begin of next choice
		}
		
		size_t limit = str.find('|',pos);
		return (limit==string::npos) ? str.substr(pos) : str.substr(pos, limit-pos);
	}
	
	//! returns choice type
	inline ChoiceType get_type() {
		return type;
	}
};

#endif // MC_STRING_H
