/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#if 0
#include <string>
using namespace std;

#include <odf.h>
using namespace odf;

class D321GOInfo
{
	private:
		
	
	public:
		
		const string d321go_home_path; // path of current user
		const string d321go_share_path; // d321go's install path of administrator
		
		// http://vlaurie.com/computers2/Articles/environment.htm
		D321GOInfo() : d321go_home_path(homedir(".d321go/")), d321go_share_path() // APPDATA? PROGRAMMFILES
		{
			
		}
		
		
		// TODO : static const char[] version = "0.29 INOFFICIAL";
		
};
#endif

