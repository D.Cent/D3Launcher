/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file main.cpp
	Contains the main() function (on the bottom of this page)
*/

// GUI specific includes
#if defined(USE_GTKMM)
	#include <gtkmm.h>
	using namespace Gtk;
	#include "../gui_gtkmm/d321go_mw.h"
	#include "../gui_gtkmm/dialogs.h"
#elif defined(USE_QT)
	#include <QApplication>
	#include "../gui_qt/qd321go.h"
	#include "../gui_qt/dialogs.h"
#endif

// something unuseful no one uses
#define DEFAULT_REMOTE_PORT 3719

#include <odf.h> // libodf
using namespace odf;

#define _LIBODF_MIN_VER_ "0.11"

#include "globals.h" // globals are initialized here
#include "args.h"
#include "../tools/mc_string.h"
#include "../tools/langfile.h"
#include "../callbacks/player_selection.h"
#include "../network/d321go_server.h"

namespace g = d321go_globals; // we use d321go_globals very often here, so let's use a shorter name

// globals, initialized extern in globals.h
class AppPaths* g::D321GOPaths;
string g::player_name;
string g::language;
OptFile* g::PlayerOptfile;
OptFile* g::LanguageOptfile;
LangFile* g::HelpStrings;
LangFile* g::Translations;
D321GOVersion g::version;

const char *exit_sentence[5] = { "Thank you for using D321GO!!", "Have a nice day!", "Visit http://www.odf-online.org for more information...", "Interested? We still need some coders - apply at http://www.odf-online.org today!", "D321GO!! - Quality since 2006!" };
bool first_start;

void first_start_init(void)
{	
	t_mkdir( g::D321GOPaths->homepath.c_str() );
	first_start=true;

#ifdef __LINUX__
	msg_box("Compiling checksum filter", "Please make sure you have gcc installed, or building the library will fail.");
	
	string compiler_command("gcc -fPIC -m32 -O2 -pipe -s -shared /usr/local/share/ODF/d321go/send_recv_filter.c -o ");
	compiler_command+=(g::D321GOPaths->homepath + "send_recv_filter.so -ldl").c_str();
	
	if(system(compiler_command.c_str()) != 0)
	 msg_box("Error", "Checksum filter failed at compiling - you won't be able to use it.");
#endif
}

int main(int argc, char** argv)
{
	try
	{

#if defined(USE_GTKMM)
		//g_mem_gc_friendly=TRUE;
#ifdef __WIN32__
		if (!g_thread_supported ()) //make sure the thread system is initialized
		 g_thread_init (NULL); //if not, initialize it
#endif

		Main kit(argc, argv);
#elif defined(USE_QT)
		QApplication app(argc, argv);

		QFont fnt = app.font();
		fnt.setPointSize(10);
		app.setFont(fnt);
#endif
		if(strncmp(LIBODF_VERSION,_LIBODF_MIN_VER_,4)<0) {
			msg_box("libodf version too old!","Please get a new libodf - at least "\
				_LIBODF_MIN_VER_" - from odf-online.org!");
			return 1;		
		}

		// initialize d321go's paths
		g::D321GOPaths = new AppPaths("d321go");

		if( ! checkdir ( g::D321GOPaths->homepath.c_str() ) )
		{
			msg_box("Welcome", "Welcome to D321GO!!\nYour personal files will now be created...");
			
			first_start_init();
		}
		else
		 first_start=false;
		
		// read arguments and store info in job structure
		job cur_job;
		if(! read_parameters(argc, argv, &cur_job) )
		 return 0;
		
		if(cur_job.remote)
		{
			try
			{
				TCPClient remote_client("127.0.0.1", (cur_job.remote_port) ? cur_job.remote_port : DEFAULT_REMOTE_PORT);

				if(cur_job.directip)
					remote_client.send(cur_job.directip, _MIN(strlen(cur_job.directip)+1, 128));
				else
					remote_client.send("", 1);

				return 0;
			}
			catch(ErrorClass e)
			{
				e.print_error();
				puts("Remote client failed - starting new client...");
				cur_job.remote = false;
			}
		}
		
		if( cur_job.pilot )
		 g::player_name = cur_job.pilot;
		else
		 if(! player_selection_dialog() )
		  return 0; // quit if user refuses to choose player
		
		// init Optfile for the player and his language
		g::PlayerOptfile = new OptFile((g::D321GOPaths->homepath + g::player_name + ".plr").c_str(), true);
		
		//choose language on first startup
		if(first_start)
		{
			MultipleChoiceString mcs_lang(g::PlayerOptfile->get_single_entry("language","d321go_options"));
			list<string> languages = mcs_lang.get_list();
			
			SelectionDialog sdlg("Select a language", languages);

			if(sdlg.status())
			{
				mcs_lang.set_current(sdlg.get_selected_item());
				g::PlayerOptfile->set_single_entry("language", mcs_lang.get_whole_string().c_str(), "d321go_options", true);
			}
		}
		
		try
		{
			g::language = MultipleChoiceString::get_current( g::PlayerOptfile->get_single_entry("language","d321go_options"));
			g::HelpStrings = new LangFile(MultipleChoiceString::
				get_current(g::PlayerOptfile->get_single_entry("language","d321go_options")), string(".hlp"));
			g::Translations = new LangFile(MultipleChoiceString::
				get_current(g::PlayerOptfile->get_single_entry("language","d321go_options")), string(".tra"));
			g::LanguageOptfile = new OptFile((g::D321GOPaths->sharepath+"lang/"+g::language + ".lang").c_str(), true, "rb");
		}
		catch(ErrorClass ec) {
			msg_box("Error in pilot file.", "Obviously, your pilot file is too old for this version or it is corrupted!\n" 
				"Aborting, Error description follows (in English) ...");
			msg_box("Pilot File Error", ec.description());
			throw NEW_ERROR("Pilot File Error", EC_STOP_EXECUTION);
		}
		
		// init the main window
#if defined(USE_GTKMM)
		MainWindow d321go;
#elif defined(USE_QT)
		qd321go d321go;
#endif
		
		// initialize remote server
		//printf("REMOTE PORT: %d\n",cur_job.remote_port);
		D321GOServer remote_srv((cur_job.remote_port) ? cur_job.remote_port : DEFAULT_REMOTE_PORT, &d321go);
		pthread_t remote_thread;
		pthread_create(&remote_thread, NULL, D321GOServer::call_ac, &remote_srv);

		// parameter specific stuff
		if( cur_job.directip )
		 d321go.set_directip( cur_job.directip );
		
		// run the main window
#if defined(USE_GTKMM)
		if(first_start)
		 d321go.showStartupHelp();

		Main::run(d321go);
#elif defined(USE_QT)
		d321go.show();
		
		if(first_start)
		 d321go.showStartupHelp();
	
		app.exec();
#endif
		
		// clean up
		delete g::LanguageOptfile;
		delete g::PlayerOptfile;
		delete g::HelpStrings;
		delete g::Translations;
		delete g::D321GOPaths;

	}
	catch (ErrorClass e)
	{
		e.print_error();
		return 1;
	}
	
	srand(time(NULL));
	int number=(int) (5*1.0*rand()/(RAND_MAX+1.0));
	printf("%s\n", exit_sentence[number]);
	
	return 0;
}
