/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef D321GO_GLOBALS_H
#define D321GO_GLOBALS_H D321GO_GLOBALS_H

#include <string>
using namespace std;

#include <odf.h>
using namespace odf;

#include "version.h"

#ifdef USE_KOPETE
 #include "../plugins/kopete/kopete.h"
#endif

class LangFile;

namespace d321go_globals
{
	//! version info, needed help and for updates
	extern D321GOVersion version;
	
	//! All paths for our application
	extern AppPaths* D321GOPaths;
	
	//! Name of the player. Append ".plr" to get the profile
	extern string player_name;
	
	//! Language the player speaks. Append ".lang" for language file
	extern string language;
	
	//! OptFile for all options of the Player. Location is HomePath + "./d321go/" + player_name + ".plr" 
	extern OptFile* PlayerOptfile;
	
	//! OptFile for all strings the GUI has to use. Location is language + ".plr" in the SharePath
	extern OptFile* LanguageOptfile;

	//! Strings the GUI needs to show help-boxes. Location is language.hlp in the SharePath
	extern LangFile* HelpStrings;

	//! Strings the GUI needs for ToolTips in Options windows
	extern LangFile* Translations;
	
#ifdef USE_KOPETE
#include "../plugins/kopete/kopete.h"
	//! Kopete Object for Communication with Kopete
	extern D321GOKopeteClient kopete_client;
#endif

};

#endif // D321GO_GLOBALS_H

