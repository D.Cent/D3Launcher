/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef D321GO_ARGS_H
#define D321GO_ARGS_H D321GO_ARGS_H

/**
	\struct job
	stores information which is given to the current job via commandline
*/
struct job
{
	const char* directip;
	const char* pilot;
	bool remote;
	int remote_port;
	job(void) : directip(NULL), pilot(NULL), remote(false), remote_port(0) {} // default options
};

//! reads all parameters into job struct, returns false if program shall not be started (-h or -v)
bool read_parameters(int p_argc, char *p_argv[], job* cur_job);

#endif // D321GO_ARGS_H
