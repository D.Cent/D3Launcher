/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;

#include <getopt.h>

#include <odf.h> // libodf
using namespace odf;

#include "globals.h" // for version info

#include "args.h"

bool read_parameters(int p_argc, char *p_argv[], job* cur_job)
{
	
	char c;
	int option_index = 0;
	
	static struct option long_options[] = {
		{"help", 0, 0, 'h'},
		{"version", 0, 0, 'v'},
		{"directip", 1, 0, 'i'},
		{"pilot", 1, 0, 'p'},
		{"remote", 0, 0, 'r'},
		{"rport", 1, 0, 'o'},
		{0, 0, 0, 0}
	};
	
	/* for(count=0; count < p_argc; count++)
	printf("argv[%d]=%s\n",count,p_argv[count]); */
	
	optind=0;
	
	while(1)
	{

#define D321GO_GETOPT_STR "hvi:p:ro:"
			
#if defined(__WIN32__) /* MINGW compiler has no getopt_long_only() ... */
		c=getopt_long(p_argc, p_argv, D321GO_GETOPT_STR, long_options, &option_index);
#else
		c=getopt_long_only(p_argc, p_argv, D321GO_GETOPT_STR, long_options, &option_index);
#endif
		
#if 0
		printf("c=%c\n",c);
		printf("optind=%d, opterr=%d, optopt=%d, optarg=%s\n",optind, opterr, optopt, optarg);
#endif
		
		if(c==-1)
		 break;
	
		switch(c)
		{
		
			case 'h':
				printf("\nD321GO!!\n");
				printf("::::::::\n");
				printf("%s %s\n", d321go_globals::version.product.c_str(), 
					d321go_globals::version.name.c_str());
				printf("\nSyntax: d321go [-h|-v| OPTIONS [VALUES] ]\n\n");
				printf("\t[-h|--help]\t\tshow this helpscreen\n");
				printf("\t[-v|--version]\t\tdisplay version\n");
/*				printf("\t[-q|--query]\t\tVersionen diverser benutzbarer Programme anzeigen\n");
				printf("\n\tOPTIONEN\n");
				printf("\t[-r|--remote] ID\tStartet D321GO!! im Remote-Modus (bei -p oder -i)\n\n");*/
				printf("\n\tOPTIONS\n\n");
				printf("\t[-i|--directip] IP\tspecify an IP for netgames\n");
				printf("\t[-p|--pilot] PILOTNAME\tspecify pilot name (without any file ending)\n");
				printf("\t[-r|--remote] ID\tstart D321GO!! in remote mode (useful with -i [IP])\n");
				printf("\t[-o|--rport] PORT\tuse port for remote server and client instead of default port\n\n");
#ifdef __LINUX__
				printf("You may also use one minus instead of two (-pilot instead of --pilot).\n");
#endif
				printf("The parameters -h, -v und -q will not start the program.\n\n");
				return false;
			
			case 'v':
				fprintf(stderr, "\nD321GO!!\n");
				fprintf(stderr, "::::::::\n");
				fprintf(stderr,"\nThis binary was compiled on " __DATE__ " at " __TIME__ ".\n\n");
				printf("%s %s\n\n", d321go_globals::version.product.c_str(),
					d321go_globals::version.name.c_str());
				return false;

			case 'i':
				cur_job->directip = optarg;
			break;
			
			case 'p':
				cur_job->pilot = optarg;
			break;
			
			case 'r':
				cur_job->remote = true;
			break;

			case 'o':
				cur_job->remote_port = atoi(optarg);
			break;

			
		}
		
		
		
	}
	
	// now check for all other arguments... they are not understood!
	if (optind < p_argc)
	{
		optind++;
		for (; optind < p_argc; optind++)
		 printf("WARNING: unknown parameter %s!\n",p_argv[optind]);
	}
	
	return true;
}
