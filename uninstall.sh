#!/bin/bash

# check for superuser rights
AMIROOT=`whoami`

if [ $AMIROOT != "root" ]; then
        echo "This script needs to be run as superuser (root). Try 'sudo sh $0'."
        exit
fi

printf "This will remove D321GO!! completely from your system. Continue? (y/n) "
read INSTALL

case $INSTALL in
	y)
	;;
	*)
		echo "Uninstallation aborted ..."
		exit
	;;
esac

rm -rf /usr/local/share/ODF/d321go
rm -f /usr/local/bin/d321go

if kde4-config >/dev/null 2>/dev/null; then
 rm -f `kde4-config --install services`/d1.protocol
 rm -f `kde4-config --install services`/d2.protocol
 rm -f `kde4-config --install services`/d3.protocol
fi

if kde-config >/dev/null 2>/dev/null; then
 rm -f `kde-config --install services`/d1.protocol
 rm -f `kde-config --install services`/d2.protocol
 rm -f `kde-config --install services`/d3.protocol
fi

echo "The program has been uninstalled. If you want to remove the settings-folder, remove ~/.d321go!"

