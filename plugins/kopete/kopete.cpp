/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/* kopete-remote-functions */

#include <string>
#include <iostream>
using namespace std;

#include <odf.h> // for error class
using namespace odf;

#include <dcopclient.h>
#include <qcstring.h> // the QT-strings

#include "kopete.h"

static QString channel; // TODDO: ="#descent"

D321GOKopeteClient::D321GOKopeteClient(void) // attaching the Client to the DCCP-Server
{
	client = new DCOPClient;
	if( ! client->attach())
 	 throw NEW_ERROR("D321GOKopeteClient: couldn't connect to DCOP-Client (maybe it is not running?)");
	
	QString realAppId = client->registerAs("D321GO");
	channel="#descent";
}


void D321GOKopeteClient::change_status(d321go_kopete_status status, const char *directip, bool is_away)
{
	//cout << "status : " << status << endl;
	
	if(status == STATUS_QUIT)
	 kopete_write_message("/quit Quit via D321GO!! ( http://d321go.funpic.de )");
	/*if(status == STATUS_QUIT){ // TODO -> NEW FUNC!
	        QByteArray ba;
                QDataStream ds(ba, IO_WriteOnly);
                ds << QString("IRCProtocol") << QString("DescentNet");
                client->send("kopete", "KopeteIface", "connect(QString,QString)", ba);
	}*/
	else if(status == STATUS_AWAY)
	{
	
		string status_message="Yeah baby! I am playing Descent again!\n Join me @ d3://";
		status_message += directip;
	
		QString away_message_qs;
		away_message_qs=status_message;
	
		QByteArray ba;
		QDataStream ds(ba, IO_WriteOnly);
		ds << away_message_qs << is_away;
	
		if( !client->send("kopete", "KopeteIface", "setAway(QString,bool)", ba))
		 throw NEW_ERROR("D321GOKopeteClient:  Client couldn't send data to Kopete!");
		
	}
}


void D321GOKopeteClient::playing_msg(const char *directip) // write the clicked-on-launchlink-message...
{
	string message="/me joined Server via Link: d3://";
	if(directip != NULL)
	 message += directip;
	else
	 message = "/me joins a game";
	
	kopete_write_message(message);
}


void D321GOKopeteClient::kopete_write_message(string message)
{
	QString message_qs;
	message_qs=message;
	
	QByteArray ba;
	QDataStream ds(ba, IO_WriteOnly);
	ds << channel << message_qs;
	
	if( !client->send("kopete", "KopeteIface", "messageContact(QString,QString)", ba))
	 throw("D321GOKopeteClient: Client couldn't send data to Kopete!");
}

D321GOKopeteClient::~D321GOKopeteClient()
{
	delete client;
}
