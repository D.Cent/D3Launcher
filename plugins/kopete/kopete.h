/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef D321GO_KOPETE_H
#define D321GO_KOPETE_H D321GO_KOPETE_H

//#include <dcopclient.h> // for the DCopClient

enum d321go_kopete_status {
	STATUS_AWAY,
	STATUS_QUIT
};

class DCOPClient;

class D321GOKopeteClient
{
	private:
		DCOPClient* client; // use a pointer so the files including kopete.h won't have to know about DCOPClient
	
	public:
		D321GOKopeteClient(void);
		~D321GOKopeteClient();
		/**
			Change Kopete Client status
			@param status the status that shall be true afterwards
			@param directip the IP the player will join
			@param is_away says whether the away status shall be swithed on (true) or of (false)
		*/
		void change_status(d321go_kopete_status status, const char *directip, bool is_away = true);
		//! write the clicked-on-launchlink-message so others can follow
		void playing_msg(const char *directip);
		//! write a message to the chat, currently not used by D321GO
		void kopete_write_message(string message);
};

#endif // D321GO_KOPETE_H

