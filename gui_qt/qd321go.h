/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/* Main GUI header - here everything comes together :-) */

#ifndef QD321GO_H
#define QD321GO_H QD321GO_H

#include <QVariant>
#include <QAction>
#include <QLineEdit>
#include <QPushButton>
#include <QMainWindow>
#include <QLabel>
#include <QTableWidget>
#include <QMenu>
#include <QMenuBar>
#include <QMouseEvent>
#include <QGroupBox>
#include <QMessageBox>
#include <QToolTip>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QProgressBar>
#include <QCursor>
#include <QTimer>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QSpacerItem>

#ifdef USE_KONVERSATION_PLUGIN
	#include <QDBusMessage>
	#include <QDBusConnection>
#endif

#include "menus.h"

#include "label_d321go.h"
#include "tracker_d3.h"

extern D321GOMenuBar menu_bar;

class qd321go : public QMainWindow
{
	Q_OBJECT

	private:
		short mode; // 1=Descent1, 2=Descent2, 3=Descent3 and 4=Flight Back
		bool minimized;
		bool remote;
		vector<string> link_list;

	public:
		QWidget widget_main;
		QWidget widget_gb;
		QWidget widget_lbl;
		
		QHBoxLayout* main_layout;
		QVBoxLayout* lbl_layout;
		QVBoxLayout* gb_layout;
		QHBoxLayout* gb_main_layout;
		QHBoxLayout* gb_main_layout_fb;
		QHBoxLayout* gb_start_layout;
		QVBoxLayout* gb_main_layout_refresh;
		
		D321GOMenuBar menu_bar;
	
		QGroupBox gb_start;
		QGroupBox gb_main;
		QLineEdit le_directip;
		QPushButton pb_refresh;
		QPushButton pb_options;

		ServerTracker tracker;
		label_d321go lbl_d;
		label_d321go lbl_3;
		label_d321go lbl_2;
		label_d321go lbl_1;
		label_d321go lbl_go;
		
		QLabel lbl_fb;
		QLabel lbl_directip;
		
		QSystemTrayIcon tray_icon;
		QIcon *d321go_icon;
		
		QProgressBar bar_tracker;
		RefreshThread thread;
		
		QMenu popup;
		QAction act_exit;
	
		QAction *launchlinks[10];
		
		Qt::MouseButton lastMouseBtn;
		
		QTimer *tmr_trackerRefresh;
		
		QSpacerItem spacer1;
		QSpacerItem spacer2;
		
		qd321go() :     widget_main(this), widget_gb(this), widget_lbl(this), menu_bar(this), gb_start(&widget_gb), gb_main(&widget_gb), le_directip(&gb_start), pb_refresh(&gb_start),
				pb_options(&gb_start), tracker(this, &gb_main), lbl_d(&widget_lbl), lbl_3(&widget_lbl), lbl_2(&widget_lbl), lbl_1(&widget_lbl), lbl_go(&widget_lbl),
				lbl_fb(&widget_gb), lbl_directip(&gb_start), tray_icon(this), bar_tracker(&widget_gb), thread(this), popup(this), act_exit(tr("&Exit"), this),
				spacer1(0, 0, QSizePolicy::Maximum, QSizePolicy::Maximum), spacer2(0, 0, QSizePolicy::Maximum, QSizePolicy::Maximum)
                        {
				main_layout = new QHBoxLayout;
				lbl_layout = new QVBoxLayout;
				gb_layout = new QVBoxLayout;
				gb_main_layout = new QHBoxLayout;
				gb_start_layout = new QHBoxLayout;
				gb_main_layout_fb = new QHBoxLayout;
				
				gb_main_layout_refresh = NULL;
				
                                setupUi();
				
				for(int i=0; i!=10; ++i)
					launchlinks[i] = new QAction(this);
                        }
		
		~qd321go()
		{
			// we have to delete the layouts manually or the program will destroy the groupboxes first which would cause a crash
			delete main_layout;
			delete lbl_layout;
			delete gb_layout;
			delete gb_main_layout;
			delete gb_start_layout;
			delete gb_main_layout_fb;
			delete gb_main_layout_refresh;
			
			delete d321go_icon;
			delete tmr_trackerRefresh;
			
			for(int i=0; i!=9; ++i)
			 delete launchlinks[i];
		}
		
		void setupUi();
		void retranslateUi();

	public slots:
		void edit_options();
		
		void switch_d3();
		void switch_d2();
		void switch_d1();
		void switch_fb();
		
		void start_game();
		void set_directip(const string& ip);
		
		void switch_tray(QSystemTrayIcon::ActivationReason reason);
		void show_message(const QString& text);
		
		void start_refresh();
		void show_tracker();

		void logout();
		void contact_d321go(QAction* action);
		
		void showStartupHelp();
	
	signals:
		void tracker_refreshed();
	
	protected:
		void closeEvent(QCloseEvent *event);
};

#endif // QD321GO_H
