/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
#include <QString>

#include <string>
using namespace std;

QString int_to_str(int number) {
	char char_string[5];
	snprintf(char_string, 5, "%hu", number);
	
	return char_string;
}

char* int_to_char(int number) {
	static char char_string[5];
	snprintf(char_string, 5, "%hu", number);
	
	return char_string;
}

string turn_string(string to_turn)
{
	char array[256], result[256];   
	int smax, k;

	strcpy(array, to_turn.c_str());

	for(smax=0; array[smax] != '\0'; smax++) ;
	   
	result[smax] = '\0';

	for(k = smax-1; k >= 0; k--)
   
	result[smax-k-1] = array[k];

	return string(result);
}
