/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "str_funcs.h"
#include "qd321go.h"
#include "server_info.h"
#include "options.h"
#include "d321go.h"

#include "../callbacks/start.h"
#include "../network/download_mod.h"
#include "dialogs.h"
#include "../main/globals.h"

#include "../tools/langfile.h"

using namespace d321go_globals;

void qd321go::setupUi()
{
	widget_gb.setLayout(gb_layout);
	widget_lbl.setLayout(lbl_layout);
	widget_main.setLayout(main_layout);
	
	setCentralWidget(&widget_main);
	
	mode=3;

	resize(600, 335);

	// setup the system tray icon
	d321go_icon = new QIcon(QString((D321GOPaths->sharepath + string("images/icon.png")).c_str()));
	tray_icon.setIcon(*d321go_icon);
	tray_icon.setVisible(true);
	
	QString d321go_ver(d321go_globals::version.product.c_str());
	d321go_ver.append(" ");
	d321go_ver.append(d321go_globals::version.name.c_str());
	
	tray_icon.setToolTip(d321go_ver);
	
	minimized=false;

	//menubar
	setMenuBar(&menu_bar);
	
	lbl_directip.setText("Direct IP:");

	//let's set the label - we split it into pieces because the user must be able to click on the letters
	lbl_d.setPixmap(QPixmap((D321GOPaths->sharepath + string("images/label/d.png")).c_str()));
	lbl_d.setToolTip(QString::fromLocal8Bit((HelpStrings->get_help("label_fb")).c_str()));

	lbl_3.setPixmap(QPixmap((D321GOPaths->sharepath + string("images/label/3.png")).c_str()));	
	lbl_3.setToolTip(QString::fromLocal8Bit((HelpStrings->get_help("label_d3")).c_str()));
	
	lbl_2.setPixmap(QPixmap((D321GOPaths->sharepath + string("images/label/2.png")).c_str()));
	lbl_2.setToolTip(QString::fromLocal8Bit((HelpStrings->get_help("label_d3")).c_str()));
	
	lbl_1.setPixmap(QPixmap((D321GOPaths->sharepath + string("images/label/1.png")).c_str()));
	lbl_1.setToolTip(QString::fromLocal8Bit((HelpStrings->get_help("label_d1")).c_str()));
	
	lbl_go.setPixmap(QPixmap((D321GOPaths->sharepath + string("images/label/go.png")).c_str()));
	lbl_go.setToolTip(QString::fromLocal8Bit((HelpStrings->get_help("label_go")).c_str()));
	
	//because "Flight Back" didn't release yet, we just show a little text
	lbl_fb.hide();
	
	bar_tracker.setGeometry(240, 145, 200, 25);
	bar_tracker.setMaximum(0);
	bar_tracker.setMinimum(0);
	bar_tracker.hide();
	
	remote=false;
	
	retranslateUi();
	
	tray_icon.setContextMenu(&popup);
	popup.addAction(&act_exit);
	
	QObject::connect(&pb_refresh, SIGNAL(clicked()), this, SLOT(start_refresh())); 
	QObject::connect(&pb_options, SIGNAL(clicked()), this, SLOT(edit_options())); 
	
	QObject::connect(&lbl_d, SIGNAL(clicked()), this, SLOT(switch_fb()));
	QObject::connect(&lbl_3, SIGNAL(clicked()), this, SLOT(switch_d3()));
	QObject::connect(&lbl_2, SIGNAL(clicked()), this, SLOT(switch_d2()));
	QObject::connect(&lbl_1, SIGNAL(clicked()), this, SLOT(switch_d1()));
	QObject::connect(&lbl_go, SIGNAL(clicked()), this, SLOT(start_game()));
	QObject::connect(&le_directip, SIGNAL(textChanged(QString)), this, SLOT(show_message(QString)));
	
	QObject::connect(&tray_icon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(switch_tray(QSystemTrayIcon::ActivationReason)));
	QObject::connect(&tray_icon, SIGNAL(messageClicked()), this, SLOT(start_game()));
	
	QObject::connect(&thread, SIGNAL(threadDone()), this, SLOT(show_tracker()));
	
	QObject::connect(&popup, SIGNAL(triggered(QAction*)), this, SLOT(contact_d321go(QAction*)));
	QObject::connect(&act_exit, SIGNAL(triggered()), this, SLOT(logout()));

	QMetaObject::connectSlotsByName(this);
	
	tmr_trackerRefresh = new QTimer(this);
	
	QObject::connect(tmr_trackerRefresh, SIGNAL(timeout()), this, SLOT(start_refresh()));
	
	unsigned int timeout = atoi(d321go_globals::PlayerOptfile->get_single_entry("tracker_refresh_interval", "d321go_options").c_str());

	gb_main_layout->addWidget(&tracker);
	gb_main.setLayout(gb_main_layout);
	
	gb_start_layout->addWidget(&pb_options, Qt::AlignLeft);
	gb_start_layout->addWidget(&pb_refresh, Qt::AlignLeft);
	gb_start_layout->addStretch(1000);
	gb_start_layout->addWidget(&lbl_directip, 0, Qt::AlignRight);
	gb_start_layout->addWidget(&le_directip, 0, Qt::AlignRight);
	gb_start.setLayout(gb_start_layout);
	
	gb_layout->addWidget(&gb_main);
	gb_layout->addWidget(&gb_start);
	widget_gb.setLayout(gb_layout);
	
	lbl_layout->setSpacing(0);
	lbl_layout->addStretch(1000);
	lbl_layout->addWidget(&lbl_d, Qt::AlignCenter);
	lbl_layout->addWidget(&lbl_3, Qt::AlignCenter);
	lbl_layout->addWidget(&lbl_2, Qt::AlignCenter);
	lbl_layout->addWidget(&lbl_1, Qt::AlignCenter);
	lbl_layout->addWidget(&lbl_go, Qt::AlignCenter);
	lbl_layout->addStretch(1000);
	widget_lbl.setLayout(lbl_layout);
	
	main_layout->addWidget(&widget_lbl);
	main_layout->addWidget(&widget_gb);
	widget_main.setLayout(main_layout);
	
	widget_lbl.setStyleSheet("QWidget { background-color: white; }");

	// automatic tracker refresh
	if (timeout > 0)
	{
		tmr_trackerRefresh->start(1000*60*timeout);
		start_refresh();
	}
}
		
void qd321go::retranslateUi()
{
	setWindowTitle("D3Launcher");
	
	pb_options.setText(QString::fromLocal8Bit((Translations->get_help("mainwindow_options")).c_str()));
	pb_refresh.setText(QString::fromLocal8Bit((Translations->get_help("mainwindow_tracker")).c_str()));
	
	QString fb_tmp(QString::fromLocal8Bit((Translations->get_help("mainwindow_fb_1")).c_str()));
	fb_tmp.append("\n");
	fb_tmp.append(QString::fromLocal8Bit((Translations->get_help("mainwindow_fb_2")).c_str()));
	fb_tmp.append("\n\n");
	fb_tmp.append(QString::fromLocal8Bit((Translations->get_help("mainwindow_fb_3")).c_str()));
	fb_tmp.append("\n");
	fb_tmp.append(QString::fromLocal8Bit((Translations->get_help("mainwindow_fb_4")).c_str()));
	
	lbl_fb.setText(fb_tmp);
	
	tracker.retranslateUi();
}

void qd321go::closeEvent(QCloseEvent *event)
{
	Q_UNUSED(event);
	logout();
}

void qd321go::edit_options()
{
	opt_dlg* dlg;

	if(mode == 1)
	 dlg = new opt_dlg("d1_options");
	else if(mode == 2)
	 dlg = new opt_dlg("d2_options");
	else if(mode == 3)
	 dlg = new opt_dlg("d3_options");
	else
	 return;

	dlg->show();
	dlg->exec();

	delete dlg;
}

void qd321go::switch_d3()
{
	if (mode == 3)
		return;
	
	if (mode == 4)
	{
		delete gb_main_layout_fb;
		gb_main_layout_fb = NULL;
		
		gb_main_layout = new QHBoxLayout;
		gb_main_layout->addWidget(&tracker);
		
		gb_main.setLayout(gb_main_layout);
	}
	
	mode=3;

	gb_start.show();
	gb_main.show();

	lbl_fb.hide();
	pb_refresh.setEnabled(true);

	tracker.show();
}

void qd321go::switch_d2()
{
	if (mode == 2)
		return;
	
	if (mode == 4)
	{
		delete gb_main_layout_fb;
		gb_main_layout_fb = NULL;
		
		gb_main_layout = new QHBoxLayout;
		gb_main_layout->addWidget(&tracker);
		
		gb_main.setLayout(gb_main_layout);
	}
	
	mode=2;

	gb_start.show();
	gb_main.hide();
	
	pb_refresh.setEnabled(false);
	
	tracker.hide();
}

void qd321go::switch_d1()
{
	if (mode == 1)
		return;
	
	if (mode == 4)
	{
		delete gb_main_layout_fb;
		gb_main_layout_fb = NULL;
		
		gb_main_layout = new QHBoxLayout;
		gb_main_layout->addWidget(&tracker);
		
		gb_main.setLayout(gb_main_layout);
	}
	
	mode=1;

	gb_start.show();
	gb_main.hide();

	pb_refresh.setEnabled(false);
	
	tracker.hide();
}

void qd321go::switch_fb()
{
	if (mode == 4)
		return;
	
	lbl_fb.show();
	
	//gb_main_layout->removeWidget(&tracker);
	//gb_main_layout->addWidget(&lbl_fb);
	
	delete gb_main_layout;
	gb_main_layout = NULL;
	
	gb_main_layout_fb = new QHBoxLayout;
	gb_main_layout_fb->addStretch(1000);
	gb_main_layout_fb->addWidget(&lbl_fb, Qt::AlignCenter);
	gb_main_layout_fb->addStretch(1000);
	gb_main.setLayout(gb_main_layout_fb);
	
	mode=4;

	gb_start.hide();
	tracker.hide();
	gb_main.show();
}

void qd321go::start_game()
{
	if (le_directip.text().length() == 0)
	{
		func_start(mode, "");
		return;
	}

#ifdef USE_KONVERSATION_PLUGIN
	QDBusMessage m = QDBusMessage::createMethodCall("org.kde.konversation",
		"/irc",
		"",
		"say");
	
	m << "irc.descentforum.net";
	m << "#Descent";
#endif

	try
	{
		if( mode == 1 )
		{
			QString msg("/me joined server via link: d1x://");

			int pos = le_directip.text().indexOf("://");

			if (pos != -1)
				msg.append(le_directip.text().right(le_directip.text().length() - pos - 3));
			else
				msg.append(le_directip.text());

#ifdef USE_KONVERSATION_PLUGIN
			m << msg;
			QDBusConnection::sessionBus().send(m);
#endif

			tmr_trackerRefresh->stop();
			func_start(1, le_directip.text().toStdString().c_str());
		}
		else if( mode == 2 )
		{
			QString msg("/me joined server via link: d2x://");

			int pos = le_directip.text().indexOf("://");

			if (pos != -1)
				msg.append(le_directip.text().right(le_directip.text().length() - pos - 3));
			else
				msg.append(le_directip.text());

#ifdef USE_KONVERSATION_PLUGIN
			if (le_directip.text().length() > 0)
			QDBusConnection::sessionBus().send(m);
#endif

			tmr_trackerRefresh->stop();
			func_start(2, le_directip.text().toStdString().c_str());
		}
		else if (mode == 3 )
		{
			bool in_tracker=false;
			int count=0;

			for(list<string>::iterator itr = tracker.ip_list.begin(); itr != tracker.ip_list.end(); itr++ )
			{
				if(count == tracker.rowCount())
				 break;
			
				if(strstr(le_directip.text().toStdString().c_str(), itr->c_str()) != NULL)
				{
					in_tracker=true;
					break;
				}
				
				++count;
			}
			
			if( in_tracker )
			{
				list<d3server>::const_iterator itr2 = ServerList.begin();
				for(unsigned short num = 0; num!=count; itr2++, num++) ;
			
				int ok=download_mod(itr2->mod, true);
			
				if(ok==1 || ok==2 || ok==3 || ok==4 || ok==6)
				 return;
				
				if( itr2->plr == itr2->plrmax)
				 if( ! question( (Translations->get_help("server_full_title")).c_str(), (Translations->get_help("server_full")).c_str() ))
				  return;

				QString msg("/me joined server ");
				msg.append(itr2->sname.c_str());
				msg.append("     running mission: ");
				msg.append(itr2->missionname.c_str());
				msg.append("     game type: ");
				msg.append(itr2->mod.c_str());
				msg.append("     launch link: d3://");

				int pos = le_directip.text().indexOf("://");
	
				if (pos != -1)
					msg.append(le_directip.text().right(le_directip.text().length() - pos - 3));
				else
					msg.append(le_directip.text());

#ifdef USE_KONVERSATION_PLUGIN	
				m << msg;	
				QDBusConnection::sessionBus().send(m);
#endif

				func_start(&(*itr2));
				tmr_trackerRefresh->stop();
			}
			else
			{
				QString msg("/me joined server via link: d3://");
	
				int pos = le_directip.text().indexOf("://");
	
				if (pos != -1)
					msg.append(le_directip.text().right(le_directip.text().length() - pos - 3));
				else
					msg.append(le_directip.text());
	
#ifdef USE_KONVERSATION_PLUGIN
				m << msg;
				QDBusConnection::sessionBus().send(m);
#endif

				tmr_trackerRefresh->stop();
				func_start(3, le_directip.text().toStdString().c_str());
				QDBusConnection::sessionBus().send(m);
			}
		}
		else
		 return;
	}
	catch(ErrorClass ec)
	{
		msg_box("An error occured!", ec.description());
	}

	unsigned int timeout = atoi(d321go_globals::PlayerOptfile->get_single_entry("tracker_refresh_interval", "d321go_options").c_str());
	
	if (timeout > 0)
		tmr_trackerRefresh->start();
}

void qd321go::set_directip(const string& ip)
{
	remote=true;
	sleep(1); // sleep is needed here (since this is threaded, the user will not notice it)
	le_directip.setText(ip.c_str());
	remote=false;
}

void qd321go::switch_tray(QSystemTrayIcon::ActivationReason reason)
{
	switch (reason) {
		case QSystemTrayIcon::Trigger:
		case QSystemTrayIcon::DoubleClick:
		{
			if(minimized==false)
			{
				minimized=true;
				setVisible(false);
			}
			else
			{
				minimized=false;
				setVisible(true);
			}
		
			break;
		}
		case QSystemTrayIcon::MiddleClick:
		{
			break;
		}
		default:
		{
			int limit = link_list.size();

			for(int count=0; count < limit; ++count)
			{
				if(count > 9)
				 break;
						
				launchlinks[count]->setText(link_list[count].c_str());
				popup.addAction(launchlinks[count]);
			}

			popup.addSeparator();
			popup.addAction(&act_exit);

			popup.exec(QCursor::pos());
			
			break;	
		}
	}
}

void qd321go::show_message(const QString& text)
{
	if(! remote)
	 return;

	char tmp[32];
	strcpy(tmp, text.toStdString().c_str());
	
	if(! strncmp(tmp, "d", 1))
	{
		char v[2];
		strncpy(v, tmp+1, 1);
	
		int version=atoi(v);
		
		if(version == 1)
		{
			int pos = text.indexOf("://");

			switch_d1();
			set_directip(text.right(text.length() - pos - 3).toStdString().c_str());
		}
		else if(version == 2)
		{
			int pos = text.indexOf("://");

			switch_d2();
			set_directip(text.right(text.length() - pos - 3).toStdString().c_str());
		}
		else if(version == 3)
		 switch_d3();

		tray_icon.showMessage(QString::fromLocal8Bit((Translations->get_help("ip_changed_title")).c_str()), QString::fromLocal8Bit((Translations->get_help("ip_changed")).c_str()));
	}
}

void qd321go::start_refresh()
{
	delete gb_main_layout;
	gb_main_layout = NULL;
	
	gb_main_layout_refresh = new QVBoxLayout;
	
	gb_main_layout_refresh->addStretch(1000);
	gb_main_layout_refresh->addWidget(&bar_tracker);
	gb_main_layout_refresh->addStretch(1000);
	
	gb_main.setLayout(gb_main_layout_refresh);
	
	thread.start();
	
	lbl_d.setEnabled(false);
	lbl_3.setEnabled(false);
	lbl_2.setEnabled(false);
	lbl_1.setEnabled(false);
	lbl_go.setEnabled(false);
	
	tracker.hide();
	bar_tracker.show();
	pb_refresh.setEnabled(false);
}

void qd321go::show_tracker()
{
	delete gb_main_layout_refresh;
	gb_main_layout_refresh = NULL;
	
	gb_main_layout = new QHBoxLayout;
	gb_main_layout->addWidget(&tracker);
	gb_main.setLayout(gb_main_layout);
	
	//gb_start.show();
	gb_main_layout->removeWidget(&bar_tracker);
	gb_main_layout->addWidget(&tracker);
	
	lbl_d.setEnabled(true);
	lbl_3.setEnabled(true);
	lbl_2.setEnabled(true);
	lbl_1.setEnabled(true);
	lbl_go.setEnabled(true);

	bar_tracker.hide();
	tracker.show();
	pb_refresh.setEnabled(true);
}

void qd321go::contact_d321go(QAction* action)
{
	set_directip(string(action->text().toStdString().c_str()));
}

void qd321go::logout()
{
	close();
	sleep(1);
	QCoreApplication::exit();
}

void qd321go::showStartupHelp()
{
	sleep(1); //wait until the mainwindow has been built or the bubble won't appear on the tray icon

	//because optfiles don't support newlines, we have to replace every '@' sign with a '\n'
	QString info(QString::fromLocal8Bit((Translations->get_help("first_start_help")).c_str()));
	info.replace("@", "\n");

#ifdef __LINUX__	
	tray_icon.showMessage((Translations->get_help("first_start_help_title")).c_str(), info, QSystemTrayIcon::Information, 1000000);
#elif __WIN32__
	msg_box((Translations->get_help("first_start_help_title")).c_str(), info.toStdString().c_str());
#endif
}
