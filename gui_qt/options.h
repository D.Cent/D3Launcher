/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef OPTIONS_H
#define OPTIONS_H OPTIONS_H

#include <QApplication>
#include <QDialog>
#include <QComboBox>
#include <QCheckBox>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QFileDialog> //also used for directories
#include <QEvent>
#include <QHelpEvent>
#include <QToolTip>

#include <string>
#include <map>
#include <list>
#include <iostream>
using namespace std;

#include <odf.h>
using namespace odf;

#include "str_funcs.h"

#include "../tools/mc_string.h"
#include "../main/globals.h"
using namespace d321go_globals;

class components;

class opt_dlg : public QDialog
{
	Q_OBJECT

	private:
		string section;
		int line_count;
		map<string, string> opt_map;
		int tt_y;

	public:
		QPushButton pb_accept;
		QPushButton pb_cancel;
		
		list<components*> lines; // TODO: remove new!!!
		
		opt_dlg(string _section) : pb_accept(this), pb_cancel(this)
			{
				section=_section;
				setupUi();
                        }
		
		//old desctructor - DO NOT USE!!
		/*~opt_dlg()
		{
			for(list<components*>::iterator itr = lines.begin(); itr != lines.end(); itr++) {
				//delete *itr;
				lines.erase(itr);
			}
		}*/
		
		void setupUi();
		void retranslateUi();
	
	protected:
		bool event(QEvent*);
	
	public slots:
		void apply_changes();
};

class components : public QWidget
{
	Q_OBJECT

	friend class opt_dlg;

	private:
		opt_dlg* parent;
		string left, right;
		ChoiceType ct;
		
		QLabel *lbl;
		QComboBox* cb;
		QCheckBox* ch;
		QLineEdit* le;
		QPushButton* pb;
		
		MultipleChoiceString *mcs;

	public:
		static int count;
		
		components(opt_dlg* _parent, const string& _left, const string& _right) : parent(_parent), left(_left), right(_right)
		{
			parent= _parent;
			
			MultipleChoiceString mcs_type(right.c_str()); //TODO: remove all these strings
			ct = mcs_type.get_type();
			
			if(ct == CHOICE_COMBO)
			{
				char tmp[1024];
				strcpy(tmp, right.c_str());
				StrCutAt(tmp, '|');
				
				mcs = new MultipleChoiceString(tmp);
				
				MultipleChoiceString  mcs_temp(right.c_str());
				
				int num=mcs_type.get_num();
															
				for(short count=1; count != num; ++count)
				 mcs->add_choice(mcs_temp[count].c_str());
			}
			else
			 mcs = new MultipleChoiceString(right.c_str());
						
			int cur_pos = (5+35*count);
			
			//this label can not be freed manually - it will be deleted by QObject
			lbl = new QLabel(parent);
			
			lbl->setGeometry(5, cur_pos, 140, 30);
			lbl->setText(left.c_str());
			
			switch(ct)
			{
				case CHOICE_COMBO:
				{
					cb = new QComboBox(parent);
					cb->setGeometry(150, cur_pos, 140, 30);

					//for some reason that code does not work since i use "new"									
					/*for(short count=0; count != num; ++count)
					 cb->addItem((*mcs)[count].c_str());*/
					
					list<string> combo_entries;
					combo_entries=mcs->get_list();
					
					for(list<string>::iterator itr = combo_entries.begin(); itr != combo_entries.end(); itr++)
					 cb->addItem(itr->c_str()); 
					
					break;
				}

				case CHOICE_FILE:
					le = new QLineEdit(parent);
					pb = new QPushButton(parent);
					
					le->setGeometry(150, cur_pos, 65, 30);
					le->setText((MultipleChoiceString::get_current(right.c_str())).c_str());

					pb->setGeometry(220, cur_pos, 70, 30);
					pb->setText("Choose...");
					
					QObject::connect(pb, SIGNAL(clicked()), this, SLOT(open_file()));
				break;				
				case CHOICE_DIR:
					le = new QLineEdit(parent);
					pb = new QPushButton(parent);
					
					le->setGeometry(150, cur_pos, 65, 30);
					le->setText((MultipleChoiceString::get_current(right.c_str())).c_str());

					pb->setGeometry(220, cur_pos, 70, 30);
					pb->setText("Choose...");
					
					QObject::connect(pb, SIGNAL(clicked()), this, SLOT(open_dir()));
				break;
				
				case CHOICE_LINEEDIT:
					le = new QLineEdit(parent);
					
					le->setGeometry(150, cur_pos, 140, 30);
					le->setText((MultipleChoiceString::get_current(right.c_str())).c_str());
				break;
				
				case CHOICE_CHECKBOX:
					ch = new QCheckBox(parent);
					
					ch->setGeometry(150, cur_pos, 140, 30);
					
					char tmp[2];
					
					strcpy(tmp, string(MultipleChoiceString::get_current(right.c_str())).c_str());
					
					if(strcmp(tmp, "0") == true)				
					 ch->setChecked(true);
					else
					 ch->setChecked(false);
				break;
			}
			count++;
		}
		
		~components()
		{
			delete mcs;
		
			switch(ct)
			{
				case CHOICE_COMBO:
					delete cb;
					break;
				case CHOICE_FILE:
				case CHOICE_DIR:
					delete pb;
					break;
				case CHOICE_LINEEDIT:
					delete le;
					break;
				case CHOICE_CHECKBOX:
					delete ch;
					break;
			}
		}

	public slots:
		inline void open_file() {
			QString file=QFileDialog::getOpenFileName(this, "QD321GO!! - Choose a file...", QDir::homePath());
			if(file == NULL) return;
			le->setText(file);
		}
		
		inline void open_dir() {
			QString dir=QFileDialog::getExistingDirectory(this, "QD321GO!! - Choose a directory...", QDir::homePath());
			if(dir == NULL) return;
			le->setText(dir);
		}
};

#endif // OPTIONS_H
