/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef DIALOGS_H
#define DIALOGS_H DIALOGS_H

#include <QDialog>
#include <QComboBox>
#include <QPushButton>
#include <QMessageBox>
#include <QInputDialog>

#include <list>
#include <string>
using namespace std;

#define msg_box(title, text) QMessageBox::information(NULL, title, QString::fromLocal8Bit(text))

bool question(const char* title, const char* text);
string input_dlg(const char* title, const char* label, const char* text="");

class SelectionDialog : public QDialog
{
	Q_OBJECT
	
	public:
		list<string> plr_list;

		QComboBox cb_plrbox;
		QPushButton pb_accept;
		QPushButton pb_cancel;
		QString title;
		bool end;
		
		SelectionDialog(const char* _title, const list<string>& choice_list) : cb_plrbox(this), pb_accept(this), pb_cancel(this)
                        {
				plr_list=choice_list;
				title=_title;
				end=false;
                                setupUi();
                        }
		
		void setupUi();
		void retranslateUi();

		inline string get_selected_item()
		{
			return string(cb_plrbox.currentText().toStdString().c_str());
		}
		
	public slots:
		int status();
		void abort();
};

#endif // DIALOGS_H
