/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "options.h"
#include "../tools/langfile.h"

int components::count;

void opt_dlg::setupUi()
{
	components::count=0;

	PlayerOptfile->rewind();
	PlayerOptfile->to_map(opt_map, section.c_str());

	int count=5;
	for(map<string, string>::const_iterator itr = opt_map.begin(); itr != opt_map.end(); itr++)
	 count+=35;
	
	count+=35;
	
	resize(295, count);
	setFixedSize(295, count);
	
	count=5;	
	line_count=0;
	
	for(map<string, string>::const_iterator itr = opt_map.begin(); itr != opt_map.end(); itr++)
	{
		lines.push_back( new components(this, itr->first, itr->second) );
		++line_count;
	}
	
	pb_accept.setGeometry(5, 5+(line_count*35), 140, 30);
	pb_accept.setText(QString::fromLocal8Bit((Translations->get_help("options_accept")).c_str()));
	
	pb_cancel.setGeometry(150, 5+(line_count*35), 140, 30);
	pb_cancel.setText(QString::fromLocal8Bit((Translations->get_help("options_cancel")).c_str()));
	
	retranslateUi();
			
	QObject::connect(&pb_cancel, SIGNAL(clicked()), this, SLOT(close()));
	QObject::connect(&pb_accept, SIGNAL(clicked()), this, SLOT(apply_changes()));

	QMetaObject::connectSlotsByName(this);
}
		
void opt_dlg::retranslateUi()
{
	// TODO: translate
	setWindowTitle("D3Launcher - Options");
}

bool opt_dlg::event(QEvent *event)
{
	if (event->type() == QEvent::ToolTip)
	{
		QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);

		tt_y=helpEvent->y() / 35 + 1;
				
		if(tt_y > line_count)
		 return QWidget::event(event);
		
		int count_itr=0;
		QString optline;
		
		for(list<components*>::const_iterator itr = lines.begin(); itr != lines.end(); itr++)
		{
			++count_itr;
			
			if(count_itr != tt_y)
			 continue;
			
			optline = (*itr)->lbl->text();
		}
		
		optline.push_front("_");
		optline.push_front(section.c_str());
		
		QToolTip::showText(helpEvent->globalPos(), QString::fromLocal8Bit((HelpStrings->get_help(optline.toStdString().c_str())).c_str()));
	}
	
	return QWidget::event(event);
}

void opt_dlg::apply_changes()
{
	for(list<components*>::iterator itr = lines.begin(); itr != lines.end(); itr++)
	{
		switch((*itr)->ct)
		{
			case CHOICE_COMBO:
			{
				QString tmp((*itr)->cb->currentText());
			
				(*itr)->mcs->set_current(string(tmp.toStdString().c_str()));
				string str=(*itr)->mcs->get_whole_string();
				
				opt_map[(*itr)->left]=str;
				
				break;
			}
			case CHOICE_FILE:
			{
				string str("f.)");
				str.append((*itr)->le->text().toStdString().c_str());
			
				opt_map[(*itr)->left]=str;
				
				break;
			}
			case CHOICE_DIR:
			{
				string str("d.)");
				str.append((*itr)->le->text().toStdString().c_str());
			
				opt_map[(*itr)->left]=str;
				
				break;
			}
			case CHOICE_LINEEDIT:
			{
				char temp[1024];
				QString str((*itr)->le->text());
				
				strcpy(temp, str.toStdString().c_str());
				
				if(! strncmp(temp, "\0", 1))
				 opt_map[(*itr)->left]=string("");
				else
				 opt_map[(*itr)->left]=string(temp);
				 
				break;
			}
			case CHOICE_CHECKBOX:
			{
				if((*itr)->ch->isChecked())
				{
					string str("b.)");
					str.append("1");					
				
					opt_map[(*itr)->left]=str;
				}
				else
				{
					string str("b.)");
					str.append("0");
				
					opt_map[(*itr)->left]=str;
				}
				
				break;
			}
		}
	}
	
	try {
		PlayerOptfile->from_map(opt_map, section.c_str());
		close();
	} 
	catch(ErrorClass ec) { 
		ec.print_error();
	}
}
