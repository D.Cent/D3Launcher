/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "tracker_d3.h"

#include "qd321go.h"
#include "../network/download_level.h"

#include "../main/globals.h"
using namespace d321go_globals;

#include "../tools/langfile.h"

list<d3server> ServerList; //this list contains the information about all servers
int srv_num; //needed to know which server-info should be displayed

// constructor
ServerTracker::ServerTracker(qd321go *_parent, QGroupBox* gb) : QTableWidget(0, 7, gb), parent(_parent), trackermenu(this),
info_act(tr("&Info"), this), dl_level_act(tr("Download &Level"), this), dl_mod_act(tr("Download &Mod"), this)
{
	// please never ask my why this is necessary - I don't have any idea...
	qRegisterMetaType<Qt::SortOrder>("SortOrder");
	qRegisterMetaType<QList<QPersistentModelIndex>>("QList<QPersistentModelIndex>");
	qRegisterMetaType<QVector<int>>("QVector<int>");
	qRegisterMetaType<QAbstractItemModel::LayoutChangeHint>("LayoutChangeHint");

	QStringList description;
	
	description.insert(0, QString::fromLocal8Bit((Translations->get_help("tracker_name")).c_str()));
	description.insert(1, "Mission");
	description.insert(2, "Mod");
	description.insert(3, QString::fromLocal8Bit((Translations->get_help("tracker_players")).c_str()));
	description.insert(4, QString::fromLocal8Bit((Translations->get_help("tracker_max")).c_str()));
	description.insert(5, "Ping");
	
	setHorizontalHeaderLabels(description);
	
	//make a popup-menu
	trackermenu.addAction(&info_act);
	trackermenu.addAction(&dl_mod_act);
	trackermenu.addAction(&dl_level_act);

	QObject::connect(this, SIGNAL(customContextMenuRequested(const QPoint&)), this, SLOT(exec_tracker(const QPoint&))); // popup menu for clicked cells
	QObject::connect(this, SIGNAL(cellClicked(int, int)), this, SLOT(select_slot(int, int)));			
	QObject::connect(this, SIGNAL(cellDoubleClicked (int, int) ), this, SLOT(take_ip(int, int)));		
	QObject::connect(&info_act, SIGNAL(triggered()), this, SLOT(get_info()));
	QObject::connect(&dl_mod_act, SIGNAL(triggered()), this, SLOT(dl_mod()));
	QObject::connect(&dl_level_act, SIGNAL(triggered()), this, SLOT(dl_level()));
	
	hideColumn(6);

	this->setContextMenuPolicy(Qt::CustomContextMenu);
}
		
void ServerTracker::refresh()
{
	ip_list.clear();

	setSortingEnabled(false);

	get_server_info(&ServerList);

	setRowCount(ServerList.size());
	int count=0;

	QTableWidgetItem *sname;
	QTableWidgetItem *missionname;
	QTableWidgetItem *mod;
	QTableWidgetItem *plr;
	QTableWidgetItem *plrmax;
	QTableWidgetItem *ping;
	QTableWidgetItem *hidden_item;
	
	string tmp_ip;
	
	QBrush red_color;
	red_color.setColor(Qt::red);
	
	QBrush black_color;
	black_color.setColor(Qt::black);
	
	list<string> lvl_list;
	get_installed_levels(&lvl_list);
	
	for(list<d3server>::iterator itr = ServerList.begin(); itr != ServerList.end(); itr++ )
	{
		sname = new QTableWidgetItem(itr->sname.c_str());
		missionname = new QTableWidgetItem(itr->missionname.c_str());
		mod = new QTableWidgetItem(itr->mod.c_str());
		plr = new QTableWidgetItem(int_to_str(itr->plr-1));
		plrmax = new QTableWidgetItem(int_to_str(itr->plrmax-1));
		hidden_item = new QTableWidgetItem(int_to_str(count));
		
		ping = new QTableWidgetItem();
		ping->setData(Qt::DisplayRole, int_to_str((int)itr->ping));
		ping->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		
		missionname->setForeground(red_color);
		
		for (list<string>::const_iterator mn3itr = lvl_list.begin(); mn3itr != lvl_list.end(); ++mn3itr)
		{
			if (! strcasecmp(mn3itr->c_str(), itr->mn3.c_str()))
			{
				missionname->setForeground(black_color);
				break;
			}
		}
		
		if(! mod_is_available(itr->mod))
		 mod->setForeground(red_color);
		
		if(itr->plr-1 >= 1 && itr->plr != itr->plrmax)
		{
			sname->setBackground(QBrush(QColor(255,255,0)));
			missionname->setBackground(QBrush(QColor(255,255,0)));
			mod->setBackground(QBrush(QColor(255,255,0)));
			plr->setBackground(QBrush(QColor(255,255,0)));
			plrmax->setBackground(QBrush(QColor(255,255,0)));
			ping->setBackground(QBrush(QColor(255,255,0)));
		}
		else if(itr->plr == itr->plrmax)
		{
			sname->setBackground(QBrush(QColor(255,165,0)));
			missionname->setBackground(QBrush(QColor(255,165,0)));
			mod->setBackground(QBrush(QColor(255,165,0)));
			plr->setBackground(QBrush(QColor(255,165,0)));
			plrmax->setBackground(QBrush(QColor(255,165,0)));
			ping->setBackground(QBrush(QColor(255,165,0)));
		}
				
		sname->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		missionname->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		mod->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		plr->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		plrmax->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		
		sname->setData(Qt::DisplayRole, itr->sname.c_str());
		missionname->setData(Qt::DisplayRole, itr->missionname.c_str());
		mod->setData(Qt::DisplayRole, itr->mod.c_str());
		plr->setData(Qt::DisplayRole, int_to_str(itr->plr-1));
		plrmax->setData(Qt::UserRole, int_to_str(itr->plrmax-1));
		hidden_item->setData(Qt::DisplayRole, int_to_str(count));
		
		char real_ip[23];
		strcpy(real_ip, itr->ip.c_str());
		
		StrCutAt(real_ip, ':');
				
		setItem(count, 0, sname);
		setItem(count, 1, missionname);
		setItem(count, 2, mod);
		setItem(count, 3, plr);
		setItem(count, 4, plrmax);
		setItem(count, 5, ping);
		setItem(count, 6, hidden_item);
		
		setRowHeight(count, 18);
		++count;
		
		tmp_ip=itr->ip.c_str();
		tmp_ip.append(":");
		tmp_ip.append(int_to_char(itr->hostport));

		ip_list.push_back(tmp_ip);
	}
	
	setSortingEnabled(true);
	sortByColumn(3, Qt::DescendingOrder);
	resizeColumnsToContents();
}

void ServerTracker::retranslateUi()
{
	QStringList description;

	description.insert(0, QString::fromLocal8Bit((Translations->get_help("tracker_name")).c_str()));
	description.insert(1, "Mission");
	description.insert(2, "Mod");
	description.insert(3, QString::fromLocal8Bit((Translations->get_help("tracker_players")).c_str()));
	description.insert(4, QString::fromLocal8Bit((Translations->get_help("tracker_max")).c_str()));
	description.insert(5, "Ping");
	
	setHorizontalHeaderLabels(description);
}

void ServerTracker::exec_tracker(const QPoint& point) {
	if (this->currentItem() == NULL)
	 return;

	Q_UNUSED(point);
	selectRow(this->currentItem()->row());
	trackermenu.exec(QCursor::pos());
}
		
void ServerTracker::select_slot(int a, int b)
{
	Q_UNUSED(b);
	selectRow(a);
}
		
void ServerTracker::take_ip(int a, int b)
{
	Q_UNUSED(a);
	Q_UNUSED(b);

	srv_num = atoi( item(currentRow(), 6)->text().toStdString().c_str() );

	list<d3server>::const_iterator itr = ServerList.begin();
	for(unsigned short num = 0; num!=srv_num; itr++, num++) ;

	QString buf(itr->ip.c_str());
	buf.append(":");
	buf.append(int_to_str(itr->hostport));

	parent->le_directip.setText(buf);
}
	
void ServerTracker::get_info()
{
	srv_num = atoi( item(currentRow(), 6)->text().toStdString().c_str() );
	
	server_info info_dlg;
	info_dlg.show();
	info_dlg.exec();
}

void ServerTracker::dl_level()
{
	srv_num = atoi( item(currentRow(), 6)->text().toStdString().c_str() );
	
	list<d3server>::const_iterator itr = ServerList.begin();
	for(unsigned short num = 0; num!=srv_num; itr++, num++) ;

	try {
		
	if(! download_level_by_mn3(*itr))
	 msg_box("Download", (Translations->get_help("level_exist")).c_str());
	
	} catch (ErrorClass ec) {
		msg_box((Translations->get_help("level_error")).c_str(), ec.description());
		return;
	}
	
	QBrush black_color;
	black_color.setColor(Qt::black);
	
	item(currentRow(), 1)->setForeground(black_color);
	
	unsigned short count = 0;
	
	for(list<d3server>::iterator itr = ServerList.begin(); itr != ServerList.end(); itr++ )
	{
		if(! strcasecmp(item(count, 1)->text().toStdString().c_str(), item(currentRow(), 1)->text().toStdString().c_str()))
		 item(count, 1)->setForeground(black_color);
		
		++count;
	}
}

void ServerTracker::dl_mod()
{
	char to_download[128];
	strcpy(to_download, item(currentRow(), 2)->text().toStdString().c_str());
	
	if(download_mod(to_download) == 0)
	{
		QBrush black_color;
		black_color.setColor(Qt::black);
	
		unsigned short count = 0;

		for(list<d3server>::iterator itr = ServerList.begin(); itr != ServerList.end(); itr++ )
		{
			if(! strcasecmp(item(count, 2)->text().toStdString().c_str(), to_download))
			 item(count, 2)->setForeground(black_color);
			
			++count;
		}
	}
}

void RefreshThread::run()
{
	parent->tracker.refresh();
	emit threadDone();
}
