/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <iostream>
using namespace std;

#include "network/get_servers.h"
#include "str_funcs.h"
#include "server_info.h"

void server_info::setupUi()
{
	resize(350, 400);

	pb_close_window.setText("Close");
	pb_close_window.setGeometry(280, 365, 60, 25);

	tw_plr_info.setGeometry(5, 145, 340, 210);

	QString info_text;
	
	//fill all server-info in a QString and integrate it into the QLabel

	list<d3server>::iterator itr = ServerList.begin();
	for(unsigned short num = 0; num!=srv_num; itr++, num++) ;
	char buf[16];
	
	info_text.append("Name: \t\t");
	info_text.append(itr->sname.c_str());

	info_text.append("\nMission: \t\t");
	info_text.append(itr->missionname.c_str());
	info_text.append(" (");
	info_text.append(itr->mn3.c_str());
	info_text.append(")");
	
	info_text.append("\nLevel #: \t\t");
	sprintf(buf, "%hu", itr->level_num);
	info_text.append(buf);

	info_text.append("\nGametype: \t");
	info_text.append(itr->mod.c_str());

	info_text.append("\nServer-IP: \t");
	info_text.append(itr->ip.c_str());
	
	info_text.append("\nGameport: \t");
	sprintf(buf, "%d", itr->hostport);
	info_text.append(buf);

	info_text.append("\nVersion: \t\t");
	info_text.append(itr->gamever.c_str());

	info_text.append("\nLocation: \t");
	info_text.append(itr->region_name());

	info_text.append("\nPlayers: \t\t");
	sprintf(buf, "%hu out of %hu", itr->plr-1, itr->plrmax-1);
	info_text.append(buf);

	lbl_info.setText(info_text);
	
	//collect player-information and put it into a table
	
	QStringList description;

	description.insert(0, "Player");
	description.insert(1, "Kills");
	description.insert(2, "Deaths");
	description.insert(3, "Team");
	description.insert(4, "Ping");
	
	tw_plr_info.setColumnCount(5);
	tw_plr_info.setHorizontalHeaderLabels(description);

	int count=0;

	tw_plr_info.setRowCount((itr->p).size()-1);

	for(vector<player_info>::const_iterator p_itr = (itr->p).begin(); p_itr != (itr->p).end(); p_itr++)
	{
		if( p_itr->team == -1) // kick the server out of the table
		 continue;
		
		player = new QTableWidgetItem(p_itr->name.c_str());
		
		sprintf(buf, "%hu", p_itr->kills);
		kills = new QTableWidgetItem(buf);
		
		sprintf(buf, "%hu", p_itr->deaths);
		deaths = new QTableWidgetItem(buf);
		
		if(itr->num_teams >= 2)
		{
			sprintf(buf, "%hu", p_itr->team);
		
			if(! strncmp(buf, "0", 1))	
			 team = new QTableWidgetItem("Team Red");
			else if(! strncmp(buf, "1", 1))	
			 team = new QTableWidgetItem("Team Blue");
			else if(! strncmp(buf, "2", 1))	
			 team = new QTableWidgetItem("Team Green");
			else if(! strncmp(buf, "3", 1))	
			 team = new QTableWidgetItem("Team Yellow");
		}
		else
		 team = new QTableWidgetItem("None");
		
		sprintf(buf, "%hu", p_itr->ping);
		ping = new QTableWidgetItem(buf);
				
		tw_plr_info.setItem(count, 0, player);
		tw_plr_info.setItem(count, 1, kills);
		tw_plr_info.setItem(count, 2, deaths);
		tw_plr_info.setItem(count, 3, team);
		tw_plr_info.setItem(count, 4, ping);

		tw_plr_info.setRowHeight(count, 18);
		++count;
	}

	tw_plr_info.setSortingEnabled(true);
	
	retranslateUi();
			
	connect(&pb_close_window, SIGNAL(clicked()), this, SLOT(close()));

	QMetaObject::connectSlotsByName(this);
}
		
void server_info::retranslateUi()
{
	// skip to the elemnet we look for
	list<d3server>::const_iterator itr = ServerList.begin();
	for(unsigned short num = 0; num!=srv_num; itr++, num++) ;
	setWindowTitle(itr->sname.c_str());	
}
