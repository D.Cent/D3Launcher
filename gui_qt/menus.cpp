/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "qd321go.h"
#include "dedicated.h"
#include "menus.h"
#include "options.h"
#include "../callbacks/update.h"
#include "../callbacks/start.h"
#include "../tools/langfile.h"
#include "../network/download_level.h"
#include "../callbacks/player_selection.h"

void D321GOAction::action_clicked() {
	parent->set_last_activated( this );
}

void D321GOMenuBar::apply_menus()
{
	clear();

	for(unsigned int count=0; count<NUM_MENUS; count++)
	 top_menus[count] = new D321GOMenu(this, &cb);

	MenuBarMap.clear();
	
	d321go_globals::LanguageOptfile->to_map(MenuBarMap,"menu_bar");
		
	std::map<string, string>::const_iterator sections = MenuBarMap.begin();
	for(unsigned char i = 0; i<NUM_MENUS; i++, sections++)
	{
		std::map<string, string> MenuMap;
		d321go_globals::LanguageOptfile->to_map(MenuMap, sections->first.c_str());
		
		QString tmp(QString::fromLocal8Bit(sections->second.c_str()));
		top_menus[i]->set_menu(&tmp, &MenuMap);

		addMenu(top_menus[i]);
	}
			
	top_menus[MENU_FILE]->set_callback(FILE_PILOT, MENU_FILE);
	top_menus[MENU_FILE]->set_callback(FILE_UPDATE, MENU_FILE);
	top_menus[MENU_FILE]->set_callback(FILE_QUIT, MENU_FILE);
			
	top_menus[MENU_START]->set_callback(START_TS, MENU_START);
	
	top_menus[MENU_OPTIONS]->set_callback(OPT_D321GO, MENU_OPTIONS);
	top_menus[MENU_OPTIONS]->set_callback(OPT_BROWSER, MENU_OPTIONS);
	top_menus[MENU_OPTIONS]->set_callback(OPT_DIRS, MENU_OPTIONS);
	//top_menus[MENU_OPTIONS]->set_callback(OPT_CHAT, MENU_OPTIONS);
	top_menus[MENU_OPTIONS]->set_callback(OPT_PREFIXES, MENU_OPTIONS);
	top_menus[MENU_OPTIONS]->set_callback(OPT_COMMANDS, MENU_OPTIONS);
	top_menus[MENU_OPTIONS]->set_callback(OPT_POSTFIXES, MENU_OPTIONS);
			
	top_menus[MENU_EXTRAS]->set_callback(EXTRAS_LEVEL, MENU_EXTRAS);
	//top_menus[MENU_EXTRAS]->set_callback(EXTRAS_CALENDAR, MENU_EXTRAS);
	top_menus[MENU_EXTRAS]->set_callback(EXTRAS_START_DEDICATED, MENU_EXTRAS);
	
	top_menus[MENU_HELP]->set_callback(HELP_ABOUT, MENU_HELP);
	top_menus[MENU_HELP]->set_callback(HELP_MANUAL, MENU_HELP);
	
	// add browser menubar seperately

	for(map<string, string>::const_iterator itr = browser_map.begin(); itr!=browser_map.end(); itr++)
	{
		D321GOAction* tmp_action = new D321GOAction(itr->first.c_str(), top_menus[MENU_BROWSER]); 
		top_menus[MENU_BROWSER]->addAction(tmp_action);
		QObject::connect(tmp_action, SIGNAL(triggered()), &cb, SLOT(browser_start()));
	}
}

void D321GOMenu::set_menu(QString *caption, const std::map<string,string>* entries)
{
	setTitle(*caption);

	std::map<string, string>::const_iterator itr = entries->begin();
	
	for(unsigned char i = 0; itr != entries->end(); i++, itr++)
	{
		if(i==MENU_ITEM_SIZE)
		 throw NEW_ERROR("language file has too many entries!",0); // TODO: 0

		elems[i]->setText(QString::fromLocal8Bit(itr->second.c_str()));				

#ifdef __WIN32__
		if( strncmp(itr->second.c_str(), "Browser", 7)) // hide browser menu on windows
#endif
		addAction(elems[i]);
	}
}

void D321GOMenu::set_callback(char menu_num, char section)
{
	if(section == MENU_FILE)
	{		
		if(FILE_PILOT == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(file_pilot()));
		if(FILE_UPDATE == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(file_update()));
		if(FILE_QUIT == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(file_quit()));
	}
	
	if(section == MENU_START)
	{
		if(START_TS == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(start_ts()));
	}
	
	if(section == MENU_OPTIONS)
	{
		if(OPT_D321GO == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_d321go()));
		if(OPT_BROWSER == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_browser()));
		if(OPT_DIRS == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_dirs()));
		//if(OPT_CHAT == menu_num)
		//QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_chat()));
		if(OPT_PREFIXES == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_prefixes()));
		if(OPT_COMMANDS == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_commands()));
		if(OPT_POSTFIXES == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(opt_postfixes()));
	}
	
	if(section == MENU_EXTRAS)
	{
		if(EXTRAS_LEVEL == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(extras_level()));
		//if(EXTRAS_CALENDAR == menu_num)
		//QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(extras_calendar()));
		if(EXTRAS_START_DEDICATED == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(start_dedicated()));
	}
	
	if(section == MENU_HELP)
	{
		if(HELP_ABOUT == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(help_about()));
		if(HELP_MANUAL == menu_num)
		QObject::connect(elems[(unsigned char)menu_num], SIGNAL(triggered()), cb, SLOT(help_manual()));
	}
}

void CallBacks::file_pilot()
{
	player_selection_dialog();
	
	mw_pointer->menu_bar.apply_menus();
	mw_pointer->retranslateUi();
}

void CallBacks::file_update()
{
	update_d321go();
}

void CallBacks::file_quit()
{
	mw_pointer->close();
}

void CallBacks::start_ts()
{
	start_teamspeak();
}

void CallBacks::browser_start()
{
	try {

	OptFile browser_of((d321go_globals::D321GOPaths->sharepath + "links.txt").c_str(), false, "rb");

	string link(browser_of.get_single_entry(mw_pointer->menu_bar.get_menu(MENU_BROWSER)->get_last_activated()));
#ifdef __WIN32__
	d321go_execvp("", link.c_str());
#else
	string browser_name = g::PlayerOptfile->get_single_entry("linkbrowser","browsers");
	d321go_execvp("", (MultipleChoiceString::get_current(browser_name) + " " + link).c_str());
#endif
	}
	catch(ErrorClass ec)
	{
		ec.print_error();
	}
}

void CallBacks::opt_d321go()
{
	unsigned int timeout_before = atoi(d321go_globals::PlayerOptfile->get_single_entry("tracker_refresh_interval", "d321go_options").c_str());
	
	opt_dlg dlg(string("d321go_options"));

	dlg.show();
	dlg.exec();
	
	update_language();

	mw_pointer->menu_bar.apply_menus();
	mw_pointer->retranslateUi();
	
	unsigned int timeout_after = atoi(d321go_globals::PlayerOptfile->get_single_entry("tracker_refresh_interval", "d321go_options").c_str());
	
	// automatic tracker refresh
	if (timeout_before != timeout_after && timeout_after > 0)
	{
		mw_pointer->tmr_trackerRefresh->start(1000*60*timeout_after);
		mw_pointer->start_refresh();
	}
	else if (timeout_after == 0)
	{
		mw_pointer->tmr_trackerRefresh->stop();
	}
}

void CallBacks::opt_browser()
{
	opt_dlg dlg(string("browsers"));

	dlg.show();
	dlg.exec();
}

void CallBacks::opt_dirs()
{
	opt_dlg dlg(string("d321go_dirs"));

	dlg.show();
	dlg.exec();
}

/*void CallBacks::opt_chat()
{
	opt_dlg dlg(string("chat"));

	dlg.show();
	dlg.exec();
}*/

void CallBacks::opt_prefixes()
{
	opt_dlg dlg(string("dx_prefixes"));

	dlg.show();
	dlg.exec();
}

void CallBacks::opt_commands()
{
	opt_dlg dlg(string("dx_commands"));

	dlg.show();
	dlg.exec();
}

void CallBacks::opt_postfixes()
{
	opt_dlg dlg(string("dx_postfixes"));

	dlg.show();
	dlg.exec();
}

void CallBacks::extras_level()
{
	string level_url = input_dlg("Level Download", (Translations->get_help("lvl_download_headline")).c_str(), (Translations->get_help("lvl_download_http")).c_str());
	
	if(level_url.empty())
	 return;

	download_level_by_url(level_url.c_str());
}

//next release
/*void CallBacks::extras_calendar()
{
	puts("extras->calendar");
}*/

void CallBacks::start_dedicated()
{
	dedi_starter dlg;
	
	dlg.show();
	dlg.exec();
}

void CallBacks::help_about()
{
	//because optfiles don't support newlines, we have to replace every '@' sign with a '\n'

	QString info((Translations->get_help("aboutdlg_qt")).c_str());
	info.replace("@", "\n");
	
	msg_box((Translations->get_help("aboutdlg_title")).c_str(), info.toStdString().c_str());
}

void CallBacks::help_manual()
{
	puts("help->manual");
}
