/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <QApplication>

#include "dialogs.h"

void SelectionDialog::setupUi()
{
	resize(150, 90);

	cb_plrbox.setGeometry(10, 10, 130, 30);

	for(list<string>::const_iterator itr = plr_list.begin(); itr != plr_list.end(); itr++)
	{
		string tmp(*itr);
		cb_plrbox.addItem(tmp.c_str());
	}
	
	pb_accept.setGeometry(10, 50, 60, 30);
	pb_accept.setText("Accept");
	
	pb_cancel.setGeometry(80, 50, 60, 30);
	pb_cancel.setText("Cancel");
		
	retranslateUi();
			
	QObject::connect(&pb_cancel, SIGNAL(clicked()), this, SLOT(abort()));
	QObject::connect(&pb_accept, SIGNAL(clicked()), this, SLOT(close()));

	QMetaObject::connectSlotsByName(this);

	exec();
}
		
void SelectionDialog::retranslateUi()
{
	setWindowTitle(title.toStdString().c_str());
}

int SelectionDialog::status()
{
	if(end==true)
	 return false;
	 
	return true;
}

void SelectionDialog::abort()
{
	end=true;
	close();
}

bool question(const char* title, const char* text)
{
        QMessageBox dlg;

        dlg.setIcon(QMessageBox::Question);

        dlg.setWindowTitle(title);
        dlg.setText(QString::fromLocal8Bit(text));
        dlg.addButton("Yes", QMessageBox::YesRole); // returns 0
        dlg.addButton("No", QMessageBox::AcceptRole); //returns 1

        int ret_value=dlg.exec();

	return (ret_value==0)?true:false;
}

string input_dlg(const char* title, const char* label, const char* text)
{
	bool ok;
	QString input = QInputDialog::getText(NULL, title, QString::fromLocal8Bit(label), QLineEdit::Normal, QString::fromLocal8Bit(text), &ok);

	string ret_value;

	if (ok && !input.isEmpty())
	 ret_value = string(input.toStdString().c_str());
	
	return ret_value;
}
