/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QCheckBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QRadioButton>
#include <QSpinBox>
#include <QDialog>
#include <QFileDialog>

#include <odf.h>
using namespace odf;

#ifndef DEDICATED_H
#define DEDICATED_H DEDICATED_H

class dedi_starter : public QDialog
{
	Q_OBJECT

	public:
		QGroupBox gb_tracker;
		QCheckBox tracker_cx;
		QGroupBox gb_servertype;
		QRadioButton rb_cs;
		QRadioButton rb_p2p;
		QLineEdit le_pwd;
		QLabel lbl_pwd;
		QGroupBox gb_ships;
		QCheckBox ships_gl;
		QCheckBox ships_phoenix;
		QCheckBox ships_magnum;
		QCheckBox ships_bp;
		QGroupBox gb_mission;
		QLineEdit le_mission;
		QPushButton mission_choose;
		QGroupBox gb_mod;
		QLineEdit le_mod;
		QPushButton mod_choose;
		QLineEdit le_name;
		QLabel lbl_name;
		QLineEdit le_motd;
		QLabel lbl_motd;
		QLabel lbl_goal;
		QLabel lbl_time;
		QLabel lbl_respawn;
		QLabel lbl_pps;
		QLabel lbl_maxplayers;
		QLabel lbl_audio;
		QSpinBox le_goal;
		QSpinBox le_time;
		QSpinBox le_respawn;
		QSpinBox le_pps;
		QSpinBox le_maxplayers;
		QSpinBox le_audio;
		QPushButton pb_abort;
		QPushButton pb_start;

		dedi_starter() : gb_tracker(this), tracker_cx(&gb_tracker), gb_servertype(this), 							 rb_cs(&gb_servertype), rb_p2p(&gb_servertype), le_pwd(this), lbl_pwd(this), gb_ships(this), 							 ships_gl(&gb_ships), ships_phoenix(&gb_ships), ships_magnum(&gb_ships), ships_bp(&gb_ships), 							 gb_mission(this), le_mission(&gb_mission), mission_choose(&gb_mission), gb_mod(this), le_mod(&gb_mod), 					 mod_choose(&gb_mod), le_name(this), lbl_name(this), le_motd(this), lbl_motd(this), lbl_goal(this), 						 lbl_time(this), lbl_respawn(this), lbl_pps(this), lbl_maxplayers(this), lbl_audio(this), le_goal(this), 					 le_time(this), le_respawn(this), le_pps(this), le_maxplayers(this), le_audio(this), pb_abort(this), 						 pb_start(this)
                      		 {
                                 	setupUi(this);
                        	 }

		void setupUi(QDialog *dedi_starter);
		void retranslateUi(QDialog *dedi_starter);
		
	public slots:
		void insert_mission();
		void insert_mod();
		
		void process_file();
};

#endif // DEDICATED_H
