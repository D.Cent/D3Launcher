/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef MENUS_H
#define MENUS_H MENUS_H

#include <string>
#include <map>
using namespace std;

#include <QAction>
#include <QMenu>
#include <QMenuBar>

#include "../main/globals.h"
#include "../callbacks/menu_enums.h"
namespace g=d321go_globals;

const size_t NUM_MENUS = 6;
const size_t MENU_ITEM_SIZE = 30;

class qd321go;

class CallBacks : public QObject
{
	Q_OBJECT
	private:
		qd321go* mw_pointer;
	
	public:
		void set_mw(qd321go* mw) { mw_pointer = mw; }
	
	public slots:
		void file_pilot();
		void file_update();
		void file_quit();
		void start_ts();
		void browser_start();
		void opt_d321go();
		void opt_browser();
		void opt_dirs();
		//void opt_chat();
		void opt_prefixes();
		void opt_commands();
		void opt_postfixes();
		void extras_level();
		//void extras_calendar();
		void start_dedicated();
		void help_about();
		void help_manual();
};

class D321GOMenu;

class D321GOAction : public QAction
{
	Q_OBJECT;
	
	protected:
		D321GOMenu* parent;
	public:	
		D321GOAction(const char* text, D321GOMenu* _parent) : QAction(text, (QObject*)_parent), parent(_parent) {
			QObject::connect(this, SIGNAL(triggered()), this, SLOT(action_clicked()));
		}
		D321GOAction(D321GOMenu* _parent) : QAction((QObject*)_parent), parent(_parent) {
			QObject::connect(this, SIGNAL(triggered()), this, SLOT(action_clicked()));
		}
	
	public slots:
		void action_clicked();
};

class D321GOMenu : public QMenu // inherited by the buttons in the menu bar that open the menus
{
	Q_OBJECT
	public:
		D321GOMenu( QWidget* parent, CallBacks* _cb ) : QMenu( parent ), cb(_cb) {
			for(unsigned int count=0; count<MENU_ITEM_SIZE; count++)
				elems[count] = new D321GOAction(this);
		}

		~D321GOMenu() { 
			for(unsigned int count=0; count<MENU_ITEM_SIZE; count++)
				delete elems[count];
		}

		void set_callback(char menu_num, char section);
		void set_menu(QString *caption, const std::map<string,string>* entries);
		
		void set_last_activated( D321GOAction *_last_action ) {
			last_activated = _last_action->text().toStdString().c_str();
		}
		
		const char* get_last_activated( void ) const {
			return last_activated.c_str();
		}
		
	private:
		//D321GOAction* last_action;
		string last_activated;
		D321GOAction* elems[MENU_ITEM_SIZE];
		CallBacks* cb;
};

class D321GOMenuBar : public QMenuBar
{
	Q_OBJECT
	private:
		std::map<string, string> MenuBarMap; // WTF? namespace std is unknown here? TODO!
		D321GOMenu *top_menus[NUM_MENUS]; // the full menus
		CallBacks cb;
		map<string, string> browser_map;
		OptFile browser_of;
		
	public:
	
		D321GOMenuBar( qd321go* parent ) : QMenuBar((QWidget*)parent), browser_of((d321go_globals::D321GOPaths->sharepath + "links.txt").c_str(), false, "rb")
		{
			browser_of.to_map(browser_map);
			cb.set_mw(parent);
			apply_menus();
		}
		
		~D321GOMenuBar() noexcept {
			for(unsigned int count=0; count<NUM_MENUS; count++)
			 delete top_menus[count];
		}
		
		inline const D321GOMenu* get_menu(menu_bar _menu_num) const {
			return top_menus[_menu_num];
		}
		void apply_menus();
};

#endif // MENUS_H

