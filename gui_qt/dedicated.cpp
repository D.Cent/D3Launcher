/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "dedicated.h"
#include "../main/globals.h"
#include "str_funcs.h"
#include "../callbacks/start.h"
#include "../tools/langfile.h"
using namespace d321go_globals;

void dedi_starter::setupUi(QDialog *dedi_starter)
{
	resize(292, 578);
	
	gb_tracker.setGeometry(QRect(10, 10, 131, 91));

	tracker_cx.setGeometry(QRect(10, 60, 111, 23));
	tracker_cx.setChecked(true);
	
	gb_servertype.setGeometry(QRect(150, 190, 131, 91));
	
	rb_cs.setGeometry(QRect(10, 30, 101, 25));
	rb_cs.setChecked(true);
	
	rb_p2p.setGeometry(QRect(10, 60, 101, 25));
	
	le_pwd.setGeometry(QRect(10, 120, 131, 26));
	le_pwd.setEchoMode(QLineEdit::Password);
	
	lbl_pwd.setGeometry(QRect(10, 105, 121, 20));
	
	gb_ships.setGeometry(QRect(10, 145, 131, 151));
	
	ships_gl.setGeometry(QRect(10, 30, 111, 23));
	
	ships_phoenix.setGeometry(QRect(10, 60, 111, 23));
	
	ships_magnum.setGeometry(QRect(10, 90, 111, 23));
	
	ships_bp.setGeometry(QRect(10, 120, 111, 23));
	
	gb_mission.setGeometry(QRect(150, 10, 131, 80));
	
	le_mission.setGeometry(QRect(10, 20, 111, 26));
	
	mission_choose.setGeometry(QRect(10, 50, 111, 25));
	
	gb_mod.setGeometry(QRect(150, 100, 131, 80));
	
	le_mod.setGeometry(QRect(10, 20, 111, 26));

	mod_choose.setGeometry(QRect(10, 50, 111, 25));

	le_name.setGeometry(QRect(150, 300, 131, 26));

	lbl_name.setGeometry(QRect(20, 300, 121, 26));
	lbl_name.setLayoutDirection(Qt::RightToLeft);

	le_motd.setGeometry(QRect(150, 330, 131, 26));

	lbl_motd.setGeometry(QRect(20, 330, 121, 26));
	lbl_motd.setLayoutDirection(Qt::RightToLeft);

	lbl_goal.setGeometry(QRect(20, 360, 121, 26));
	lbl_goal.setLayoutDirection(Qt::RightToLeft);

	lbl_time.setGeometry(QRect(20, 390, 121, 26));
	lbl_time.setLayoutDirection(Qt::RightToLeft);

	lbl_respawn.setGeometry(QRect(20, 420, 121, 26));
	lbl_respawn.setLayoutDirection(Qt::RightToLeft);

	lbl_pps.setGeometry(QRect(20, 450, 121, 26));
	lbl_pps.setLayoutDirection(Qt::RightToLeft);

	lbl_maxplayers.setGeometry(QRect(20, 480, 121, 26));
	lbl_maxplayers.setLayoutDirection(Qt::RightToLeft);

	lbl_audio.setGeometry(QRect(20, 510, 121, 26));
	lbl_audio.setLayoutDirection(Qt::RightToLeft);

	le_goal.setGeometry(QRect(150, 360, 131, 26));
	le_goal.setMinimum(1);
	le_goal.setMaximum(32000);
	le_goal.setValue(100);

	le_time.setGeometry(QRect(150, 390, 131, 26));
	le_time.setMinimum(1);
	le_time.setMaximum(1000);
	le_time.setValue(120);

	le_respawn.setGeometry(QRect(150, 420, 131, 26));
	le_respawn.setMinimum(1);
	le_respawn.setMaximum(60);
	le_respawn.setValue(5);

	le_pps.setGeometry(QRect(150, 450, 131, 26));
	le_pps.setMinimum(1);
	le_pps.setMaximum(12);
	le_pps.setValue(10);

	le_maxplayers.setGeometry(QRect(150, 480, 131, 26));
	le_maxplayers.setMinimum(2);
	le_maxplayers.setMaximum(32);
	le_maxplayers.setValue(11);

	le_audio.setGeometry(QRect(150, 510, 131, 26));
	le_audio.setMinimum(4);
	le_audio.setMaximum(10);
	le_audio.setValue(4);

	pb_abort.setGeometry(QRect(10, 540, 131, 31));

	pb_start.setGeometry(QRect(154, 540, 131, 31));

	retranslateUi(dedi_starter);
	QObject::connect(&pb_abort, SIGNAL(clicked()), this, SLOT(close()));
	QObject::connect(&pb_start, SIGNAL(clicked()), this, SLOT(process_file()));

	QObject::connect(&mission_choose, SIGNAL(clicked()), this, SLOT(insert_mission()));
	QObject::connect(&mod_choose, SIGNAL(clicked()), this, SLOT(insert_mod()));

	QMetaObject::connectSlotsByName(this);
}

void dedi_starter::retranslateUi(QDialog *dedi_starter)
{
	setWindowTitle(Translations->get_help("dedicated_title").c_str());

	// TODO: re-work this
	gb_tracker.setTitle("Tracker");
	tracker_cx.setText("Descent.CX");
	gb_servertype.setTitle("Server-type");
	rb_cs.setText("cs-server");
	rb_p2p.setText("peer2peer");
	lbl_pwd.setText("Remote Password:");
	gb_ships.setTitle("Disallowed ships");
	ships_gl.setText("Pyro-GL");
	ships_phoenix.setText("Phoenix");
	ships_magnum.setText("Magnum-AHT");
	ships_bp.setText("Black Pyro");
	gb_mission.setTitle("Mission");
	mission_choose.setText("Choose...");
	gb_mod.setTitle("Mod");
	mod_choose.setText("Choose...");
	le_name.setText("Dedicated Server!");
	lbl_name.setText("Name");
	le_motd.setText("Prepare for Descent!!");
	lbl_motd.setText("MOTD");
	lbl_goal.setText("Goallimit/Killgoal");
	lbl_time.setText("Timelimit");
	lbl_respawn.setText("Respawntime");
	lbl_pps.setText("Packets Per Second");
	lbl_maxplayers.setText("Max Players");
	lbl_audio.setText("Audiotaunt Delay");
	pb_abort.setText(Translations->get_help("dedicated_abort").c_str());
	pb_start.setText(Translations->get_help("dedicated_start").c_str());

	// TODO: is this necessary?
	Q_UNUSED(dedi_starter);
}

void dedi_starter::insert_mission()
{
#ifdef __LINUX__
	CommonPath<D3HomePath> dir("missions");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string dir(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	dir.append("\\missions\\");
#endif

	QString	filename = QFileDialog::getOpenFileName(
						this, 
						"Open MN3-file", dir.c_str(), 
						"Descent3 Missions (*.mn3 *.MN3)"
					);

	char turned_string[1024];
	strcpy(turned_string, (turn_string(string(filename.toStdString().c_str()))).c_str());

	StrCutAt(turned_string, DIR_SEP);

	le_mission.setText((turn_string(turned_string)).c_str());
}

void dedi_starter::insert_mod()
{
#ifdef __LINUX__
	CommonPath<D3HomePath> dir("netgames");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string dir(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	dir.append("\\netgames\\");
#endif

	QString filename = QFileDialog::getOpenFileName(
						this, 
						"Open D3M-file", dir.c_str(), 
						"Descent3 Mods (*.d3m *.D3M)"
					);
	
	char turned_string[1024];
	strcpy(turned_string, (turn_string(string(filename.toStdString().c_str()))).c_str()+4);

	StrCutAt(turned_string, DIR_SEP);

	le_mod.setText((turn_string(turned_string)).c_str());
}

void dedi_starter::process_file()
{
	const char srv_conf[] = "server config file";

	try
	{
		remove((d321go_globals::D321GOPaths->homepath + "Dedicated.cfg").c_str());
		remove((d321go_globals::D321GOPaths->homepath + "gamespy.cfg").c_str());
		remove((d321go_globals::D321GOPaths->homepath + "multisettings.mps").c_str());

		FILE* fp;
		fp = t_fopen((d321go_globals::D321GOPaths->homepath + "Dedicated.cfg").c_str(), "wb");
		fprintf(fp, "[server config file]\n");
		t_fclose(fp);

		RegFile of((d321go_globals::D321GOPaths->homepath + "Dedicated.cfg").c_str(), "r+b");
#ifdef __LINUX__
		of.add_single_entry("MultiSettingsFile", (d321go_globals::D321GOPaths->homepath + "multisettings.mps").c_str(), srv_conf);
#elif __WIN32__
		char old_cwd[1024];
		getcwd(old_cwd, 1024);

		string tmp("\"");
		tmp.append(old_cwd);
		tmp.append((d321go_globals::D321GOPaths->homepath + "multisettings.mps").c_str());
		tmp.append("\"");
#endif

		if(strlen(le_pwd.text().toStdString().c_str()) >= 2)
		{
			of.add_single_entry("AllowRemoteConsole", "1", srv_conf);
			of.add_single_entry("ConsolePassword", le_pwd.text().toStdString().c_str(), srv_conf);
			of.add_single_entry("RemoteConsolePort", "3000", srv_conf);
		}
		
		of.add_single_entry("MissionName", le_mission.text().toStdString().c_str(), srv_conf);
		of.add_single_entry("Scriptname", le_mod.text().toStdString().c_str(), srv_conf);
		
		if(rb_p2p.isChecked())
		 of.add_single_entry("Peer2Peer", "1", srv_conf);
		
		of.add_single_entry("GameName", le_name.text().toStdString().c_str(), srv_conf);
		of.add_single_entry("MOTD", le_motd.text().toStdString().c_str(), srv_conf);

		of.add_single_entry("KillGoal", int_to_char(le_goal.value()), srv_conf);
		of.add_single_entry("TimeLimit", int_to_char(le_time.value()), srv_conf);
		of.add_single_entry("RespawnTime", int_to_char(le_respawn.value()), srv_conf);
		of.add_single_entry("PPS", int_to_char(le_pps.value()), srv_conf);
		of.add_single_entry("MaxPlayers", int_to_char(le_maxplayers.value()), srv_conf);
		of.add_single_entry("AudioTauntDelay", int_to_char(le_audio.value()), srv_conf);
		
		of.add_single_entry("ConnectionName", "Direct TCP~IP", srv_conf);
		
		FILE* gspy_file;
		gspy_file=t_fopen((d321go_globals::D321GOPaths->homepath + "gamespy.cfg").c_str(), "wb");
		
		if(tracker_cx.isChecked())
		 fprintf(gspy_file, "0\ntracker.descent.cx:27900\n");
		
		t_fclose(gspy_file);
		
		FILE* multisettings;
		multisettings=t_fopen((d321go_globals::D321GOPaths->homepath + "multisettings.mps").c_str(), "wb");
		
		if(ships_gl.isChecked())
		 fprintf(multisettings, "ShipBan   Pyro-GL\n");
				
		if(ships_phoenix.isChecked())
		 fprintf(multisettings, "ShipBan   Phoenix\n");
				
		if(ships_magnum.isChecked())
		 fprintf(multisettings, "ShipBan   Magnum-AHT\n");
				
		if(ships_bp.isChecked())
		 fprintf(multisettings, "ShipBan   Back Pyro\n");
		
		t_fclose(multisettings);
		
		start_dedicated_server((d321go_globals::D321GOPaths->homepath + "Dedicated.cfg"),
				(d321go_globals::D321GOPaths->homepath + "gamespy.cfg"));
		
		close();
	}
	catch(ErrorClass ec)
	{
		ec.print_error();
	}
}
