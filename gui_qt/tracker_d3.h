/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef TRACKER_D3_H
#define TRACKER_D3_H TRACKER_D3_H

#include <QApplication>
#include <QAction>
#include <QMenu>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QVariant>
#include <QMouseEvent>
#include <QMainWindow>
#include <QThread>
#include <QGroupBox>

#include "d321go.h"
#include "str_funcs.h"
#include "server_info.h"
#include "../network/download_mod.h"

class qd321go;

class ServerTracker : public QTableWidget
{
	Q_OBJECT
	
	private:
		qd321go* parent;
		QMenu trackermenu;
		QAction info_act;
		QAction dl_level_act;
		QAction dl_mod_act;
		
	public:	
		ServerTracker(qd321go *parent, QGroupBox* gb);
		void retranslateUi();
		list<string> ip_list;
		
		static void* thread_start(void* p);

	public slots:
		void exec_tracker(const QPoint& point);
		void refresh();
		void select_slot(int a, int b);
		void take_ip(int a, int b);
		void get_info();
		void dl_mod();
		void dl_level();
};

class RefreshThread : public QThread
{
	Q_OBJECT

	private:
		qd321go* parent;

	public:
		RefreshThread(qd321go* _parent) { parent = _parent; }
		virtual void run();
	signals:
		void threadDone(); 
};

#endif // TRACKER_D3_H
