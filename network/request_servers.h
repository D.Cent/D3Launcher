/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file request_servers.h
	\brief This file contains the recv_answers() function and some constants for it.

	\note If you don't need to know details about this function, there is no need for you
	to look in the following files
	- request_servers.cpp
	- d3_server.h
	- d3_server.cpp
*/

#ifndef REQUEST_SERVERS_H
#define REQUEST_SERVERS_H REQUEST_SERVERS_H

const unsigned char TRIES_MAX_SEC = 20; //!< Number of tries, e.g. of select() looking for reponses.
const unsigned int TRIES_DELTA_USEC = 100 * 1000; //!< Time between two tries.

const unsigned char SECONDS_NO_RESPONSE = 5; //!< Number of seconds of no response we abort.

#ifdef __LINUX__
/**
	\brief Timeout in seconds to compute a ping.
	\note The timeout does not affect the ping computation and should not be of any interest.
	Make it high to let the CPU chill.
*/
 const unsigned int PING_SELECT_TIMEOUT_SEC = 1;
 const unsigned int PING_SELECT_TIMEOUT_USEC = 0; //!< Timeout in (1/1000)seconds to compute a ping.
#else
/**
	\brief Timeout in seconds to compute a ping.
	\note The timeout does not affect the ping computation and should not be of any interest.
	Make it high to let the CPU chill.
*/
 const unsigned int PING_SELECT_TIMEOUT_SEC = 0;
 const unsigned int PING_SELECT_TIMEOUT_USEC = 100; //!< Timeout in (1/1000)seconds to compute a ping.
#endif

const unsigned int USEC_AFTER_REQ_1 = 1000; //!< value to be passed to D3Server struct, look there for docs.
const unsigned int USEC_AFTER_REQ_2 = 1000; //!< value to be passed to D3Server struct, look there for docs.

const unsigned char TRIES_NO_RESPONSE = (1000000 / TRIES_DELTA_USEC) * SECONDS_NO_RESPONSE; //!< Never change.
const unsigned char TRIES_MAX = (1000000 / TRIES_DELTA_USEC) * TRIES_MAX_SEC;  //!< Never change.

/**
	Sends requests to the servers in ServerList and adds all information to the d3server structs.
	(IP and port must be given in each d3server struct before calling this function!)
	@return Number of servers that did not respond.
*/
unsigned short recv_answers(list<d3server> *ServerList);

#endif // REQUEST_SERVERS_H

