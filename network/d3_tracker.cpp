/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <odf.h>
using namespace odf;

#include "server_structs.h"
#include "d3_tracker.h"

#include "../main/globals.h"

bool get_d3cx_list(list<d3server> *ServerList)
{

	try {
	
		{
			HTTPClient cxclient("d3.descent.cx");
			cxclient.GET("d3cxraw.d3?terse=y", "d3cxraw.xml", d321go_globals::D321GOPaths->homepath.c_str());
		}

		string d3cxpath(d321go_globals::D321GOPaths->homepath.c_str());
		d3cxpath.append("/d3cxraw.xml");
		 
		FILE* fp = t_fopen(d3cxpath.c_str(), "rb");	
		seek_after_str(fp, "<pre>");
		
		while(1)
		{
			d3server cur_server;
				
			char tmp[64];
			
			if(fgets(tmp, 64, fp) == NULL)
			 msg_box("Error", "Unexpected end of d3cxraw.xml file.");
			
			strcpy(strchr(tmp,'\n'),"\0");
				
			if( ! strchr(tmp,':')) {
				puts("get_d3cx_list(): Error: missing ':'!");
				exit(1);
			}
		
			cur_server.ip.assign(tmp, (char* )strchr(tmp, ':') - tmp);
			
			if(cur_server.ip == "DEBUG") {
#ifdef D321GO_DEBUG
				puts("get_d3cx_list(): done");
#endif
				break;
			}
			
			cur_server.port = atoi(strchr(tmp, ':')+1);
			
			ServerList->push_back(cur_server);
			
		}
			
		t_fclose(fp);
		
		remove(d3cxpath.c_str());
			
		for(list<d3server>::const_iterator itr = ServerList->begin(); itr!=ServerList->end(); itr++)
		 printf("ip: %s, port:%d\n",itr->ip.c_str(),itr->port);
	}
	catch (ErrorClass ec)
	{
		puts("get_d3cx_list(): Error receiving data from d3.descent.cx");
		ec.print_error();
		ServerList->clear();
		return false;
	}
	return true;
}
