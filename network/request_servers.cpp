/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	@file request_servers.cpp
	\brief Implementation of recv_answers().
	
	This file contains very complicated code. Carefully look at the comments
	in the code to understand what's going on. Also, note that this code only uses
	- the d3server struct (which itself uses no further netcode)
	- the D3Server class (which itself only uses the d3server code)
*/

#include <cstdlib>
#include <string>
#include <list>
using namespace std;

#include <odf.h>

#include "server_structs.h"

#include "request_servers.h"

#include "d3_server.h"



bool thread_done = false; //!< Will be set to true when all answers are handled or timeout occured.

/****************************************************
*
*	HELPER FUNCTIONS FOR recv_answers()
*
*/

/**
	Helper function to call select() again. Restores a file descriptor set.
	@param _fds The file descriptor set, as it is used by select().
	@param ServerVector The D3Server list. Needed to tell FD_SET() the socket ids.
	@return Returns the highest file descriptor in this set, as it is needed by select().
*/
int restore_fds(fd_set* _fds, const list<D3Server*>* ServerVector)
{
	FD_ZERO( _fds);
	int highest_fd = 0;
	for(list<D3Server*>::const_iterator itr = ServerVector->begin(); itr!=ServerVector->end(); itr++)
	{
		if((*itr)->get_status() != STATUS_ALL_DONE) {
			FD_SET((*itr)->ServerSocket->socket_id(), _fds);
			highest_fd = _MAX(highest_fd, (*itr)->ServerSocket->socket_id());
		}
	}
	return highest_fd;
}


/**
	Thread function that handles answers, i.e. looks for incoming responses and
	tells the D3Server structure what to do next.
	@param _srv_list D3 Server list of type (list\<D3Server*\>*), castet to void*.
	@return Reserved.
*/
void* handle_answers(void* _srv_list)
{
	struct timeval tv;
	
	list<D3Server*>* srv_list = (list<D3Server*>*) _srv_list;
	
	unsigned short answers_left = srv_list->size() * 2; // 2 answers each
	unsigned short cur_answers;
	unsigned short tries_no_response = 0;
	
	printf("Requesting answers from D3 servers, %hd are left...\n",answers_left);
	
	for( unsigned char tries = 0; tries < TRIES_MAX && answers_left && tries_no_response < TRIES_NO_RESPONSE ; tries++)
	{
#ifdef D321GO_DEBUG	
		printf("Try %d, beginning select, %hd answers left (highest fd: %d, ClientVector.size(): %d)...\n",
		       tries, answers_left, D3Server::highest_fd,(int)srv_list->size());
#endif
		
		tv.tv_sec = 0;
		tv.tv_usec = TRIES_DELTA_USEC;
		
		cur_answers = select(D3Server::highest_fd+1, &D3Server::fds, NULL, NULL, &tv);

		for(SrvIter itr = srv_list->begin(); itr!=srv_list->end(); )
		{
			if( FD_ISSET( (*itr)->ServerSocket->socket_id(), &D3Server::fds))
			{
				D3Server* srv_ptr = *itr;
				char buffer[1024];
#ifdef D321GO_DEBUG
				printf(" -> receiving from socket %d!\n",srv_ptr->ServerSocket->socket_id());
#endif
				if(! srv_ptr->ServerSocket->recv(buffer, 1024, 0))
				 printf("  -> recv() error at socket %d\n",srv_ptr->ServerSocket->socket_id()); // TODO
				
				FD_CLR( srv_ptr->ServerSocket->socket_id(), &D3Server::fds);
				
				if(srv_ptr->get_status() == STATUS_FIRST_RECV)
				{
					srv_ptr->insert_d3server(buffer);
					srv_ptr->change_to_hostport();
					//srv_ptr->start_timer();
					srv_ptr->send_2nd_request();
				}
				else
				{
					srv_ptr->add_mn3info(buffer);
					srv_ptr->set_status_done();
				//	delete (*itr); // calls destr.
				//	itr = ServerVector.erase(itr);
				}
				itr++;
			}
			else
			 itr++;
		}
		D3Server::highest_fd = restore_fds(&D3Server::fds, srv_list);
		answers_left-=cur_answers;
		tries_no_response = (cur_answers==0) ? tries_no_response+1 : 0;
	}
	thread_done = true;
	return NULL;
}

/*******************************************************************/

// defined in request_servers.h
unsigned short recv_answers(list<d3server> *ServerList)
{
	list<D3Server*> ServerVector;
	
	//fd_set rfds;
	//FD_ZERO(&rfds);
	//int highest_fd = 0;
	
	/*
		Set up Clients, connect
	*/
	for(list<d3server>::iterator s_itr = ServerList->begin(); s_itr != ServerList->end(); )
	{
#ifdef D321GO_DEBUG
		printf("connecting to %s:%d\n",s_itr->ip.c_str(), s_itr->port);
#endif
		D3Server* new_srv = NULL;
		
		try {
			new_srv = new D3Server(&(*s_itr), USEC_AFTER_REQ_1, USEC_AFTER_REQ_2);
		}
		catch(...)
		{
			delete new_srv;
			s_itr = ServerList->erase(s_itr);
			continue;
		}
		
		FD_SET(new_srv->ServerSocket->socket_id(), &D3Server::fds);
		D3Server::highest_fd = _MAX(D3Server::highest_fd, new_srv->ServerSocket->socket_id());
		
		ServerVector.push_back(new_srv);
		s_itr++;
	}
	
	/*
		Send first requests to all clients
	*/
	for(SrvIter itr = ServerVector.begin(); itr!=ServerVector.end(); itr++) {
		(*itr)->start_timer();
		(*itr)->send_1st_request();
	}
	
	//sleep(3);
	
	//unsigned short answers_left = ServerVector.size() * 2; // 2 answers each
	//unsigned short cur_answers;
	//struct timeval tv; // time interval for select()
	
	thread_done = false;
	pthread_t answer_thread;
	pthread_create(&answer_thread, NULL, handle_answers, &ServerVector); // fetches requests
	
/*	for( unsigned char tries = 0; tries < TRIES_MAX && answers_left; tries++)
	{
		printf("try %d, beginning select, %d answers left (highest fd: %d, ClientVector.size(): %d)...\n",tries, answers_left, D3Server::highest_fd,ServerVector.size());
		
		tv.tv_sec = 0;
		tv.tv_usec = 100 * 1000;
		cur_answers = select(D3Server::highest_fd+1, &D3Server::fds, NULL, NULL, &tv);
		
		for(SrvIter itr = ServerVector.begin(); itr!=ServerVector.end(); )
		{
			if( FD_ISSET( (*itr)->ServerSocket->socket_id(), &D3Server::fds))
			{
#if 0
				char buffer[1024];
				printf("receiving from %d!\n",(*itr)->ServerSocket->socket_id());
				if(! (*itr)->ServerSocket->recv(buffer, 1024, 0))
				 printf("recv()-ERROR"); // TODO
				
				FD_CLR( (*itr)->ServerSocket->socket_id(), &rfds);
				
				if((*itr)->get_status() == STATUS_FIRST_RECV)
				{
					(*itr)->insert_d3server(buffer);
					(*itr)->change_to_hostport();
					(*itr)->send_2nd_request();
				}
				else
				{
					(*itr)->add_mn3info(buffer);
					delete (*itr); // calls destr.
					itr = ServerVector.erase(itr);
				}
#endif
				handle_answers(*itr);
			}
			else
			 itr++;
		}
	
		D3Server::highest_fd = restore_fds(&D3Server::fds, &ServerVector);
		answers_left-=cur_answers;
	} */
	
	
	// TODO: own fd set!
	
	/*
		Timers are stopped here
		NOTE:
		 - even if another thread calls select, too, don't bother. If a response came in,
		   *both* servers will be informed by that
		 - so, you can set the ping timeout to a high value (see header)
	*/
	{
		fd_set rfds;
		int highest_fd;
		struct timeval tv3; // time interval for select()
		do
		{	
			//highest_fd = restore_fds(&rfds, &ServerVector);
			
			// first, put all needed timers into rfds
			FD_ZERO( &rfds);
			highest_fd = 0;
			int count = 0;
			for(list<D3Server*>::const_iterator itr = ServerVector.begin(); itr!=ServerVector.end(); itr++)
			{
				// of course, only timers that are still running...
				if(!(*itr)->timer_stopped()) {
					count++;
					FD_SET((*itr)->ServerSocket->socket_id(), &rfds);
					highest_fd = _MAX(highest_fd, (*itr)->ServerSocket->socket_id());
				}
			}
#ifdef D321GO_DEBUG
			printf("recv_answers(): stopping timers: server count: %d\n",count);
#endif
			// now, call select
			tv3.tv_sec = PING_SELECT_TIMEOUT_SEC;
			tv3.tv_usec = PING_SELECT_TIMEOUT_USEC;

			select(highest_fd+1, &rfds, NULL, NULL, &tv3); // TODO: ret val?
			
			// finally, stop timers if select() saw a response
			for(SrvIter itr = ServerVector.begin(); itr!=ServerVector.end(); itr++)
			 if( FD_ISSET( (*itr)->ServerSocket->socket_id(), &rfds))
			  (*itr)->stop_timer();
			
		} while(!thread_done);
		FD_ZERO(&rfds);
	}
	
	
	//clean up
	//FD_ZERO(&rfds);
	
	/*
		Everything is received => Clean up d3server structs
	*/
	for(SrvIter itr = ServerVector.begin(); itr!=ServerVector.end(); itr++)
	{
		if((*itr)->get_status() != STATUS_ALL_DONE) {
			printf("recv_answers(): cleaning up - error at server from %s:%hu\n",
			       (*itr)->ServerSocket->get_ip().c_str(), (*itr)->ServerSocket->get_port());
#ifdef D321GO_DEBUG
			printf(" socket %d, ServerVector.size()): %d\n",(*itr)->ServerSocket->socket_id(), (int)ServerVector.size());
#endif
			if((*itr)->get_status()==STATUS_READY)
			 puts(" -> Status: nothing done yet - connection problems?");
			else if((*itr)->get_status()==STATUS_FIRST_RECV)
			 puts(" -> Status: receive (1/2) from gameport done");
			else if((*itr)->get_status()==STATUS_SECOND_RECV)
			 puts(" -> Status: receive (1/2) from hostport done - SHOULD NEVER HAPPEN!");
			else
			 puts(" -> Status: UNKNOWN! THIS SHOULD NEVER HAPPEN!");
		}
//		else
//		 (*itr)->compute_ping();
		delete(*itr);
	}
	ServerVector.clear();
	
	/*
		Clean up structs that are not filled in
	*/
	for(list<d3server>::iterator s_itr = ServerList->begin(); s_itr != ServerList->end(); )
	{
		if( s_itr->missionname.empty() ) {
			//puts("WARNING!!!!!! THIS SHOULD  N E V E R  HAPPEN! (but not dangerous)");
			//puts(" => ACTUAL MEANING: Connected to server, but no response");
			puts("Removing one incomplete server from list...");
			s_itr = ServerList->erase(s_itr);
		}
		else
		 s_itr++;
	}
	
	//return answers_left; // TODO?!?!
	return 0;
}

