/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "server_structs.h"

const char *regions[13] =
      { "Unspecified", "Southeast US", "Western US", "Midwest US",
      "Northwest US/West Canada", "Northeast US/East Canada",
      "United Kingdom", "Continental Europe", "Central Asia/Middle East",
      "Southeast Asia, Pacific", "Africa", "Australia/NZ/Pacific",
      "Central, South America" };


const char* d3server::region_name(void) {
        if (location<0 || location>12)
         throw NEW_ERROR("asked for an unknown location", EC_STOP_EXECUTION);
        return regions[location];
}

