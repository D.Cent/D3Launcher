/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file d321go_server.h
	\brief File containing the D321GO remote control server
*/

#ifndef D321GO_SERVER_H
#define D321GO_SERVER_H D321GO_SERVER_H

#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>

#ifdef __WIN32__
 #include <ws2tcpip.h>
#endif

#include <odf.h>
using namespace odf;

#ifdef USE_QT
	#define MainWindow qd321go
#endif

class MainWindow;

/**
	\class D321GOServer
	\brief The D321GO Server class is a handle for remote control.
*/
class D321GOServer : public odf::Socket
{
	
	/*
		DATA
	*/
	
	private:
		fd_set rfds;
		MainWindow* mw_ptr;
		//static bool quit;
		
	/*
		BASIC FUNCTIONS
	*/
	
	public:
		/**
			initializes remote server
			@param port the remote port to be opened
			@param _mw_ptr pointer to MainWindow - must have a set_directip(const char*) function
		*/
		D321GOServer(int port, MainWindow* _mw_ptr);
		
		virtual ~D321GOServer() { }
		
		//! accept()s clients, receives directip and tells the MainWindow
		void* accept_clients(void*);

		//! Static function to call client accept procedure (pthread needs static functions)
		static void* call_ac(void *S){ ((D321GOServer*)S)->accept_clients(NULL); return NULL; }

/*
		inline static void p_sig_handler(int) {
			//exit(99); // close socket safe
		}*/
		
};

#endif // D321GO_SERVER_H

