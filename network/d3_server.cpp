/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "d3_server.h"

fd_set D3Server::fds;
int D3Server::highest_fd;

void D3Server::send_1st_request(void)
{
#ifdef D321GO_DEBUG
	puts("sending 1st request...");
#endif
	if( !ServerSocket->send("\\status\\",8) )
	 printf("D3Server::send_1st_request(): sendto() failed for socket %d\n",ServerSocket->socket_id());
		
	select(0, NULL, NULL, NULL, &after_req1); // wait until message has been sent
	ServerStatus = STATUS_FIRST_RECV;
}
		
void D3Server::send_2nd_request(void)
{
#ifdef D321GO_DEBUG
	puts("sending 2nd request...");
#endif
	//tv.tv_sec = 0;
	//tv.tv_usec = 100 * 1000;
	//select(0, NULL, NULL, NULL, &tv);
	if( !ServerSocket->send("\x01\x1e\x0b\x00\x00\x00\x00\x00\x09\x00\x00\x00", 12) )
	 printf("D3Server::send_2nd_request(): sendto() failed for socket %d\n",ServerSocket->socket_id());
	
	select(0, NULL, NULL, NULL, &after_req2);
	ServerStatus = STATUS_SECOND_RECV;
}

//! Converts received buffer into struct info
void D3Server::insert_d3server(const char* raw)
{
	d3server* cur_server = ServerData;
	
	char cur[128];
	char last[128];
	int diff=0;
	const char* first=raw;
	const char* second=strchr(first,'\\');
	
	bool stop=false;
	bool check=true;

	cur_server->accollisions = false; // default value required

	while(true)
	{
		if(stop == true)
		 break;
	
		if(check == true)
		{
			first = second;
			second = strchr(first+1,'\\');
		
			diff=second-first-1;
		
			cur[diff]='\0';
		
			strncpy(cur, first+1, diff);
		
			strcpy(last, cur);
	
			first = second;
			second = strchr(first+1,'\\');
		
			diff=second-first-1;
		
			cur[diff]='\0';
		
			strncpy(cur, first+1, diff);
			
			//puts("WARNING!");
		}
		
		if(! strncmp(last, "final", 5))
		 break;
		else if(! strncmp(last, "gamever", 7))
		 cur_server->gamever = cur;
		else if(! strncmp(last, "location", 8))
		 cur_server->location = (short)atoi(cur);
		else if(! strncmp(last, "hostname", 8))
		 cur_server->sname = cur;
		else if(! strncmp(last, "hostport", 8))
		 cur_server->hostport = atoi(cur);
		else if(! strncmp(last, "mapname", 7))
		 cur_server->mn3 = cur;
		else if(! strncmp(last, "gametype", 8))
		 cur_server->mod = cur;
		else if(! strncmp(last, "numplayers", 10))
		 cur_server->plr = (unsigned short)atoi(cur);
		else if(! strncmp(last, "maxplayers", 10))
		 cur_server->plrmax = (unsigned short)atoi(cur);
		else if(! strncmp(last, "gamemode", 8))
		 cur_server->isopen = cur;
		else if(! strncmp(last, "teamplay", 8))
		 cur_server->num_teams = (short)atoi(cur);
		else if(! strncmp(last, "timelimit", 9))
		 cur_server->timelimit = (short)atoi(cur);
		else if(! strncmp(last, "fraglimit", 9))
		 cur_server->killgoal = (short)atoi(cur);
		else if(! strncmp(last, "cl_pxotrack", 11))
		 cur_server->pxo = (atoi(cur)) ? true : false;
		else if(! strncmp(last, "mouselook", 9))
		 cur_server->mouselook = (atoi(cur)) ? true : false;
		else if(! strncmp(last, "permissable", 11))
		 cur_server->permissable = (atoi(cur)) ? true : false;
		else if(! strncmp(last, "brightships", 11))
		 cur_server->brightships = (atoi(cur)) ? true : false;		
		else if(! strncmp(last, "accollisions", 12))
		 cur_server->accollisions = (atoi(cur)) ? true : false;
		else if(! strncmp(last, "randpowerup", 11))
		 cur_server->randpowerup = (atoi(cur)) ? true : false;
		else if(! strncmp(last, "player_", 7))
		{
			player_info inf;
			
			inf.name = cur;

			while(1)
			{
				first = second;
				second = strchr(first+1,'\\');
		
				diff=second-first-1;
		
				cur[diff]='\0';
				strncpy(cur, first+1, diff);
		
				strcpy(last, cur);

				first = second;
				second = strchr(first+1,'\\');
				
				diff=second-first-1;
		
				cur[diff]='\0';
				strncpy(cur, first+1, diff);

				if(! strncmp(last, "frags_", 6))
				 inf.kills = (unsigned short)atoi(cur);
				else if(! strncmp(last, "deaths_", 7))
				 inf.deaths = (unsigned short)atoi(cur);
				else if(! strncmp(last, "team_", 5))
				 inf.team = (signed short)atoi(cur);
				else if(! strncmp(last, "ping_", 5))
				 inf.ping = (unsigned short)atoi(cur);
				else
				 break;
			}
			
			cur_server->p.push_back(inf);
			
			if(! strncmp(last, "final", 5))
			 stop=true;
			 
			if(! strncmp(last, "player_", 7))
			 check=false;
		}
	}
}

/*
	send: 01 1e 0b 00 b6 73 05 42 09 00 00 00
	recv 01 1f 58 00 00 00 00 00 00 00 2e 08 00 00 00 00 00 00 00 00
		length srv name - srv name (length not for \0!)
		length mn3 - mn3
		length missionname - missionname
		length mod - mod
		(int) levelnum - ....
*/

void D3Server::add_mn3info(const char* buf)
{
	d3server* cur_server = ServerData;
	const char* ptr = buf+20; // skip packet header
	
	ptr += (*ptr) + 1; // skip server name - already known + \0
	ptr += (*ptr) + 1; // skip mn3 name - already known + \0
	
	short mission_name_length = *ptr;
	cur_server->missionname.reserve(mission_name_length);
	for(short i = 1; i<(mission_name_length); i++)
		cur_server->missionname.push_back(*(++ptr));

	ptr++;	
	ptr++;
	ptr += (*ptr) + 1; // skip mod name - already known + \0
	memcpy(&(cur_server->level_num), ptr, 4);
}
