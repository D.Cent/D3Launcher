/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
#include <cassert>
#include <vector>
using namespace std;

#include <odf.h>
using namespace odf;

#include "download_level.h"

#include "../main/globals.h"
#include "../tools/langfile.h"
using namespace d321go_globals;

#ifdef __WIN32__
 #include "../tools/mc_string.h"
#endif

vector<string> get_level_servers(const char* ip, unsigned short port)
{

	// router?
#if 0 // TODO: useful?
	const char* send_1_r = "\x01\x1e\x0b\x00\x42\x60\xfd\x40\x09\x00\x00\x00"; // 255.255.255.255
					       const char* send_1 =   "\x01\x1e\x0b\x00\x06\x81\xf3\x41\x09\x00\x00\x00"; // server, no.1
#endif	
					       const char* send_2 =   "\x01\x60\x03\x00"; // get level servers

	// ... qstat
	// serveradr rausfinden

	
			       char buffer1[512];
			       UDPClient c (ip, port);
	
	//c.send( send_1, 12 );
	//c.recv( buffer1, 512, 0 ); // len: 85
	//fwrite( buffer1, sizeof(char), 512, fp );
	
			       c.send( send_2, 4 );
/* c.recv( buffer1, 512, 0 ); // len: 266
			fwrite( buffer1, sizeof(char), 512, fp ); */
/*c.recv( buffer1, 16 );
			ubyte len;
			puts("a");
			c.recv( &len, 1 );
			printf("len=%d\n",(int)len);	
			*/
	
			vector<string> v;

			sleep(1);
			c.recv( buffer1, 512 ); // 13 is number of server

			for (int i = 0; i < 512; ++i)
				printf("buffer1[%d] = '%c'\n", i, buffer1[i]);

			printf("Server answer: %s\n", buffer1);

			// 14 is length
			ubyte sequence = buffer1[4];

			printf("sequence = %d\n", (int)sequence);

			ubyte len = buffer1[5+sequence];
			printf("len = %d\n", (int)len);
			char* pos = buffer1+6+sequence;
			printf("startpos = %s\n", pos);
//cout << "get_level_servers(): len=" << (int)len << endl;
			for (ubyte i=0; i<len; i++)
			{
				ubyte slen=pos[0];
	//cout << "len=" << (int)len << "\nslen=" << (int)slen << "\npos=" << pos << endl;
	//cout << "--> pos+2" << (int)pos+2 << "slen=" << (int)pos+2+slen << endl;
	//string s(buffer1,pos+2,pos+2+slen);//,pos+1+slen);
				string s(pos+2,slen);
#ifdef D321GO_DEBUG
				printf("%s.\n",s.c_str());
#endif
				v.push_back(s);
	//cout << s << endl;
	//sleep(1);
				pos+=slen+2;
			}

			return v;
}

bool level_is_available(const char* mn3name)
{
	if(mn3name == NULL)
	 return false;
	
	list<string> flist;
	get_installed_levels(&flist);
	
	for(list<string>::const_iterator itr = flist.begin(); itr != flist.end(); ++itr)
	 if(! strcasecmp(mn3name, itr->c_str()))
	  return true;

#ifdef __LINUX__
	for(list<string>::const_iterator itr = flist.begin(); itr != flist.end(); ++itr)
	 if(! strcasecmp(mn3name, itr->c_str()))
	  return true;
#endif
	
	return false;
}

void get_installed_levels(list<string>* flist)
{
	flist->clear();

#ifdef __LINUX__
	CommonPath<D3HomePath> missionpath1("missions");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string missionpath1(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	missionpath1.append("\\missions\\");
#endif

#ifdef __LINUX__
	CommonPath<D3SharePath> missionpath2("missions");
#endif

	get_dir_entries(flist, missionpath1.c_str(), ".mn3", false);
	get_dir_entries(flist, missionpath1.c_str(), ".MN3", false);

#ifdef __LINUX__
	get_dir_entries(flist, missionpath2.c_str(), ".mn3", false);
	get_dir_entries(flist, missionpath2.c_str(), ".MN3", false);
#endif
}

#include "../libunzip/unzip.h" // include libunzip - this is the ONLY place this library is included!

void unzip_mn3s_from_zip(const char* zipfilename, const char* extract_dir)
{	
	UnZip uz;
	uz.openArchive(zipfilename);
		
	vector<string> filelist = uz.getFileList();
	
	for(vector<string>::iterator itr = filelist.begin(); itr != filelist.end(); ++itr)
	{
		// TODO: make this algorithm faster?
		string last_chars = itr->substr(itr->length()-4);
#ifdef D321GO_DEBUG
		printf("unzip_mn3s_from_zip() last chars=%s\n", last_chars.c_str());
#endif		
		if(strncasecmp(last_chars.c_str(), ".mn3", 4) == 0)
		 uz.extractFile(*itr, extract_dir);
	}
	uz.closeArchive();
}

// TODO: replace by throw()
// for msg_box
#if defined(USE_GTKMM)
	#include "../gui_gtkmm/dialogs.h"
#elif defined(USE_QT)
	#include "../gui_qt/dialogs.h"
#endif

bool download_level_by_mn3(const char* ip, unsigned short hostport, const char* mn3name)
{
	if( level_is_available(mn3name) )
	 return false;

#ifdef __LINUX__
	CommonPath<D3HomePath> save_path("missions");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string save_path(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	save_path.append("\\missions\\");
#endif
	
	vector<string> level_v = get_level_servers(ip, hostport);
	vector<string>::iterator itr = level_v.begin();

	for(; itr != level_v.end(); itr++ )
	{
#ifdef D321GO_DEBUG
		printf("Processing URL: %s\n", itr->c_str());
#endif
		if( itr->size() == 1 ) // empty string - ignore
		 continue;
		if( download_url(itr->c_str(), save_path.c_str()) == true )
		 break;
		else
		 printf("Error while downloading level from %s\n",itr->c_str());
	}
	if(itr==level_v.end())
	 throw NEW_ERROR("Level not found (level list did not contain any useful links)", EC_CONTINUE);
	
	string lvname = itr->substr(itr->rfind('/')+1);
	string last_chars = lvname.substr(lvname.length()-5);
#ifdef D321GO_DEBUG
	printf("download_level_by_mn3(): last_chars=%s\n",last_chars.c_str());
#endif
	if(strncasecmp(last_chars.c_str(), ".mn3", 4) == 0)
	 printf("Downloaded 1 \".mn3\" file");
	else if(strncasecmp(last_chars.c_str(), ".zip", 4) == 0)
	{
		const char* downloaded_zip = (save_path+lvname).c_str();
		string tmp(downloaded_zip);
		unzip_mn3s_from_zip(downloaded_zip,save_path.c_str());
		remove(tmp.c_str());
	}
	else
	{
		string error_str = "Receiving level ";
		error_str += (*itr);
		error_str += "failed because unknown file format (should be \".mn3\" or \".zip\")."
				"This should never happen! Please contact the server admin...";
		throw NEW_ERROR(error_str.c_str(), EC_CONTINUE);
	}
	return true;
}

bool download_level_by_url(const char* level_url)
{
	//string level_url = input_dlg("Download Level", "Insert the URL which contains the path to the zip- or mn3-file here:", "(http only)");
	
	if(strlen(level_url)==0)
	 return false;
	
	bool is_mn3=false;
	
#ifdef __LINUX__
	CommonPath<D3HomePath> save_path("missions");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string save_path(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	save_path.append("\\missions\\");
#endif

	tURL tmp(level_url);

	if(! strcmp(tmp.filename().c_str()+strlen(tmp.filename().c_str())-4, ".mn3") || 
		    ! strcmp(tmp.filename().c_str()+strlen(tmp.filename().c_str())-4, ".MN3"))
	{
		is_mn3=true;
#ifdef D321GO_DEBUG
		printf("download_level_by_url(): CHECK: save_path+string(tmp.filename())=%s\n",
			(save_path+string(tmp.filename())).c_str() );
#endif
		if(level_is_available(tmp.filename().c_str()))
		{
			msg_box("Error", (Translations->get_help("level_exist")).c_str());
			return false;
		}
	}
	
	if( download_url(level_url, save_path.c_str()) == false )
	{
		msg_box("Error", (Translations->get_help("level_error")).c_str());
		return false;
	}
	
	if(is_mn3)
	{
		msg_box("Level Download", (Translations->get_help("lvl_download_success")).c_str());
		return true;
	}
	
	string zipfile(save_path+string(tmp.filename()));
	
	unzip_mn3s_from_zip(zipfile.c_str(), save_path.c_str());
	
	remove(zipfile.c_str());
	
	msg_box("Level Download", (Translations->get_help("lvl_download_success")).c_str());

	return true;
}
