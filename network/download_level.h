/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef DOWNLOAD_LEVEL_H
#define DOWNLOAD_LEVEL_H DOWNLOAD_LEVEL_H

#include <list>
#include <vector>
#include <string>
using namespace std;

#include "server_structs.h"

bool download_level_by_url(const char* level_url);

/**
	download and unzip all MN3s of a level packet to D3's mission directory
	@param ip IP of the server currently running the game
	@param port port of this server
	@param mn3name name of the mn3 file. only needed to check if level is avialable at local machine
	@return false if level was already available, otherwise true
*/
bool download_level_by_mn3(const char* ip, unsigned short port, const char* mn3name = NULL);

/**
	download and unzip all MN3s of a level packet to D3's mission directory
	 @param srv_info Server struct of the server the mission is running on
		@return false if level was already available, otherwise true
*/			 
inline bool download_level_by_mn3(const d3server& srv_info) {
	return download_level_by_mn3(srv_info.ip.c_str(), srv_info.hostport, srv_info.mn3.c_str());
}

/**
	get a list of all servers which let you download the level which runs at IP:port
	@param ip IP of the server currently running the game
	@param port port of this server
*/
vector<string> get_level_servers(const char* ip, int port = 2092);

/**
	Check whether the mission given by \a mn3name is available to D3 at your machine
	@param mn3name Name of the mn3 file, incl. ".mn3"-tag. If NULL, false is returned
	\note this is not a real network function
*/
bool level_is_available(const char* mn3name);

/**
	Check whether the mission given by \a mn3name is available to D3 at your machine
	@param mn3name Name of the mn3 file, incl. ".mn3"-tag. If NULL, false is returned
	\note this is not a real network function
*/
void get_installed_levels(list<string>* flist);

/**
	Unzip all files that end on .MN3. Does not remove the zipfile.
	@param zipfilename Path to the zipfile
	@param extract_dir Directory where MN3 files shall be extracted
*/
void unzip_mn3s_from_zip(const char* zipfilename, const char* extract_dir=".");

#endif //DOWNLOAD_LEVEL_H

