/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file d3_server.h
	Contains the D3Server class, which is needed for the D3 game tracker.
*/

#ifndef D3_SERVER_H
#define D3_SERVER_H D3_SERVER_H

#include "server_structs.h"

#ifdef __WIN32__
 #include "../win32/gettimeofday.h" // for gettimeofday()
#endif

#include <sys/time.h>

/**
	\enum D3_SERVER_STATUS
	\brief Contains information about what was the last action done.
*/
enum D3_SERVER_STATUS
{
	STATUS_READY, //!< Server is initialized, but nothing done yet.
	STATUS_FIRST_RECV, //!< First send() done, receive (from gameport) is awaited.
	STATUS_SECOND_RECV, //!< Second send() done, receive (from hostport) is awaited.
	STATUS_ALL_DONE //!< Second recv() done. Must be set from outside.
};

/**
	\class D3Server
	\brief Class that will be given IP and port of a D3 Server and get all data from there.

	This class will be given the IP and port of a Descent3 Server. It holds functions that
	will enable fetching all data from the D3 Servers and putting it into a given d3server struct.
	The IP and port will be given via that d3server buffer in the constructor.

	The functions for the requests are
	- send_1st_request()
	- send_2nd_request()
	- start_timer() and stop_timer()
	
	The first request is used to fetch most of the data from the gameport. However,
	more data will be needed (mn3 information) from the hostport - Here, the second request
	will be needed, whose reponse will fill the rest of the d3 server struct. Only the
	ping will have to be filled in with the timer functions.
	
	\note This function will only do the requests, i.e. send data to the D3 server. It is YOUR task
	to look for answers, receive them, and pass them to the D3 Server's class with these functions:
	- insert_d3server()
	- add_mn3info()
	
	Also, you will have to stop the timer on your own! See request_servers.cpp for details.

	\note The name might seem strange at the first moment. D3ServerRequest would probably sound more
	logical. However, this class holds exactly one D3 Server and represents it. You could
	concider the class as the D3 Server from outside; inside, it passes the requests to the real
	server in the internet.
*/
class D3Server // : protected Client - TODO ?
{
	protected:
		D3_SERVER_STATUS ServerStatus;
		d3server* ServerData;
		long timer;
		timeval start, end;
		timeval after_req1, after_req2;
		bool _timer_stopped;
	public:
		static fd_set fds;
		static int highest_fd;
		UDPClient* ServerSocket;
		
		/**
			\brief The constructor. Sets up the socket and data.
			@param _ServerData the d3server struct you want to have filled in.
				Must contain the fields ip and port! Other fields can be overwritten.
			@param usec_after_req1 Value for CPU to wait after first request. Shall be very small,
				but if you set it to 0, some data might be lossed.
			@param usec_after_req2 Value for CPU to wait after second request.
		*/
		D3Server(d3server* _ServerData, const int usec_after_req1, const int usec_after_req2)
		 : ServerStatus(STATUS_READY), ServerData(_ServerData), timer(0), _timer_stopped(false)
		{
			ServerSocket = new UDPClient(ServerData->ip.c_str(), ServerData->port, eUDP);
			FD_ZERO(&fds);
			
			after_req1.tv_sec = 0;
			after_req1.tv_usec = usec_after_req1;

			after_req2.tv_sec = 0;
			after_req2.tv_usec = usec_after_req2;
		}
		
		//! Starts internal timer. Can be done anytime, usually when first request is sent.
		inline void start_timer(void) {
#ifdef __WIN32__
			gettimeofday(&start);
#else
			gettimeofday(&start, 0);
#endif
		}
		
		/**
			\brief Stops internal timer. Can be done anytime, usually when first response was received.
			@return the long integer value in 1/1000 seconds or 0 on error.
		*/
		inline long int stop_timer(void) {
#ifdef __WIN32__
			gettimeofday(&end);
#else
			gettimeofday(&end, 0);
#endif

			if(_timer_stopped)
			 return 0;
			
			timer=((end.tv_sec - start.tv_sec) * 1000) + (end.tv_usec - start.tv_usec) / 1000;
			
#ifdef D321GO_DEBUG
			printf("D3Server::stop_timer(): Ping stopped was: %ld\n",timer);
//cout << "ping: " << 1000*(end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) << "ms" << endl;
#endif
			_timer_stopped = true;

			compute_ping();
			return timer;
		}
		
		//! returns true if timer was allready stopped once, false if it was never started or is still running.
		bool timer_stopped(void) {
			//return (timer!=0 && timer < 5000);
			return _timer_stopped;
		}
		
		//! Sends the first request to the gameport. See class description.
		void send_1st_request(void);
		
		//! Sends the second request to the hostport (which must be known in the d3server struct). See class description.
		void send_2nd_request(void);
		
		//! Converts received buffer into d3server struct info
		void insert_d3server(const char* raw);
		
		//! Adds received binary buffer to d3server struct info
		void add_mn3info(const char* buf);
		
		//! Computes ping from internal timer into the d3server struct's timer field.
		inline void compute_ping(void) {
			ServerData->ping = (unsigned short)timer; // TODO: limits.h?
		}
		
		//! Reconnects to the server, but now to its hostport.
		inline void change_to_hostport(void) {
			delete ServerSocket;
			ServerSocket = new UDPClient(ServerData->ip.c_str(), ServerData->hostport, eUDP);
		}
		
		//! Returns status enum, expressing what was the last activity of *this
		inline const D3_SERVER_STATUS& get_status(void) const {
			return ServerStatus;
		}
		
		//! Sets status to 'done'. This is necessary because *this does not know when the second recv() was done.
		inline void set_status_done(void) {
			ServerStatus = STATUS_ALL_DONE;
		}
		
		~D3Server() {
			FD_ZERO(&fds);
			delete ServerSocket;
		}
};

typedef list<D3Server*>::iterator SrvIter;

#endif // D3_SERVER_H
