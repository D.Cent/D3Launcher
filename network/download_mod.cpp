/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <string>
#include <odf.h>
using namespace odf;

#include "download_mod.h"
#include "../main/globals.h"
#include "../callbacks/d321go_execvp.h"
#include "../tools/langfile.h"
#include "../main/globals.h"
using namespace d321go_globals;

#ifdef __WIN32__
 #include "../tools/mc_string.h"
#endif

#include "../libunzip/unzip.h"

/*
	RETURN-VALUES FOR DOWNLOAD_MOD():
		0: mod was downloaded
		1: mod was not found in optfile
		2: connection to server could not be established
		3: optfile could not be found on the server
		4: mod could not be downloaded
		5: mod does already exist
		6: mod not linux-compatible (linux only)
*/

string UpToLow(string str)
{
	for (unsigned int i=0;i<strlen(str.c_str());i++)
	 if (str[i] >= 0x41 && str[i] <= 0x5A)
	  str[i] = str[i] + 0x20;

	return str;
}

bool mod_is_available(const string& mod)
{
#ifdef __LINUX__
	CommonPath<D3HomePath> save_path("netgames");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string save_path(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	save_path.append("\\netgames\\");
#endif

	list<string> flist;
			
	get_dir_entries(&flist, save_path.c_str(), ".d3m", false);
#ifdef __LINUX__
	CommonPath<D3SharePath> save_path2("netgames/");
	get_dir_entries(&flist, save_path2.c_str(), ".d3m", false);
#endif
		
	const string modfile = mod + ".d3m";
#ifdef D321GO_DEBUG
	printf("mod_is_available(): searching for \"%s\"...\n", modfile.c_str());
#endif
	for(list<string>::iterator itr = flist.begin(); itr != flist.end(); itr++)
	{
#ifdef D321GO_DEBUG
		printf(" -> found: \"%s\"\n", itr->c_str());
#endif
		if( ! strcasecmp(itr->c_str(), modfile.c_str()) )
		 return true;
	}
	
	return false;
}

int download_mod(const string& mod, bool quiet)
{
#ifdef __LINUX__
	//some mods require the dmfc-lib, or the mods cannot be initialized - the user will now be asked if he wants to download the lib
	if(! tFILE::access("/usr/lib/dmfc.so", F_OK))
	{
		if(question("QD321GO!! - dmfc.so", (Translations->get_help("mod_dmfc")).c_str()))
		{
			HTTPClient dmfc_client("www.odf-online.org");
			dmfc_client.GET("/d321go/others/dmfc.so", NULL, d321go_globals::D321GOPaths->homepath.c_str());
			
			msg_box("D321GO!!", (Translations->get_help("mod_dmfc_execute")).c_str());
			
			d321go_execvp("", "xterm");
		}
	}
#endif

#ifdef __LINUX__
	CommonPath<D3HomePath> save_path("netgames");
#elif __WIN32__
	OptFile* of = d321go_globals::PlayerOptfile;
	string save_path(MultipleChoiceString::get_current((of->get_single_entry("d3_root","d321go_dirs"))));
	save_path.append("\\netgames\\");
#endif

	if( mod_is_available(mod) )
	{
		if(! quiet)
		 msg_box("Mod Download", (Translations->get_help("mod_exist")).c_str());
	
		return 5;
	}

	HTTPClient* c;

	try { c = new HTTPClient("www.odf-online.org"); } 
	catch(ErrorClass ec) {
		msg_box("Error", (Translations->get_help("mod_connection_failed")).c_str());
		return 2;
	}

	try {
	
	if(! c->GET("/d321go/modlist.txt", NULL, d321go_globals::D321GOPaths->homepath.c_str()))
	{
		msg_box("Error", "HTTP-Error - 404");
		delete c;
		return 3;
	}
	
	map<string, string> mod_map;
	OptFile mod_of((d321go_globals::D321GOPaths->homepath + "/modlist.txt").c_str(), true);
	
	mod_of.to_map(mod_map, "mods");
	
	if(! mod_of.contains_entry(UpToLow(mod).c_str(), "mods"))
	{
		msg_box((Translations->get_help("mod_not_found_title")).c_str(), (Translations->get_help("mod_not_found")).c_str());
		delete c;
		return 1;
	}

#ifdef __LINUX__
	OptFile linux_of((d321go_globals::D321GOPaths->homepath + "/modlist.txt").c_str(), true);
	
	if(! linux_of.contains_entry(UpToLow(mod).c_str(), "linux_compatible"))
	{
		msg_box("Mod Downloader", (Translations->get_help("mod_not_linux")).c_str());
		delete c;
		return 6;
	}
#endif
	string URL(mod_of.get_single_entry(UpToLow(mod).c_str(), "mods"));
	
	tURL zip_path(URL.c_str());
	
	c->GET(zip_path.path().c_str(), NULL, save_path.c_str());
	
	delete c;
	
	tURL mod_path(URL.c_str());
	string zipfilename=mod_path.filename();
	
	UnZip uz;
#ifdef D321GO_DEBUG
	printf("download_mod(): save_path=%s\n", save_path.c_str());
#endif
	uz.openArchive((save_path + string(zipfilename)).c_str());

#ifndef D321GO_DEBUG
	uz.extractAll(save_path.c_str());
#else
	int ret=uz.extractAll(save_path.c_str());
	printf("download_mod(): unzip returned: %d\n", ret);
#endif
	uz.closeArchive();

	remove((save_path + string(zipfilename)).c_str());

	}
	catch(ErrorClass ec) { ec.print_error();  }
	
	return 0;
}
