/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef SERVER_STRUCTS_H
#define SERVER_STRUCTS_H SERVER_STRUCTS_H

#include <string>
using namespace std;

#include <odf.h>
using namespace odf;

/**
	@struct player_info
	@brief Each object of this class contains all data for one player.
*/
struct player_info
{
        string name;
        unsigned short kills;
        unsigned short deaths; // deaths + suicides
        signed short team; // team-number; max. 3 - if -1, it's the server
        unsigned short ping;
};

#include <vector>

/**
	@struct d3server
	@brief Class to store all options a Descent3 server can provide.
*/
struct d3server
{
        string ip;
        unsigned int port;
        string gamever;
        short location; //!< continent
        string sname;
        int hostport; // TODO: short?
        string mn3;
	string missionname; // from extra request
	unsigned short level_num;
        string mod;
        unsigned short plr;
        unsigned short plrmax;
        string isopen;

        /*
        1. num_teams
        2. timelimit
	3. fraglimit
	4. cl_pxotrack
        5: mouselook
	6. permissable
	7. brightships
	8. accollisions
        9. randpowerup
        */

	short num_teams; //<! "1" if no teams (e.g. in anarchy)
	short timelimit;
	short killgoal;
	bool pxo; //!< not needed because PXO won't come back
	bool mouselook; // not useful?
	bool permissable; //!< server mod: permissable or peer to peer
	bool brightships; // not useful?
	bool accollisions; //!< anti-cheat collisions
	bool randpowerup;
	
        // struct player_info *p;
        vector<player_info> p; //!< list of all players
	
	unsigned short ping; //!< ping in milliseconds
	
	//! checks whether two servers are equal
	inline bool operator==(d3server& other) {
		return ( (ip == other.ip) && (port == other.port) );
	}
	
        // ~d3server() { delete[] p; }
	
	//! returns string that belongs to the region specified in short d3server::location
        const char* region_name(void);
	
};

#endif // SERVER_STRUCTS_H
