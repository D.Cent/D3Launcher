/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

// GUI specific includes
#if defined(USE_GTKMM)
	#include "../gui_gtkmm/d321go_mw.h"
#elif defined(USE_QT)
	#include "../gui_qt/qd321go.h"
#endif

#include "d321go_server.h"

#ifdef __WIN32__
	#include <ws2tcpip.h>
#endif

// D321GOServer::quit = false;

D321GOServer::D321GOServer(int port, MainWindow* _mw_ptr)
 : odf::Socket(port, eTCP), mw_ptr(_mw_ptr)
{
	//quit = false;
	
	signal(SIGINT, SIG_IGN);
	
	// accept only local IPs - SAFETY!!!
	sock_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	
	if( bind( sock,(struct sockaddr*)&sock_addr, sizeof( sock_addr ) ) < 0 )
	{
		msg_box("Error - cannot start", "Cannot bind socket (Port already used - probably by D321GO!!)");	
		throw NEW_ERROR("Cannot bind socket (Port already used - probably by D321GO!!)");
	}
	
	if( listen(sock, 5) == -1 )
	 throw NEW_ERROR("Could not listen to socket");

#ifdef LIBODF_DEBUG
	printf("Remote server is running :-) - waiting for clients...\n");
#endif	
	
}

void* D321GOServer::accept_clients(void*)
{
	//while(! quit)
	while(true)
	{
		int len;
		struct sockaddr_in new_sockaddr;
		
		__SOCKET new_fd;
		
		len=sizeof(struct sockaddr_in);
		new_fd = accept(sock, (struct sockaddr*)&(new_sockaddr), (socklen_t*)&len);

		if( new_fd < 0)
		 return NULL;
		
		char remote_buf[128];
		recv(new_fd, remote_buf, 128);
		
		mw_ptr->set_directip(remote_buf);
	}
	return NULL;
}


