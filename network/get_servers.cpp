/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <odf.h>
using namespace odf;

#include "get_servers.h"

#include "d3_tracker.h" // function(s) for getting d3.descent.cx's tracker list
#include "request_servers.h" // function(s) to get full info from every server

#include "../main/globals.h" // needed for the pilot file (check for d3 dir)

/*
	MAIN FUNCTION TO GET FULL INFO FROM ALL SERVERS
*/

unsigned short get_server_info(list<d3server>* ResultList)
{
	unsigned short missed_answers = 0;
	
	list<d3server> ServerList;

// on Windows, check whether the path to the d3 dir is right
#ifdef __WIN32__
	string d3dir = d321go_globals::PlayerOptfile->get_single_entry("d3_root","d321go_dirs");

	if (d3dir.length() == 0)
	{
		msg_box("D3 dir not set", "Please set your D3 directory first!");
		return 0;
	}

	if (! checkdir((d3dir + DIR_SEP + "missions").c_str()) || ! checkdir((d3dir + DIR_SEP + "netgames").c_str()))
	{
		msg_box("D3 dir is wrong", "Please specify the correct path to the Descent3 directory!");
		return 0;
	}
#endif

	try {
		get_d3cx_list(&ServerList); // get ips and ports from d3.descent.cx
		
		// get full server details
		missed_answers = recv_answers(&ServerList);
	}
	catch(ErrorClass ec)
	{
		ec.print_error();
	}
	*ResultList = ServerList;
	
	return missed_answers;
}
