# D3Launcher v0.1

A Descent3 launcher program crafted with ♥ by D.Cent (https://www.github.com/DissCent/D3Launcher).


## CONTENTS

1. Project status
2. What is D3Launcher?
3. Installing
4. Running
5. Uninstalling
6. License
7. Contact
8. Credits
9. Thanks to



## Project status

As of December 2017, this project was just started. It may compile for you, but it will basically just be like the old d321go client (see "What is D3Launcher?").



## What is D3Launcher?

D3Launcher is a Descent launcher program for both platforms, Windows and Linux. It is free to use and open source. You can use it to launch Descent3, browse avaiable servers and chat with people on the Descent IRC servers.

The main aims are:

 * system independency
 * stable software without any restarts
 * an easy understandable user interface

D3Launcher includes the following projects:

 * **d321go**, a former project by the Open Descent Foundation (ODF)
 * **libodf**, a library containing useful network, string and file manipulation functions (also a former ODF project)
 * **libunzip**, a Qt-less port of the unzip library from the OSDaB project (http://osdab.sourceforge.net/)
 * **libircclient**, a C library for IRC communication (http://www.ulduzsoft.com/libircclient/)



## Installing

#### Windows

Download the latest build from the releases page, unzip it and put the created directory to some place where you'd like to have it.


#### Linux

Warning, installing this project is not recommended right now. Please wait for the first beta release!

1. Execute the configure script by typing "./configure"
2. Type "make"
3. Install the program by typing "sudo sh install.sh"



## Running

#### Windows

Simply double click d3launcher.exe.


#### Linux

Start D3Launcher by typing "d3launcher" into your console. Then go through all the options
in the "options" menu in the menubar.

Now you can start playing. Have a lot of fun! :-)



## Uninstalling

#### Windows

Simply delete the folder containing d3launcher.exe.


#### Linux

You can uninstall D3Launcher by typing "sudo sh uninstall.sh".


#### License

###### D3Launcher and libodf

All source code is licensed under the terms of the GPL version 3. See the file
'LICENSE.txt' in the root directory.


###### libunzip and libircclient

All source code is licensed under the terms of the GPL version 2. See the file
'LICENSE.txt' in the concerning directories.



## Contact

Feel free to send me a private message on GitHub or to D.Cent at www.descentforum.de/forum .



## Credits

D3Launcher was written by Philipp Lorenz aka. D.Cent. Libodf and some other parts of the project was also written by Johannes Lorenz aka. King Lo



## Thanks to

 - Johannes Lorenz aka. "King Lo" for creating d321go and contributing lots of code
 - The d321go alpha testers: Delk, Markus, Tom
 - The former Open Descent Foundation community
 - The rest of the Descent community
 - Luigi Auriemma for his gamespy code (which is now removed because of the closure of the service)
 - Georgy Yunaev for libircclient (see libircclientirc/)
 - The Qt community for their excellent API documentation

...and everyone else I might have forgotten!
