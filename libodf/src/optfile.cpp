/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
#include <cctype> // for isgraph()
#include <cassert>

#include <iostream>
#include <map>
#include <string>

using namespace std;

#include "defines.h"

#include "optfile.h"
#include "os_fix.h"

namespace odf {

/*
bool OptFile::fget_stlstring(string* str)
{
	register char c;
	bool behind_savepos = true;
	long savepos = tell();
	do
	{
		c = getc();
		if( c == EOF )
		{
			if(eof()) // no bad error
			{
				behind_savepos = false;
				rewind();
				continue;
			}
			else
			 throw NEW_ERROR("fgetc() in fget_stlstring");
		}
		if(c == '\n')
		 break;
		str->push_back(c);
	} while( behind_savepos || savepos < tell() );
	return true;
}
*/

bool OptFile::fget_stlstring(string* str)
{
	register char c;
	str->clear();
	while( 1 )
	{
		c = getc();
		if( c == EOF )
		{
			if(eof()) { // no bad error
				//rewind();
				return !(str->length()==0);	
			}
			else
			 throw NEW_ERROR("fgetc() in fget_stlstring");
		}
		if(c == '\n')
		 break;
		str->push_back(c);
	}
	return true;
}

bool OptFile::remove_quotes_from_string(string* str)
{
	if( (*str)[0]=='"')
	{
		if( (*str)[str->length()-1] != '"')
		 throw NEW_ERROR("No PAIR of quotes! (check options file!)");
		*str = str->substr(1,str->length()-2);
		return true;
	}
	else return false;
}

/*unsigned int real_string_length(const string& str) const
{
	unsigned int add = 0;
	for(string::iterator itr = str.begin(); itr!=str.end() && !add; itr++)
	 if(!isgraph(*itr))
	  add = 2;
	return str.length() + add;
}*/

string OptFile::get_section_name(const string& line) const
{
	if( ! line_is_section(line) )
	 return string();
	
	//const char* res_ptr = strchr(line,"=") + 1;
	//while(! isgraph(res_ptr) ) { res_ptr++ }
	
	string result = line.substr(line.find('=')+1);
	get_relevant_data(&result);
	return result;
}

bool OptFile::line_is_section(const string& line) const
{
	if(! sections)
	 return false;
	if(! line_is_useful(line))
	 return false;
	if(line.compare(0,7,"section") != 0)
	 return false;
	// there can be whitespace between the "section" and =
	/* for(unsigned int i=0; i<line.length(); i++)
	{
        	if(isgraph(line[i])) { // ignore trailing whitespace
			if(i)
			 line.erase(0, i);
			break;
		}
	} // TODO -> replace with function? or iterators*/
	unsigned int i=7; // after sections string "section"
	
	for( ; i<line.length() && !isgraph(line[i]); i++) ;
	//if(i==line.length())
	// return false;
	DEBUGF("line_is_section? %s\n",(line.at(i)=='=')?"true":"false");
	return(line.at(i)=='=')?true:false; // TODO?!?
}

void OptFile::get_relevant_data(string* str)
{
//	if(strchr(line,'#')!=NULL) // cut strings at # (comments)
//	 strcpy(strchr(line,'#'),"\n\0");

	for(string::iterator itr = str->begin(); itr!=str->end(); itr++)
	{
        	if(isgraph(*itr)||(*itr)=='\n') { // ignore trailing whitespace
			if(itr != str->begin())
			 str->erase(str->begin(), itr);
			break;
		}
	}
	
	string::iterator mark = str->end();
	for(string::iterator itr = str->begin(); itr!=str->end(); itr++)
	{
		if(*itr == '"')
		 for(itr++; (*itr)!='"'; itr++) ; // skip whitespace in quotes
		
        	if(! isgraph(*itr) || *itr == '#') { // ignore whitespace at the end
			mark = itr;
			// if(*itr == '#') - this was probably a bug
			break;
		}
	}
	if(mark != str->end())
	 str->erase(mark, str->end());
	
// TODO : reverse stuff, comments	
	
}

bool OptFile::pack_into_quotes(string* str) const
{
	bool do_it = false;
	for(string::const_iterator itr = str->begin(); itr != str->end() && do_it == false; itr++)
	 if( !isgraph(*itr) || *itr == '#' )
	  do_it = true;
	if(!do_it)
	 return false;
	str->insert(str->begin(),'"');
	str->push_back('"');
	DEBUGF("had to put string into quotes, now: %s\n",str->c_str());
	return true;
}

//////////////////////

// returns length of shortened optline or 0 if line was not useful
unsigned int OptFile::get_stringpair(string* optline, string* opt1, string* opt2) const
{
	//printf("BEFORE GET REL. DATA: %s\n",optline->c_str());
	get_relevant_data(optline);
	
	if(! line_is_useful(*optline) )
	 return 0;
	
//	if(strchr(optline,'=')==NULL) // ignore lines without '='
//	 throw NEW_ERROR("A line contains no '='! (check options file!)",0);

	opt1->clear(); opt2->clear(); // make buffers empty
	
	*opt1 = optline->substr(0, optline->find('='));
	*opt2 = optline->substr(optline->find('=')+1);
		
	get_relevant_data(opt1);
	get_relevant_data(opt2);
	
	//sscanf(optline, "%[^=]=%[^\n]",opt1,opt2);
	
	if(opt1->empty())
	 throw NEW_ERROR("A key is empty! (check options file!)",0);
	
/*	//bool in_quotes = false;
	if( (*opt1)[0]=='"')
	{
		//in_quotes = true;
		if( (*opt1)[opt1->length()-1] != '"')
		 throw NEW_ERROR("No PAIR of quotes! (check options file!)");
		//strncpy(opt1, opt1+1, strlen(opt1)-2); // get rid of the quotes
		*opt1 = opt1->substr(1,opt1->length()-1); // get rid of the quotes
	}*/

	remove_quotes_from_string(opt1); // function see above
	remove_quotes_from_string(opt2);
	
	//printf("optline=%s, left=%s, right=%s\n", optline->c_str(), opt1->c_str(), opt2->c_str());

	for(unsigned int i=0; i<opt1->length(); i++)
	 if(!isprint((*opt1)[i])) // test if strings are printable // OLD: and no space (isgraph()
	  {	//printf("i=%u\n",i);
		throw NEW_ERROR("A key has unprintable characters! (check options file!)",0);
	}
	
	return optline->length();
}

// returns size of unshortened line or 0 if line is section
unsigned int OptFile::get_stringpair(string* opt1, string* opt2)
{
	string optline;
	unsigned int size_before=0;
	
	//bool behind_savepos = true;
	//unsigned long savepos = tell();
	//DEBUG("get_stringpair() begin...");
	
	do
	{
		fget_stlstring(&optline);
		
		//printf("behind: %d, savepos=%lu, tell()=%ld\n", (int)behind_savepos, savepos, tell());
		//printf("optline=%s\n",optline.c_str());
		size_before = optline.length();
		
// 		if( ! get_stringpair(&optline, opt1, opt2)) // continue as long as the line was not useful
// 		 continue;
		if( get_stringpair(&optline, opt1, opt2)) // stop if line was useful
		 break;
		//printf("opt1->cstr=%s\n",opt1->c_str());
//		if(!behind_savepos && tell()>=savepos)
//		 return 0; // no more lines
		
		if( eof() ) {
			rewind();
//			behind_savepos = false;
		}
		
	} while( true );
	//DEBUG("get_stringpair() end...");
	return (line_is_section(optline)) ? 0 : size_before;
}

bool OptFile::skip_to_section(const char* section)
{
	bool break_up = false;
	string optline;
	
	DEBUGF("skip_to_section(%s)...\n", section);
	
//	bool behind_savepos = true;
//	unsigned long savepos = tell();
	
	if(section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	
	StartPosition start_pos(this);
	do
	{
		optline.clear();
		
		if( ! fget_stlstring(&optline) ) // feof
		{
//			if(tell()>=savepos)
//			 return false;
			rewind();
//			behind_savepos = false;
		}
		//break_up=true;
		DEBUGF("optline=%s\n",optline.c_str());
		if(! line_is_section(optline))
		 continue;
		
	} //while (get_section_name(optline) != section && (behind_savepos || savepos < tell()));
	while(get_section_name(optline) != section && !start_pos.overstepped());
	
	DEBUGF("skip_to_section returning %d\n",(int)!break_up);
	return !break_up;
}

void OptFile::to_map(map<string, string>& m, const char* section)
{
//	char optline[LINE_BUF];
//	auto char opt1[LINE_BUF], opt2[LINE_BUF];
//	string optline;
	string opt1, opt2;
//	long save_pos;
	
	DEBUGF("to_map(..., %s)\n",section);
	
	if(sections && section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	if(sections)
	 if(! skip_to_section(section))
	  throw NEW_ERROR("A section couldn't be found!",0);
	
	StartPosition start_pos(this);
	
	do
	{
		//save_pos = tell();
		
		
		
		if(! get_stringpair(&opt1, &opt2))
		 break;// new section begins here!

//		if(sections && strncmp(opt1,"section",7)==0) // next section begins here
//		 break;
		
		//printf("_%s_\n",optline);
		
		//printf("opt1=%s, opt2=%s\n",opt1.c_str(),opt2.c_str());
		
		if( start_pos.overstepped() )
		 break;
		
		if(m.find(opt1)!=m.end()) //already found this key before
		{
			string msg = "A key was found twice! (check options file!) : ";
			msg += opt1;
			throw NEW_ERROR(msg.c_str(),0);
		}

		m.insert(pair<string, string>(opt1, opt2));
	} while( true );
	DEBUGF("to_map end\n");
	//if( sections && strncmp(opt1,"section",7)==0) // TODO!!!!!!!!!!!!!!!!! NEXT
	// t_fseek(optfile,-len-1,SEEK_CUR);
	
	//seek(save_pos, SEEK_SET);
}

void OptFile::from_map(const map<string, string>& m, const char* section)
{

	string opt1, opt2;
	string right_stlstr, left_stlstr;
	int linelen;
	
	if(sections && section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	if(sections)
	 if(! skip_to_section(section))
	  throw NEW_ERROR("A section couldn't be found!",0);
	
	DEBUGF("from_map([map], %s)\n",section);
	
#ifdef LIBODF_DEBUG
	puts("MAP CONTENTS FOLLOWING!");
	for(map<string,string>::const_iterator itr = m.begin(); itr!= m.end(); itr++)
	 printf("%s = %s\n",itr->first.c_str(), itr->second.c_str());
	puts("MAP CONTENTS END!");
#endif

	StartPosition start_pos(this);
	while( true ) //do
	{
		if(start_pos.overstepped())
		 break;
		linelen = get_stringpair(&opt1, &opt2);
		if(! linelen ) // next section begins here
		 break;
		
		//printf("searching stringpair %s=%s\n",opt1.c_str(), opt2.c_str());
		
 		map<string,string>::const_iterator itr = m.find(opt1);
		if(itr==m.end()) // ...add (TODO)
		 throw NEW_ERROR("Adding options not provided yet",0);

		//if(opt1 == itr->second) // found left string
		{
				left_stlstr = itr->first;
				right_stlstr = itr->second;
			
                		DEBUGF("right_stl=%s (%d); left_stl=%s (%d); linelen: %d\n",
                        		right_stlstr.c_str(), right_stlstr.length(), left_stlstr.c_str(), left_stlstr.length(), linelen);
	
				// TRY to pack into quotes
				pack_into_quotes(&right_stlstr);
				pack_into_quotes(&left_stlstr);
			
				signed int diff = right_stlstr.length() + left_stlstr.length() +1 - linelen;
				
				DEBUGF("diff = %d\n",diff);
				seek(-linelen,SEEK_CUR);
				char c;
				do { c = getc(); } while ( c != '=' && c != EOF);
				if(diff<0) {
					SYNC_WRITE_BEGIN(*this);
					put(right_stlstr.c_str());
					SYNC_WRITE_END(*this);
					block_remove(*this,-diff,filestats.st_size);
				}
				else if(diff>0) {
					block_insert(*this,diff,filestats.st_size);
					SYNC_WRITE_BEGIN(*this);
					put(right_stlstr.c_str());
					SYNC_WRITE_END(*this);
				}
				else // diff = 0
				{
					if(opt2 == itr->second) { // TODO?!
						DEBUGF("equal: %s\n",opt2.c_str());
					 seek(right_stlstr.length(), SEEK_CUR);
					}
					else {
						SYNC_WRITE_BEGIN(*this);
						put(right_stlstr.c_str());
						SYNC_WRITE_END(*this);
					}
					 //fputs(right_stlstr.c_str(), *this);
				} // real line length! comments etc!
				//printf("GETC-FTELL: %ld\n",tell());
				while( getc() != '\n' ) ;
				//printf("GETC-FTELL: %ld\n",tell());
				//t_fseek(optfile,1,SEEK_CUR);
				stat(&filestats); // get new filesize
				
		}
	} //while(!start_pos.overstepped());
	DEBUG("from_map() end\n");
}

bool OptFile::contains_entry(const char* entry, const char* section)
{	
	string opt1, opt2;
	
	DEBUGF("contains_entry(%s, %s);\n",entry, section);
	
	if(sections && section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	if(sections)
	 if(! skip_to_section(section))
	  throw NEW_ERROR("A section couldn't be found!",0);
	
	StartPosition start_pos(this);
	do
	{
		if( ! get_stringpair(&opt1, &opt2) ) // next section begins here
		 break;
		if( entry == opt1 )
		 return true;
	} while(!start_pos.overstepped());

	return false;
}

string OptFile::get_single_entry(const char* left, const char* section)
{	
	string opt1, opt2;
	
	DEBUGF("get_single_entry(%s, %s);\n",left, section);
	
	if(sections && section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	if(sections)
	 if(! skip_to_section(section))
	  throw NEW_ERROR("A section couldn't be found!",0);
	
	StartPosition start_pos(this);
	do
	{
		if(! get_stringpair(&opt1, &opt2)) // next section begins here
		 break;
		//printf("LEFT=%s, OPT1=%s, OPT2=%s\n",left, opt1.c_str(), opt2.c_str());
		if( left == opt1 )
		 return opt2;
	} while(!start_pos.overstepped());

	throw NEW_ERROR("one entry could not be found");
}

// SEEMS TO WORK
void OptFile::set_single_entry(const char* left, const char* right, const char* section, bool str_in_quotes)
{
	string opt1, opt2;
	string right_stlstr = right, left_stlstr = left;
	int linelen;
		
	if(sections && section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	if(sections)
	 if(! skip_to_section(section))
	  throw NEW_ERROR("A section couldn't be found!",0);
	
	// TRY to pack into quotes
	pack_into_quotes(&right_stlstr);
	pack_into_quotes(&left_stlstr);
	
	while(1)
	{	
		linelen = get_stringpair(&opt1, &opt2);
		DEBUGF("linelen1 = %d\n", linelen);

		if(! linelen ) // next section begins here
		 break;
		//printf("right_stl=%s (%d); left_stl=%s (%d); linelen: %d\n",
		//	right_stlstr.c_str(), right_stlstr.length(), left_stlstr.c_str(), left_stlstr.length(), linelen);
		if(opt1 == left) // found left string
		{
			signed int diff = right_stlstr.length() + left_stlstr.length() +1 - linelen;
			DEBUGF("diff = %d\n",diff);
			seek(-linelen,SEEK_CUR);
			char c;
			do { c = getc(); } while ( c != '=' );
			if(diff<0) {
				SYNC_WRITE_BEGIN(*this);
				fputs(right_stlstr.c_str(), *this);
				SYNC_WRITE_END(*this);
				block_remove(*this,-diff,filestats.st_size);
			}
			else if(diff>0) {
				block_insert(*this,diff,filestats.st_size);
				SYNC_WRITE_BEGIN(*this);
				fputs(right_stlstr.c_str(), *this);
				SYNC_WRITE_END(*this);
			}
			else { // diff = 0
			// fputs(right_stlstr.c_str(),*t
				if(opt2 == right) {
					seek(right_stlstr.length(), SEEK_CUR);
				}
				else {
					SYNC_WRITE_BEGIN(*this);
					fputs(right_stlstr.c_str(), *this);
					SYNC_WRITE_END(*this);
				}
			}

			while( getc() != '\n' ) ;

			//t_fseek(optfile,1,SEEK_CUR);
			stat(&filestats); // get new filesize
			return;
		}
	}
	throw NEW_ERROR("No matching stringpair found");
}

void OptFile::add_single_entry(const char* left, const char* right, const char* section)
{
	string opt1, opt2;
	string right_stlstr = right, left_stlstr = left;
//	unsigned int linelen;
		
	if(sections && section==NULL)
	 throw NEW_ERROR("No section specified for an options file with sections!",0);
	if(sections)
	 if(! skip_to_section(section))
	  throw NEW_ERROR("A section couldn't be found!",0);
	
	// TRY to pack into quotes
	pack_into_quotes(&right_stlstr);
	pack_into_quotes(&left_stlstr);
	
	stat(&filestats); // just to be safe!
	//printf("left: %s, right: %s\n", left_stlstr.c_str(), right_stlstr.c_str());
	DEBUGF("CALLING BLOCK INSERT: ftell(): %ld, slen: %d, filestats.st_size: %ld\n",tell(),left_stlstr.length() + right_stlstr.length() + 2,filestats.st_size);
	block_insert(*this, left_stlstr.length() + right_stlstr.length() + 2, filestats.st_size); // + 2 for '=' and '\n'

	//printf("ADDING SINGLE ENTRY: %s = %s\n",left_stlstr.c_str(), right_stlstr.c_str());
	
	SYNC_WRITE_BEGIN(*this);
	put(left_stlstr.c_str());
	//printf("ftell between: %d\n",(int)tell());
	fputc('=', *this);
	//sleep(20);
	put(right_stlstr.c_str());
	fputc('\n', *this);
	SYNC_WRITE_END(*this);
	//sleep(20);
}

} // namespace odf
