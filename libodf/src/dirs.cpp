/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/*
	This code is taken from Qt (QDir)
*/

#include <string>
#include <list>
using namespace std;

#include <sys/stat.h>
#include <dirent.h>

#include "error_class.h"
#include "paths.h"
#include "defines.h" // for DEBUGF
			 
#include "dirs.h"

namespace odf {

// todo:	throw()'s 2nd args
//		sort

//list<string> read_dir_entries( const char* dirname, const string &nameFilter, int filterSpec, int sortSpec )
unsigned int get_dir_entries( list<string> *fList, const char* dirname, const char* contains, bool prepend_prefix, int filterSpec, int sortSpec )
{
	int num = 0;

	bool doDirs     = (filterSpec & Dirs)	!= 0; // ok
	bool doFiles    = (filterSpec & Files)	!= 0; // ok
	bool doSymLinks = (filterSpec & SymLinks) != 0;
	bool doReadable = (filterSpec & Readable)	!= 0;
	bool doWritable = (filterSpec & Writable)	!= 0;
	bool doExecable = (filterSpec & Executable) != 0;
	bool doHidden   = (filterSpec & Hidden)	!= 0; // ok
	bool doSystem   = (filterSpec & System)     != 0;
	bool doNoDotAndDotDot = (filterSpec & NoDotAndDotDot) != 0;
	
	//QFileInfo fi;
	DIR	*dir;
	dirent	*file;

	dir = opendir( dirname );
	if ( !dir )
	 throw NEW_ERROR("cannot open directory for reading its entries!", 0);

/*#if defined(QT_THREAD_SUPPORT) && defined(_POSIX_THREAD_SAFE_FUNCTIONS) && !defined(Q_OS_CYGWIN)
	union {
		struct dirent mt_file;
		char b[sizeof(struct dirent) + MAXNAMLEN + 1];
	} u;
	while ( readdir_r(dir, &u.mt_file, &file ) == 0 && file )
#else*/
	while ( (file = readdir(dir)) )
//#endif // QT_THREAD_SUPPORT && _POSIX_THREAD_SAFE_FUNCTIONS
	{
		string fn;
		if(dirname[0]!='.' || dirname[1]!='\0')
		{
			fn+=dirname;
			if(fn[(fn.length()-2)]!=DIR_SEP)
			 fn += DIR_SEP;
		}
		fn += file->d_name;
		
		struct stat statbuf;
#ifdef __LINUX__
		if(lstat(fn.c_str(), &statbuf) == -1) {
#else
		if(stat(fn.c_str(), &statbuf) == -1) { // no linux -> no symlinks -> no lstat() -> no need for Lstat
#endif
			puts("LIBODF STAT ERROR"); // TODO
			continue;
		}
		
		DEBUGF("file %s, mode %d\n",fn.c_str(), (int)statbuf.st_mode);
		
		mode_t mode = statbuf.st_mode;
		
#if 1 // TODO! 
		
		
		
#if 0		
		// filter RWE
		if( statbuf.st_uid == getuid() ) { // file is owned by user
			if( ((mode & S_IRUSR) * doReadable) | ((mode & S_IWUSR) * doWritable) | ((mode & S_IXUSR) * doExecable) )
		 	continue;
		}
		else if( statbuf.st_gid == getgid() ) { // file has same group as user
			if( ((mode & S_IRGRP) * doReadable) | ((mode & S_IWGRP) * doWritable) | ((mode & S_IXGRP) * doExecable) )
			 continue;
		}
		else {
			if( ((mode & S_IROTH) * doReadable) | ((mode & S_IWOTH) * doWritable) | ((mode & S_IXOTH) * doExecable) )
			 continue;
		]
#endif
		
		bool mode_r, mode_w, mode_x;
		if( statbuf.st_uid == getuid() ) {
		 mode_r = mode & S_IRUSR; mode_w = mode & S_IWUSR; mode_x = mode & S_IXUSR;
		} else if( statbuf.st_gid == getgid() ) {
		 mode_r = mode & S_IRGRP; mode_w = mode & S_IWGRP; mode_x = mode & S_IXGRP;
		} else {
		 mode_r = mode & S_IROTH; mode_w = mode & S_IWOTH; mode_x = mode & S_IXOTH;
		}

#ifdef LIBODF_DEBUG
		printf("mode: %d%d%d\n",(int)mode_r,(int)mode_w,(int)mode_x);
		printf("doRWE: %d%d%d\n",(int)doReadable,(int)doWritable,(int)doExecable);
		printf("=> %d %d %d\n",(int)(mode_r & doReadable),(int)(mode_w & doWritable),(int)(mode_x & doExecable));
		printf("filterSpec: %d\n", filterSpec);
#endif		

		if( !(mode_r & doReadable) &  !(mode_w & doWritable) & !(mode_x & doExecable) )
		 continue;

		if ( (!doDirs && S_ISDIR(mode) ) ||
		 (!doFiles && S_ISREG(mode) ) ||
		 (!doSystem && !(S_ISREG(mode) || S_ISDIR(mode) ||S_ISLNK(statbuf.st_mode)) ) 
		 )
		continue;

		{
#ifdef __LINUX__ // no linux -> no symlinks -> no S_IFLNK and no S_ISLNK
			if ( !doSymLinks && (int)S_ISLNK(statbuf.st_mode) )  // (statbuf.st_mode & S_IFLNK)
	        	 continue;
#endif
/*			if ( (filterSpec & RWEMask) != 0 )
			 if ( (doReadable && !fi.isReadable()) || (doWritable && !fi.isWritable()) || (doExecable && !fi.isExecutable()) )
			  continue; */
			
			if ( filterSpec & RWEMask )
			 
			if ( !doHidden && (file->d_name)[0] == '.'
			 && strcmp((file->d_name), ".") != 0 
			 && strcmp((file->d_name), "..") != 0 )
			 continue;
			
			if( doNoDotAndDotDot && ( strcmp((file->d_name), ".") == 0 || strcmp((file->d_name), "..") == 0 ) )
			 continue;
			
			//fList->push_back( fn );
		}

/*		num++;
		if(fList!=NULL)
		{
			if(contains!=NULL) {
				if(strstr(fn.c_str(), contains) != NULL) {
				 fList->push_back( fn );
					
				}
			}
			else
			 fList->push_back( fn );
		}
		else */
		bool file_fits = true;
		
		if(contains!=NULL)
		 if(strstr(file->d_name, contains) == NULL)
		  file_fits = false;
		
		if(file_fits) {
			DEBUGF("get_dir_entries(): file %s fits.\n",fn.c_str());
			if(fList!=NULL)
			 fList->push_back( prepend_prefix?fn:(file->d_name) );
			num++;
		}
		

/*			
		if(fList!=NULL)
		{
			if(contains!=NULL) {
				if(strstr(file->d_name, contains) != NULL) {
				 fList->push_back( fn );
					
				}
			}
			else
			 fList->push_back( fn );
		}
		*/
		 
#endif
	}
	if ( closedir(dir) != 0 )
	 throw NEW_ERROR("cannot close directory after reading its entries!", 0);

	// Sort...
	if( fList )
	{
		if(sortSpec&odf::Name) // only allow alphabetic sorting by now...
		{
			printf("WTF!!\n");
			fList->sort();
		}
	}
#if 0
	if(fiList->count()) {
		QDirSortItem* si= new QDirSortItem[fiList->count()];
		QFileInfo* itm;
		i=0;
		for (itm = fiList->first(); itm; itm = fiList->next())
		 si[i++].item = itm;
		qt_cmp_si_sortSpec = sortSpec;
		qsort( si, i, sizeof(si[0]), qt_cmp_si );
		// put them back in the list
		fiList->setAutoDelete( FALSE );
		fiList->clear();
		int j;
		for ( j=0; j<i; j++ ) {
			fiList->append( si[j].item );
			fList->append( si[j].item->fileName() );
		}
		delete [] si;
		fiList->setAutoDelete( TRUE );
	}

	if ( filterSpec == (FilterSpec)filtS && sortSpec == (SortSpec)sortS && nameFilter == nameFilt )
	 dirty = FALSE;
	else
	 dirty = TRUE;
#endif
	//return fList;
	return num;
}

} // namespace odf
