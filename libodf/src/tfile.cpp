/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "defines.h" // for DEBUGF

#include "tfile.h"

namespace odf {

/*
	function definitions
*/
void tFILE::truncate(off_t t_length)
{
	if(t_length<0)
	 throw NEW_ERROR("2nd argument of ftruncate() is less than 0",EC_LOGIC_ERROR|EC_STOP_EXECUTION);

	if( ftruncate(get_fd(),t_length) )
	 throw NEW_ERROR("calling ftruncate()",EC_CONTINUE);	
}

void tFILE::seek(long t_offset, int t_whence)
{
	DEBUGF("calling fseek(fp(%ld),%ld,%d)\n",ftell(fp),t_offset,t_whence);
	//if( (ftell(t_stream)+t_offset) < 0 ) // if 2nd arg is wrong
	// .... 
	if(! (t_whence==SEEK_SET||t_whence==SEEK_CUR||t_whence==SEEK_END) )
	 throw NEW_ERROR("3rd argument of fseek() is wrong",EC_STOP_EXECUTION|EC_LOGIC_ERROR);
	if( fseek(fp, t_offset, t_whence) != 0 ) // call fseek()
	 throw NEW_ERROR("error calling fseek()",EC_STOP_EXECUTION);
}

void tFILE::open(const char *t_path, const char *t_mode)
{
	DEBUGF("opening file %s (mode %s)!!\n",t_path,t_mode);
	fp = fopen(t_path, t_mode);
	if(fp == NULL)
	 switch(errno) {
		case EINVAL:	throw NEW_ERROR("2nd argument of fopen() is wrong",EC_LOGIC_ERROR|EC_STOP_EXECUTION);
		default:	throw NEW_ERROR("calling fopen()",EC_CONTINUE);
	 }
}

void tFILE::close(void) {
	printf("closing fp with addr %ld\n",(long)fp);
	if(fp!=NULL)
	 if(fclose(fp) != 0)
	  throw NEW_ERROR("calling fclose()",EC_CONTINUE);
	fp = NULL;
	puts("closing done");
}

char* tFILE::gets(char* t_s, int t_n)
{
	char* ret_val = fgets(t_s, t_n, fp);
	if( ret_val == NULL && !feof(fp) )
	 throw NEW_ERROR("calling gets()", EC_CONTINUE);
	return ret_val;
}

int tFILE::getc(void)
{
	int ret_val = fgetc(fp);
	if( ret_val == EOF && !feof(fp) )
	 throw NEW_ERROR("calling getc()", EC_CONTINUE);
	return ret_val;
}

bool tFILE::access(const char* t_path, int t_amode)
{
	bool res=true;
	if( ::access(t_path, t_amode) == -1 )
	 switch(errno) {
		case EACCES:
		case ENOENT:
		case ENOTDIR:
			res = false;
			break;
		case EINVAL: throw NEW_ERROR("2nd argument of access() is wrong",EC_LOGIC_ERROR|EC_STOP_EXECUTION);
                default: throw NEW_ERROR("calling access()",EC_CONTINUE);
	 }
	return res;
}

unsigned long tFILE::tell(void)
{
	long res = ftell(fp);
	if( res == -1 )
		throw NEW_ERROR("calling ftell()", EC_CONTINUE);
	return res;
}

void tFILE::read_string( char* s, unsigned int max )
{
	char* ptr = s;
	for( register unsigned int n = 0; n<max && (*ptr)!=0 ; n++, ptr++) {
		read( ptr, 1, 1 );
	}
}

void tFILE::read_string( string* s )
{
	register char c;
	do {
		c = getc();
		s->push_back( c );
	} while( c != 0 );
}

} // namespace odf
