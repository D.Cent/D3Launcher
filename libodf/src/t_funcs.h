/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file t_funcs.h
	\brief easier working with error throwing functions

	All those functions only throw instances of ErrorClass. If you take the t_-prefixes away,
	you know which functions they are wrapping. E.g. t_fseek() is a wrapper for fseek().
	\note The arguments are mostly the same, but the return values often differ.
*/

#ifndef T_FUNCS_H
#define T_FUNCS_H T_FUNCS_H

#include <unistd.h>
#ifdef MACINTOSH
#include <stat.h>
#else
#include <sys/stat.h>
#endif

#include "error_class.h"

//bool t_danger=false;

// TODO: 	
//		t_ftell()

/*
	defined in t_funcs.cpp:
*/

namespace odf {

#ifdef __WIN32__
	#define SYNC_WRITE_BEGIN(_fp)     t_fseek(_fp, 0, SEEK_CUR)
	#define SYNC_WRITE_END(_fp)    t_fseek(_fp, 0, SEEK_CUR)
#else 
	#define SYNC_WRITE_BEGIN(_fp)    /* (WRITE_SYNC_BEGIN) */
	#define SYNC_WRITE_END(_fp)    /* (WRITE_SYNC_END) */
#endif

//! \brief wraps the access() function
//! @return true if @a t_path could be accessed, otherwise false
bool t_access(const char* t_path, int t_amode);

//! \brief wraps the ftruncate() function
//! \note this function is platform independent
void t_ftruncate(FILE* t_fp, off_t t_length);

//! wraps the fseek() function
void t_fseek(FILE *t_stream, long t_offset, int t_whence);

//! wraps the fopen() function
FILE* t_fopen(const char *t_path, const char *t_mode);

//! \brief wraps the getc() function
//! @return EOF only if EOF, otherwise the read character.
//! @exception throwen on errors, but not on EOF
int t_getc(FILE *stream);

/*
	inline-functions:
*/

//! wraps the stat() function
inline void t_stat(const char* t_path, struct stat* t_buf) {
	if(stat(t_path, t_buf)!=0)
	 throw NEW_ERROR("error calling t_stat()",EC_STOP_EXECUTION);
}

//! wraps the fread() function
//! \exception ErrorClass Throwen if less than @a t_size bytes were read.	
inline void t_fread(void *t_ptr, size_t t_size, size_t t_nmemb, FILE *t_stream) {
	if(fread(t_ptr, t_size, t_nmemb, t_stream) != t_nmemb)
	 throw NEW_ERROR("fread(): not enough bytes read",EC_STOP_EXECUTION);
}

//! wraps the fwrite() function
//! \exception ErrorClass Throwen if less than @a t_size bytes were written.
inline void t_fwrite(const void *t_ptr, size_t t_size, size_t t_nmemb, FILE *t_stream) {
	if(fwrite(t_ptr, t_size, t_nmemb, t_stream) != t_nmemb)
	 throw NEW_ERROR("fread(): not enough bytes read",EC_STOP_EXECUTION);	
}

//! wraps the fclose() function
inline void t_fclose(FILE* t_fp) {
	if(fclose(t_fp) != 0)
	 throw NEW_ERROR("calling fclose()",EC_CONTINUE);
} 

//! wraps the mkdir() function
//! \note this function is platform independent
inline void t_mkdir(const char* path, mode_t mode = 0777) {
#ifdef __WIN32__
	if( mkdir(path) == -1 )
#else
	if( mkdir(path, mode) == -1)
#endif
	 throw NEW_ERROR("calling mkdir()",EC_CONTINUE);
}

} // namespace odf

#include <string>

namespace odf {

void t_read_string( char* s, unsigned int max, FILE* fp );
void t_read_string( std::string* s, FILE* fp );

} // namespace odf

#endif // T_FUNCS_H
