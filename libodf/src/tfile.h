/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file t_funcs.h
	\brief easier working with error throwing functions using the tFILE class
*/

#ifndef TFILE_H
#define TFILE_H T_FILE_H

#include <iostream>
#include <cstdio>
#include <vector>
using namespace std;

#include <unistd.h>

#ifdef MACINTOSH
 #include <stat.h>
#else
 #include <sys/stat.h>
#endif

#include "error_class.h"

namespace odf {

/**
	\class tFILE
	\brief class for easier working with error throwing functions

	Most of the functions throw ErrorClass on serious errors. If you take the f of the name
	and the FILE pointer argument of the parameter list away, you get the wrappers' names.
	E.g. fseek(FILE*, long, int) is a wrapped by for tFILE::seek(long, int).
	If you need to use a function which is not wrapped, use the class like a FILE pointer.
	E.g.: tFILE fp; fprintf(fp, "test\n"); This it made possible by the operator FILE*().
	\note The arguments are mostly the same, but the return values may differ.
*/
class tFILE
{
	private:
		/**
			The file pointer this class represents. Get it via the operator FILE*().
			The file pointer will point to NULL if no file is currently opened.
		*/
		FILE* fp;
	
	public:
		//! default constructor, does not open any file
		inline tFILE() throw() : fp(NULL) {}
		
		//! constructor that opens a file like fopen()
		inline tFILE(const char* t_path, const char *t_mode) {
			open(t_path, t_mode);
		}
		
		//! destructor, closes file if necessary
		inline ~tFILE() {
			close();
		}
		
		//! makes tFILE class usable like FILE pointers
		inline operator FILE* () throw() {
			return fp;
		}
		
		inline FILE* get_fp( void ) throw() {
			return fp;
		}
		
		//! returns file descriptor for this file
		inline int get_fd(void) throw()
		{
#if defined(__WIN32__)
			return fp->_file;
#else
			return ::fileno(fp);
#endif
		}
		
		//! wraps the fopen() function
		void open(const char *t_path, const char *t_mode);
		
		//! wraps the fclose() function
		void close(void);
		
		//! wraps the rewind() function
		inline void rewind(void) throw() {
			std::rewind(fp);
		}
		
		//! wraps the fread() function
		//! \exception ErrorClass Throwen if less than @a t_size bytes were read.	
		inline void read(void *t_ptr, size_t t_size, size_t t_nmemb) {
			if(fread(t_ptr, t_size, t_nmemb, fp) != t_nmemb)
			 throw NEW_ERROR("fread(): not enough bytes read",EC_STOP_EXECUTION);
		}
		
		//! wraps the fwrite() function
		//! \exception ErrorClass Throwen if less than @a t_size bytes were written.
		inline void write(const void *t_ptr, size_t t_size, size_t t_nmemb) {
			if(fwrite(t_ptr, t_size, t_nmemb, fp) != t_nmemb)
			 throw NEW_ERROR("fread(): not enough bytes read",EC_STOP_EXECUTION);
		}
		
		//! wraps the fstat() function
		inline void stat(struct stat* t_buf) {
			if(fstat(this->get_fd(), t_buf)!=0)
			 throw NEW_ERROR("error calling fstat()",EC_STOP_EXECUTION);
		}
	
		//! wraps the fputs() function
		inline void put(const char* t_s) {
			//puts("DEEEEEEEEEEEEEEEBUG!");
			//std::cout << "ftell: " << tell() << "t_s=" << t_s << endl;
			if( fputs(t_s, fp) == EOF )
			 throw NEW_ERROR("error calling fputs()",EC_STOP_EXECUTION);
		}

		//! wraps the feof() function
		inline bool eof() throw() {
			return (bool)feof(fp);
		}
		
		//! \brief wraps the fgetc() function
		//! @return EOF only if EOF, otherwise the read character.
		//! @exception throwen on errors, but not on EOF
		int getc();

		//! \brief wraps the fgets() function
		//! @return NULL only if EOF, otherwise the read string.
		//! @exception throwen on errors, but not on EOF
		char* gets(char* t_s, int t_n);
		
		//! \brief wraps the ftruncate() function
		//! \note this function is platform independent
		void truncate(off_t t_length);

		//! wraps the fseek() function
		void seek(long t_offset, int t_whence);
		
		//! wraps the ftell() function
                unsigned long tell(void);
		
		//! \brief wraps the acces() function
		//! @return true if @a t_path could be accessed, otherwise false
		static bool access(const char* t_path, int t_amode);

		/*
			Non wrapper functions
		*/
		void read_string( char* s, unsigned int max );
		void read_string( string* s );
		template <class T>
		inline void read_vector(vector<T>* v, void (*fptr)(T*, FILE*), int num) {
			int count;
			//printf("reading vector of %d members at ftell() %ld\n",num,ftell(oof_fp));
			v->reserve(num);
			for(count=0; count<num; count++)
			{
				T x;
				fptr(&x, fp);
				v->push_back(x);
			}
		}
		template <typename T, int t_length>
		static void read_n_bytes ( T* i, FILE* oof_fp ) {
			t_fread(i, t_length, 1, oof_fp);
		}
		
		

};

} // namespace odf

#endif // TFILE_H
