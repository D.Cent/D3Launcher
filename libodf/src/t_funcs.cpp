/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
using namespace std;

#include "defines.h" // for DEBUGF

#include "t_funcs.h"

namespace odf {

/*
	function definitions
*/
/*
bool t_access(const char* t_path, int t_amode)
{
	bool res = (bool) 1 + access(t_path, t_amode);
	printf("t_access result: %d\n",(int)res);
	switch(errno) {
		case 0: 	break;
		case EINVAL:	throw NEW_ERROR("2nd argument of access() is wrong",EC_LOGIC_ERROR|EC_STOP_EXECUTION);
		default:	throw NEW_ERROR("calling access()",EC_CONTINUE);
	}
	return res;
}*/

bool t_access(const char* t_path, int t_amode)
{
	bool res=true;
        if( access(t_path, t_amode) == -1 )
         switch(errno) {
                case EACCES:
		case ENOENT:
		case ENOTDIR:
			res = false;
		break;
                case EINVAL: throw NEW_ERROR("2nd argument of access() is wrong",EC_LOGIC_ERROR|EC_STOP_EXECUTION);
                default: throw NEW_ERROR("calling access()",EC_CONTINUE);
         }
        return res;
}

void t_ftruncate(FILE *t_fp, off_t t_length)
{
	if(t_length<0)
	 throw NEW_ERROR("2nd argument of ftruncate() is less than 0",EC_LOGIC_ERROR|EC_STOP_EXECUTION);
#ifdef __LINUX__
	if( ftruncate(fileno(t_fp),t_length) )
#else // windows has no fileno() o_O
	if( ftruncate(t_fp->_file,t_length) )
#endif	
	 throw NEW_ERROR("calling ftruncate()",EC_CONTINUE);	
}

void t_fseek(FILE *t_stream, long t_offset, int t_whence)
{
	DEBUGF("calling fseek(fp(%ld),%ld,%d)\n",ftell(t_stream),t_offset,t_whence);
	//if( (ftell(t_stream)+t_offset) < 0 ) // if 2nd arg is wrong
	// .... 
	if(! (t_whence==SEEK_SET||t_whence==SEEK_CUR||t_whence==SEEK_END) )
	 throw NEW_ERROR("3rd argument of fseek() is wrong",EC_STOP_EXECUTION|EC_LOGIC_ERROR);
	if( fseek(t_stream, t_offset, t_whence) != 0 ) // call fseek()
	 throw NEW_ERROR("error calling fseek()",EC_STOP_EXECUTION);
}

FILE* t_fopen(const char *t_path, const char *t_mode)
{
	DEBUGF("t_fopen(): opening %s\n",t_path);
	FILE* t_fp = fopen(t_path, t_mode);
	if(t_fp == NULL)
	 switch(errno) {
		case EINVAL:	throw NEW_ERROR("2nd argument of fopen() is wrong",EC_LOGIC_ERROR|EC_STOP_EXECUTION);
		default:	throw NEW_ERROR("calling fopen()",EC_CONTINUE);
	 }
	return t_fp;
}

int t_getc(FILE* t_stream)
{
	int ret_val = getc(t_stream);
	if( ret_val == EOF && !feof(t_stream) )
	 throw NEW_ERROR("calling getc()", EC_CONTINUE);
	return ret_val;
}

void t_read_string( char* s, unsigned int max, FILE* fp )
{
	char* ptr = s;
	for( register unsigned int n = 0; n<max && (*ptr)!=0 ; n++, ptr++) {
		t_fread( ptr, 1, 1, fp );
	}
}

void t_read_string( std::string* s, FILE* fp )
{
	register char c;
	do {
		c = fgetc( fp );
		s->push_back( c );
	} while( c != 0 );
}

} // namespace odf
