/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <string>
using namespace std;

#include "regfile.h"

namespace odf {

void RegFile::get_relevant_data(string* str)
{
	for(string::iterator itr = str->begin(); itr!=str->end(); itr++)
	{
        	if(isgraph(*itr)||(*itr)=='\n') { // ignore trailing whitespace
			if(itr != str->begin())
			 str->erase(str->begin(), itr);
			break;
		}
	}
// TODO : reverse stuff, comments	
	
}


bool RegFile::pack_into_quotes(string* str) const
{
	if( ! str->compare(0, 6, "dword:") )
	 return false;
	str->insert(str->begin(),'"');
	str->push_back('"');
	return true;
}

string RegFile::get_section_name(const string& line) const
{
	if( ! line_is_section(line) )
	 return string();
	
	string result = line.substr(1,line.length()-2); // TODO: sure? [sectionname]bla
	get_relevant_data(&result);
	return result;
}

} // namespace odf
