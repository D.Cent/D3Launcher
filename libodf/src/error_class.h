/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file	error_class.h
	\brief	This file contains ErrorClass

	ErrorClass is the class that will be thrown by libodf to detect errors.
	To create an object, use the NEW_ERROR macro, which will first expand to the
	ErrorLocation class, which is a helper class to set up an ErrorClass.
	Include this file whenever you want to throw() or catch() an ErrorClass.
*/

#ifndef ERRORS_H
#define ERRORS_H ERRORS_H

#include <cstring> // for strncpy()

namespace odf {

// TODO: remove this ugly stuff!
//#define dthrow(_exc) throw(_exc)
/*

Errors can:

 -be harmless
 -corrupt the hogfile (with throw or itself)
 
 
 
 -be runtime errors
 -be programmers errors




 -be memory problems or not
 -can be mathematical (arithmetic) or not
 
*/

/*

Which errors are relevant for the calling (catching) programme ?

-cannot continue execution
-data was corrupted before execution
-data was corrupted by programme





*/

// Incorrect number of files passed in
// Hog file was created successfully
// Could not allocated hog entry table
// Error occurred writing to output hog file
// An input file could not be found (filename is stored in hogerr_filename)
// An error occurred copying an input file into the hog file
// The specified hog file could not be opened for output

//! \def EC_CONTINUE
//! no serious error, the program can continue
#define EC_CONTINUE		    0
//! \def EC_STOP_EXECUTION
//! execution has to be stopped
#define EC_STOP_EXECUTION	(1<<0)
//! \def EC_LOGIC_ERROR
//! error of the programmer (e.g. error using a header from another project)
#define EC_LOGIC_ERROR		(1<<1)
//! \def EC_NEXT
//! the next free bit that can be used for user specific errors
#define EC_NEXT			(1<<2)


/*#define HF_CORRUPTED_BEFORE	(1<<1)	// hog File has already been corrupted before execution
#define HF_CORRUPTED_SELF	(1<<2)	// the programme itself corrupted the hogfile
#define HF_CORRUPTED		( HF_CORRUPTED_BEFORE | HF_CORRUPTED_SELF )
#define HF_DATA_LOSS_BEFORE	(1<<3)	// the hogfile has had data loss before execution
#define HF_DATA_LOSS_SELF	(1<<4)	// the hogfile got data loss during execution
#define HF_DATA_LOSS		( HF_DATA_LOSS_BEFORE | HF_DATA_LOSS_SELF )*/






/*

//#define HF_ERR_HARD	(1<<0)
//#define HF_ERR_SOFT
#define HF_ERR_UNDEF	    0  // error not specified
#define HF_ERR_RUNTIME	(1<<0) // is it a runtime error or an error of the programmer?
#define HF_ERR_ARG	(1<<1) // arglist is too long or invalid argument parsed? (e.g. fseek, seeking to negative values)
#define HF_ERR_MEM	(1<<2) // is it an error with memory? (e.g. buffer too small, a bad alloc...)
#define HF_ERR_MATH	(1<<3) // is it a math. error? (e.g over/underflow)
#define HF_ERR_PERM	(1<<4) // no permission to do something
#define HF_ERR_FILE	(1<<5) // error has to do with files
*/

} // namespace odf

#include <cstdio> // for perror()
#include <cerrno>

using namespace std;

namespace odf {

// TODO: 
// host/socket erros (not for hogfiles, but for odf lib)
// runtime error ?
// print error to file

/**
	\def STR_LEN
	maximum length of error strings. if the string is longer, it will be cut.
*/
#define STR_LEN 256

// These macros should be called to create an instance of ErrorClass
// #define NEW_ERROR(x...) ErrorClass(__FILE__,__LINE__,x) // OLD - INCOMPATIBLE MACROS?

/**
	\def NEW_ERROR
	use this whenever you want to throw an error. It uses the ErrorLocation class to tell ErrorClass where the error occured
*/
#define NEW_ERROR ErrorLocation(__FILE__, __LINE__)

/**
	\class ErrorClass
	\brief libodf's throwable error class

	ErrorClass is the class that will be thrown by libodf to detect errors.
        It stores a string with the error, the file and line number and the errno variable.
	Additionally, flags inform the program about the type of error.
*/
class ErrorClass { // TODO inherit std error class
	
	private:
		
		char err_file[STR_LEN]; // the file the error occured in ...
		unsigned int err_line; // ... and the line number
		
		char what[STR_LEN]; // Here, the reason or descripion for the error is stored
		unsigned char err_type; // ubyte for defined errors (see above)
		
		int err_save; // here, the errno-error will be saved
	
	protected:
		
		//bool hard; // if data was corrupted by the error or the throw, hard is true
		//void set_hard() {} // this will be overwritten by other classes, see below
		
	public:
		/**
			The constructor stores the given parameters and errno in the class
			@param _err_file Pointer to the current file's name
			@param _err_line Pointer to the crrent line number in the file
			@param _what Description of the error
			@param _err_type Typeflags describing the error
		*/
		ErrorClass( const char* _err_file, unsigned int _err_line, const char* _what, unsigned char _err_type = EC_STOP_EXECUTION )
		{
			err_save=errno; // save errno
			strncpy(what, _what, STR_LEN);
			strncpy(err_file, _err_file, STR_LEN);
			err_line = _err_line;
			err_type = _err_type;
			//printf("New error in %s, %d: _err_type=%d\n",err_file,err_line,(int)_err_type);
		//	set_hard();
		}
		
		//! This function returns the filename of the file where the ErrorClass was thrown.
		inline const char* get_file(void) const { return err_file; } // for constant objects
		
		//! This function returns the line number where the ErrorClass was thrown.
		inline unsigned int get_line(void) const { return err_line; }
		
		//! This function returns the type flags of the ErrorClass
		inline unsigned char type(void) const { return err_type; }
		
		//! This function returns to what number errno was set when the error occured
		inline int get_errno(void) const { return err_save; }
		
		//! This returns a pointer to the error description
		inline const char* description(void) const { return what; }
		
		//! Prints the error including the errno message and the file and the line where this ErrorClass was thrown
		void print_error(void) const
		{
			fprintf(stderr,"in file %s, line %d:\n",err_file,err_line);
			
			//perror(what);
			// (errno) ? perror(what) : puts(what);
			
			if(errno)
				perror(what);
			else
				puts(what);
			
		}
				
};

/**
	\class ErrorLocation
	\brief Helper Class to avoid macros with arg lists
	
	Use of
	\code
		NEW_ERROR("Shot me in the foot")
	\endcode
	would expand to:
	\code
		ErrorLocation("pistol.cpp", 33)("Shot me in the foot")
	\endcode
	which creates an unnamed object of class ErrorLocation, passes the filename and linenumber to it's constructor,
	then calls it's operator() with the provided message.
*/

class ErrorLocation
{
	private:
		const char* file;
		int line;
	public:
		ErrorLocation(const char* f, int l) : file(f), line(l) {}
		
		ErrorClass operator()(const char* _what, unsigned char _err_type = EC_STOP_EXECUTION) const { // functor
			return ErrorClass(file, line, _what, _err_type);
		}
};

#undef STR_LEN

} // namespace odf

#endif // ERRORS_H
