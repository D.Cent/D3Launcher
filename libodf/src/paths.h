/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
        \file paths.h
        \brief functions and classes helping you with paths

	This file provides you with useful, system independent workarounds with directories.
*/

#ifndef ODF_PATHS_H
#define ODF_PATHS_H ODF_PATHS_H

#include <string> // C++ string
#include <cstdlib> // for getenv()
#include <ostream> // for operator << on Path class
using namespace std;

#include "error_class.h"
#include "file_funcs.h" // for DIR_SEP // TODO ?

namespace odf {

//! \def HOME_VAR
//! Defined as the name of the variable containg your OS' home path
#if defined (__LINUX__)
	#define HOME_VAR "HOME"
#else
	#define HOME_VAR "APPDATA" 
#endif

//! returns filename without path
inline const char* get_filename(const char *s) throw() {
        return (strchr(s,DIR_SEP)) ? strrchr(s,DIR_SEP)+1 : s;
}

// TODO: does every returned dir end with / or \ ?
// 

//! if dirstring is not ending on DIR_SEP, it will be appended
void append_dir_sep( string& dirstring ) throw(); // -> paths.cpp

/**
	\class Path
	\brief basic class for handling paths

	This class contains the name of a path as a string. The class is useful for concatenating directories.
	Concatenating always makes sure that a dir seperator is added between the two pieces.
*/
class Path
{
	private:
		string pathname;
	public:
		//! readonly path name
		const string& name;
		//! basic constructor, leaves path name empty
		Path() : name(pathname) { }
		//! basic constructor, initializes the path name with a C string
		Path( const char* _str ) : pathname(_str), name(pathname) { }
		//! basic constructor, initializes the path name with a STL string
		Path( const string& _str ) : pathname(_str), name(pathname) { }
		//! virtual destructor, does nothing
		virtual ~Path() { }
		//! concatenates with C string
		virtual string operator+( const char* appendix ) const throw();
		//! concatenates with STL string
		virtual inline string operator+( const string& appendix ) const throw() {
			return operator+( appendix.c_str() );
		}
		//! concatenates 2 paths
		virtual inline string operator+( const Path& appendix ) const throw() {
			return operator+( appendix.c_str() );
		}
/*		virtual inline string operator+( char appendix ) throw() {
			return operator+( appendix );
		}
*/		//! adds a C string to the pathname
		virtual const string& operator+=( const char* appendix ) throw();
		//! adds a STL string to the pathname
		virtual inline const string& operator+=( const string& appendix ) throw() {
			return operator+=( appendix.c_str() );
		}
		//! adds a path to pathname
		virtual inline const string& operator+=( const Path& appendix ) throw() {
			return operator+=( appendix.c_str() );
		}
/*		virtual inline const string& operator+=(  char appendix ) throw() {
			return operator+=( appendix );
		}
*/		
		//! returns reference to an stl string
		inline const string& str() const throw() { return pathname; } // readonly
		//! returns the pathname as a C string
		inline const char* c_str() const throw() { return pathname.c_str(); } // readonly

};

/**
	\brief allows a Path to be used with an ostream
	
	\code
		HomePath hp;
                cout << "Your home path should be: " << hp << endl;
	\endcode
*/
inline ostream& operator<<(std::ostream& stream,const Path& p)
{
	return(stream<<p.name);
}

//! \brief returns your system's homepath using getenv
//! \note The returned string must not end on a dir seperator! Better use the HomePath struct
const char* get_homepath( void ); // -> pahs.cpp

//! \brief returns your system's sharepath where libodfs programmes may be stored
//! \note The returned string must not end on a dir seperator! Better use the SharePath struct
inline const char* get_sharepath( void )
{
#if defined(__LINUX__)
	return "/usr/local/share/ODF/";
#elif defined(__WIN32__)
	//return getenv("PROGRAMMFILES");
	return ".";
#endif
}

/**
	\class CommonPath
	\brief class for handling paths that should be concatenated during initialization

	Give the class the basic string in the template brackets. Use the initialiser for appending.
	\code
		CommonPath<HomePath> hp_subdir(".loki");
		cout << "Use the CommonPath<HomePath> class to use subdirs like: " << hp_subdir << endl;
	\endcode
*/
template <class BaseString>
class CommonPath : public Path
{
	public:
		//! Initialisation without appendix
		CommonPath() : Path( BaseString() ) {}
		//! Initialisation with C string appendix
		CommonPath( const char* _appendix ) : Path( BaseString() + _appendix )  {}
		//! Initialisation with STL string appendix
		CommonPath( const string& _appendix  ) : Path( BaseString() + _appendix )  {}
		//! Initialisation with Path class appendix
		CommonPath( const Path& _appendix  ) : Path( BaseString() + _appendix )  {}
};

/**
	\class HomePath
	\brief class for users' home path

	This class contains a home path, i.e. for linux users "$HOME"
*/
class HomePath : public Path {
	//! The default constructor. Use class CommonPath for appending instead.
	public: HomePath() : Path( get_homepath() ) {}
};

/**
	\class SharePath
	\brief class for ODF programs' share path

	This class contains a path where all ODF programmes shall be stored at.
	For linux users, this is always "/usr/local/share/ODF", for windows users "$PROGRAMFILES"
*/
class SharePath : public Path {
	//! The default constructor. Use class CommonPath for appending instead.
	public: SharePath() : Path( get_sharepath() ) {}
};

inline string get_regpath(void)
{
#if (defined __LINUX__) || (defined __BSD__)
	return CommonPath<HomePath>(".loki/descent3/.Descent3Registry").str();
#else
/*	string ret(getenv("win32"));
	return ret + "System32\\Config";*/
	throw NEW_ERROR("This functionality is not implemented.", EC_STOP_EXECUTION);
#endif
}


/**
	\class D3RegPath
	\brief class for Registry file which contains the Descent3 sections

	For Linuxers always ~/.loki/descent3/.Descent3Registry, On windows the Registry
 */
class D3RegPath : public Path {
	//! The default constructor. Use class CommonPath for appending instead.
	public: D3RegPath() : Path( get_regpath() ) {}
};

} // namespace odf

#include "regfile.h"

namespace odf {

inline string get_d3path(void)
{
	RegFile D3Registry(get_regpath().c_str());
#if (defined __LINUX__) || (defined __BSD__)
	return D3Registry.get_single_entry("local directory","Descent3");
#else
	//return D3Registry.get_single_entry("BaseDirectory","HKEY_LOCAL_MACHINE\\SOFTWARE\\Outrage\\Descent3");
	if(checkdir("C:\\Games\\Descent3"))
	 return string("C:\\Games\\Descent3");
	else
	 throw NEW_ERROR("Could not find Descent3 in C:\\Games\\Descent3, aborting");
#endif
}

/**
	\class D3HomePath
	\brief the home path where D3 is stored

	\note for loki users, this is the path in $HOME/.loki/descent3, not the sharepath
 */
class D3HomePath : public Path {
	//! The default constructor. Use class CommonPath for appending instead.
#if (defined __LINUX__) || (defined __BSD__)
	public: D3HomePath(): Path( CommonPath<HomePath>(".loki/descent3") ) {}
#else
	public: D3HomePath(): Path( get_d3path() ) {}
#endif
};

/**
	\class D3SharePath
	\brief the share path where D3 is stored

	\note for loki users, this is root's path, not the .loki/descent3
*/
class D3SharePath : public Path {
	//! The default constructor. Use class CommonPath for appending instead.
#if (defined __LINUX__) || (defined __BSD__)
	public: D3SharePath(): Path( get_d3path() ) {}
#else
	public: D3SharePath(): Path( D3HomePath().c_str() ) {} // equal to D3HomePath
#endif
};

/**
	\class AppPaths
	\brief a collection of paths for an application

	This class contains
	- a homepath for user specific data
	- a sharepath for data specific to the machine
*/
class AppPaths
{
	public:
		// readonly info
		//! the application's name
		const string name;
		//! the application's homepath ( HomePath + application name, hidden for linux )
		const CommonPath<HomePath> homepath;
		//! the application's sharepath ( SharePath + application name )
		const CommonPath<SharePath> sharepath;
		
		//constructor
		//! basic constructor, creates default paths and appends the appliucation name
		AppPaths(const char* _app_name):
			name(_app_name),
#if defined(__LINUX__)
			homepath("."+name), // . hides dirs on linux
			sharepath(name) {}
#elif defined(__WIN32__)
			homepath(name), // no . for windows
			sharepath("/data/") {}
#endif
};

} // namespace odf

#endif // ODF_PATHS_H
