/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file os_fix.h
	\brief Operating System compability functions

	This is a completion of functions which are not available at some systems
*/

#ifndef OS_FIX_H
#define OS_FIX_H OS_FIX_H

#ifndef __WIN32__
	#include <features.h>
#endif

namespace odf {

#ifdef __WIN32__ // Windows has no sleep()
/**
	Suspends execution for an interval of time
	@param secs Number of seconds to sleep.
	@return The return value is always 0;
*/
unsigned int sleep(unsigned int secs);

#endif // __WIN32__

/**
	Suspends execution for an interval of time
	@param msecs Number of milli seconds to sleep
	@return false if msecs >= 1000 (which is one second), or true otherwise
*/
bool msleep(unsigned short msecs);

} // namespace odf

#endif // OS_FIX_H

