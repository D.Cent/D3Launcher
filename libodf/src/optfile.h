/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file optfile.h
	\brief This file contains the OptFile class
*/

#ifndef OPTFILE_H
#define OPTFILE_H OPTFILE_H

#include <cstdio>
#include <map>
#include <string>
using namespace std;

#include "error_class.h"
#include "tfile.h"
#include "file_funcs.h"

namespace odf {

/**
        \class OptFile
        \brief This class can handle (read and write) D321GO's OptFiles.

        The file can look like this:
        @code
        section=section1
        "key1"="value1 with spaces"
        "key2"=Value2WithoutSpaces # comment(s)
        ...
        section=section2
        ...
        @endcode
*/
class OptFile : private tFILE
{
	protected:
		//! @brief Helper Class to parse the Optfile.
		class StartPosition
		{
			public:
				StartPosition(tFILE* _tf)
					: tf(_tf), before_start(false),
					     start_pos(_tf->tell()), last_pos(_tf->tell())
					      {}
				
				inline bool overstepped() {
					//if(tf->tell() <= last_pos)
					if( tf->tell() < last_pos )
					 before_start = true;
					last_pos = tf->tell();
					//return (before_start && tf->tell()>start_pos);
					//printf("OVERSTEPPED?!?: now %ld, start: %ld, before: %d\n",tf->tell(), start_pos, (int)before_start);
					//CHANGED BY LO ON 31/10/2009
					return (before_start && (signed int)start_pos-(signed int)tf->tell() <3); //return (before_start && tf->tell()>start_pos);
				}
			private:
				tFILE* tf;
				bool before_start;
				unsigned long start_pos, last_pos;
		};
		
		const char* filename;
		struct stat filestats;
		bool sections; // is the optfile split in sections?
		
		/**
			Parses the optfile for one line to return it in two pieces
			@param optline Line to parse (e.g.:"   a=b#..."). This string might get shortened (to "a=b")
			@param opt1 Pointer to a string where the part left of the eual(=)-sign is stored (a).
			 Must point to enough space!
			@param opt2 Pointer to a string where the part right of the eual(=)-sign is stored (b).
			 Must point to enough space!
			@return either 0 on empty lines, or the lenght of the modified optline string ("a=b")
		*/
		unsigned int get_stringpair(string* optline, string* opt1, string* opt2) const;
		
		/**
			Parses an optfile for the next line which is containing a stringpair ("a=b")
			@param opt1 Pointer to a string where the part left of the eual(=)-sign is stored (a).
			 Must point to enough space!
			@param opt2 Pointer to a string where the part right of the eual(=)-sign is stored (b).
			 Must point to enough space!
			@return either 0 on read error or EOF, or the lenght of the optline before being shortened ("a=b #...")
		*/
		unsigned int get_stringpair(string* opt1, string* opt2);

		/**
			Moves the optfiles filepointer to the begin of a new section. It will be moved behind the 
			introducing "section=..." line, i.e. to the first stringpair of the new section.
			@param section The section to look for
			@return false on read error or EOF (i.e. section was not found), otherwise true
		*/
		bool skip_to_section(const char* section);
		
		bool fget_stlstring(string* str);
		static bool remove_quotes_from_string(string* str);
		
		//virtual unsigned int real_string_length(const string& str) const;
		
		virtual string get_section_name(const string& line) const;
		
		// ignore newlines and wins registry sections
		virtual /* inline*/ /*static*/ bool line_is_useful(const string& line) const // TODO: make it inline
		{
			//printf("line is useful? %s\n",( line.length() != 0 && line[0]!='[' && line.find('=') != string::npos)?"true":"false");
			return ( line.length() != 0 && line[0]!='[' && line.find('=') != string::npos );
		}
		
		virtual bool line_is_section(const string& line) const;
		
		static void get_relevant_data(string* line);
		
		//! add quotes to string str - IF NECESSARY (i.e. if the string contains whitespace or unprintable chars)
		virtual bool pack_into_quotes(string* str) const;
		
	public:
		/**
			Constructor, which will open the optfile
			@param _filename Name of the file that should be opened. Must be a seekable file.
			@param _sections If the file is devided in sections, this should be true, otherwise false
			@param _openmod Mod to open OptFile. Uses tFILE::open(), conform to fopen().
		*/
		OptFile(const char* _filename, bool _sections = false, const char* _openmod = "r+b"):
		 tFILE(_filename, _openmod),
		 filename(_filename), sections(_sections) {
			stat(&filestats);
		}
		
		//! virtual Destructor, which will close the file
		virtual ~OptFile() {}
		
		//! Rewinds the optfile's filepointer (only necessary for runtime advantages)
		using tFILE::rewind;
		// inline void rewind(void) { ::rewind(optfile); }
		
		
		/**
			Searches for whether the file contains a string key (a in a=b)
			@param entry Parameter that is searched for.
			@param section Section which shall be searched in. Only for optfiles with sections.
			@return true if the key was found, otherwise false
		*/
		bool contains_entry(const char* entry, const char* section=NULL);
			
		/**
			Reads the optfile or parts of it into an std::map.
			@param m Map where the optfile or its parts shall be stored in.
			@param section Section which shall be stored. Only for optfiles with sections.
		*/
		void to_map(map<string,string>& m, const char* section=NULL);

		/**
			Writes an std::map into a file. The file will be updated.
			@param m Map where the data is stored in.
			@param section Section which is represented by the data. Only for optfiles with sections.
		*/
		void from_map(const map<string, string>& m, const char* section=NULL);
		
		/**
			Searches for a stringpair (a=b), where the left parameter (a) is equal to \a left
			@param left Parameter that is searched for.
			@param section Section which shall be searched in. Only for optfiles with sections.
			@return The right value of the stringpair (b). If an error occured, an ErrorClass is thrown!
			 Errors can be read errors, end of section or EOF (which means that the left value is not found)
			@note To prevent exceptions, use contains_entry() !
		*/
		string get_single_entry(const char* left, const char* section=NULL);
		
		/**
			Searches for a stringpair (a=b), where the left parameter (a) is equal to \a left, and 
			replaces the right parameter with \a right
			@param left Parameter that is searched for.
			@param right Replacement for the value right ot \a left
			@param section Section which shall be searched in. Only for optfiles with sections.
			@param str_in_quotes writes strings (all except of registry dword entrys) in quotes ("a"="b")
			@return The right value of the stringpair (b). If an error occured, an ErrorClass is thrown!
			 Errors can be read errors, end of section or EOF (which means that the left value is not found)
			@note To prevent exceptions, use contains_entry() !
		*/
		void set_single_entry(const char* left, const char* right, const char* section=NULL, bool str_in_quotes=false);

		void add_single_entry(const char* left, const char* right, const char* section=NULL);
};

} // namespace odf

#endif // OPTFILE_H

