/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdlib>
#include <cctype>
#include <cstdio>
#include <iostream>
using namespace std;

#include "file_funcs.h"
#include "defines.h"

#include "t_funcs.h" // for t_fopen(), t_fseek() etc...

namespace odf {

#ifndef __LINUX__
	#define BUFFER_SIZE (1024*1024) // 1MB
#endif

bool block_is_print(FILE *fp, int _block_len)
{
	register int i;
	register char c;
	bool res=true;
	
	for(i=0; (i<_block_len && res); ++i) {
		c=t_getc(fp);
		if(c == EOF) // we hit end of file
		 throw NEW_ERROR("calling block_is_print(): block reached EOF", EC_CONTINUE);
		res=(isprint(c)||isspace(c));
	}
	
	t_fseek(fp,-i,SEEK_CUR); // go back to old position
	return res;
}

bool file_is_print(const char* _filename)
{
	struct stat f_stat;
	t_stat(_filename,&f_stat);
	
	FILE* fp = t_fopen(_filename,"rb");
	bool ret = block_is_print(fp, f_stat.st_size);
	t_fclose(fp);
	
	return ret;
}

void block_copy(FILE *fp, int length, int move)
{
	char *buffer;
	int start_length=length;
	size_t read_len;
#ifdef __LINUX__
	const int BUFFER_SIZE = getpagesize();
#endif
	DEBUGF("block_copy(ftell()=%ld,len=%d,move=%d)\n",ftell(fp),length,move);
	
	if ( !move || !length ) {
		t_fseek(fp,move,SEEK_CUR); // do this to be compatible!
		return; // nothing to do ... :)
	}
	buffer = new char[BUFFER_SIZE];
		
	if(move > 0)
	 t_fseek(fp,length - _MIN(BUFFER_SIZE,length),SEEK_CUR);  // if we move forward, we jump before the last element
		
	while ( length > 0 )
	{
		size_t n;
		read_len = _MIN(length,(int)BUFFER_SIZE);
		DEBUGF("Moving %d bytes from %d to %d\n",(int)read_len,(int)ftell(fp),(int)ftell(fp)+move);
		
		n = fread( buffer, 1, read_len, fp); // no throw (t_fread) here, because we first have to free the buffer!
		if ( n != read_len )	{
			delete[] buffer;
			throw NEW_ERROR("fread(): not enough bytes read",EC_STOP_EXECUTION);
		}
		
		t_fseek(fp,(-read_len+move),SEEK_CUR); // a.) go before the block you have read and b.) go to the position we want to move to
		SYNC_WRITE_BEGIN(fp);
		n = fwrite( buffer, 1, read_len, fp);
		SYNC_WRITE_END(fp);
		if ( n != read_len )	{
			delete[] buffer;
			throw NEW_ERROR("fwrite(): not enough bytes written",EC_STOP_EXECUTION);
		}
		
		//printf("fseek()= ->%d,ftell()=%d",-move-(2*read_len),(int)ftell(fp));
		
		length -= read_len;
		
		/* bad commented! */
		if(move>0) // if we move forwards, 
		 //t_fseek(fp,MIN((-3*BUFFER_SIZE),move),SEEK_CUR); //old and wrong!
		 
		 // move the file pointer
		 // 1.) back to where we started the fwrite (read_len)
		 // 2.) <move> bytes back
		 // 3.) before the next possible block
		 t_fseek(fp,-read_len-move-_MIN(length,(int)read_len),SEEK_CUR);
		else // if we move backwards, jump to the next segment
		 t_fseek(fp,-move,SEEK_CUR);
	}
	
	// go back to the position of the beginning:
	if(move>0)
	 t_fseek(fp,move,SEEK_CUR);
	else // move<0
	 t_fseek(fp,-start_length,SEEK_CUR);
	
	delete[] buffer;
}

void block_remove(FILE *fp, int block_length, int filelength)
{
	if(block_length>0)
	 t_fseek(fp, block_length, SEEK_CUR);
	else
	 block_length=-block_length;
	block_copy(fp, filelength-ftell(fp), -block_length);
	t_ftruncate(fp, filelength-block_length);
}

void block_insert(FILE *fp, int block_length, int filelength)
{
	if(block_length<0)
        	block_copy(fp, filelength-ftell(fp), -block_length);
	else {
		block_copy(fp, filelength-ftell(fp), block_length);
		t_fseek(fp, -block_length, SEEK_CUR);
	}
}

void file_copy(FILE *src_fp, FILE* dest_fp, int length)
{
#ifdef __LINUX__
	const int BUFFER_SIZE = getpagesize();
#endif

	DEBUGF("file_copy(): src_fp: %ld, dest_fp: %ld, length=%d\n",ftell(src_fp),ftell(dest_fp),length);
	char *buffer;
	buffer = new char[BUFFER_SIZE];
	while (length>0) 
	{
		size_t n,read_len;
		read_len = _MIN(length,(int)BUFFER_SIZE);
		n = fread( buffer, 1, read_len, src_fp );
		if ( n != read_len )	{
			delete[] buffer;
			throw NEW_ERROR("fread(): not enough bytes read",EC_STOP_EXECUTION);
		}
		n = fwrite( buffer, 1, read_len, dest_fp);
		if ( n != read_len )	{
			delete[] buffer;
			throw NEW_ERROR("fwrite(): not enough bytes written",EC_STOP_EXECUTION);
		}
		length -= read_len;
	}
	delete[] buffer;
}


bool seek_after_str(FILE* fp, const char* comp_str)
{
        unsigned int len = strlen(comp_str);
        char* queue_buf = new char[len];
	register char c;
        memset(queue_buf,78,len); // fill buffer
        do {
                memmove(queue_buf,queue_buf+1,len-1);
                c = getc(fp);
		if(feof(fp))
		{
			delete[] queue_buf;
			return false;
		}
                queue_buf[len-1]=c;
        } while(strncmp(queue_buf,comp_str,len));
	delete[] queue_buf;
	return true;
}

// DEPRECATED
/*#include <iostream>
bool seek_after_str(iostream& seek_stream, const char* comp_str)
{
	unsigned int len = strlen(comp_str);
        char* queue_buf = new char[len];
	register char c;
        memset(queue_buf,78,len); // fill buffer
        do {
                memmove(queue_buf,queue_buf+1,len-1);
                c = (char) seek_stream.get();
		if(seek_stream.eof()){
			delete[] queue_buf;
			return false;
		}
                queue_buf[len-1]=c;
        } while(strncmp(queue_buf,comp_str,len));
	delete[] queue_buf;
}*/

bool seek_to_str(FILE* fp, const char* comp_str) {
        bool ret = seek_after_str(fp, comp_str);
        t_fseek(fp,-strlen(comp_str),SEEK_CUR);
        return ret;
}

// DEPRECATED
/*bool seek_to_str(iostream& seek_stream, const char* comp_str) {
        bool ret = seek_after_str(seek_stream, comp_str);
        seek_stream.seekg(-strlen(comp_str),ios::cur);
}*/

bool cp(const char* _src, const char* _dest, bool is_bin, bool overwrite)
{
	FILE *src_fp, *dest_fp;
	struct stat if_stat;
	
	if(overwrite && t_access(_dest, F_OK))
	 return false;	

	t_stat(_src,&if_stat);

	src_fp=t_fopen(_src,is_bin?"rb":"r");
	dest_fp=t_fopen(_dest,is_bin?"wb":"w");
	
	file_copy(src_fp, dest_fp, if_stat.st_size);

	t_fclose(dest_fp);
	t_fclose(src_fp);
	return true;
}

#ifndef __LINUX__
	#undef BUFFER_SIZE
#endif

#include <dirent.h>

bool checkdir ( const char *path ) throw()
{

	DIR *directory;
	directory=opendir(path);

	if(directory==NULL)
	 return false;
	else {
		closedir(directory);
		return true;
	}

}

bool read_line_seek(FILE* fp, char*& str, char trailer, unsigned int maxsize)
{
	char to_read=trailer+1;
	int i=0;

	for(; to_read != trailer && !feof(fp); ++i) {
		to_read=fgetc(fp);
		if(feof(fp)) return false;
	}
	
	if(maxsize && (unsigned int)i>maxsize)
	 throw NEW_ERROR("Line was too long for allocation!",EC_CONTINUE);
	
	t_fseek(fp,-i,SEEK_CUR);

	str = new char[i];
	t_fread(str,sizeof(char),i,fp);
	str[i-1]='\0';
	return true;
}

void dump_file(FILE* src_fp, FILE* dest_fp, unsigned int maxlen)
{
	if(maxlen)
	for (register unsigned int count = 0; !feof(src_fp); count++)
	{
		if(maxlen && count>=maxlen)
		 break;
		fputc(fgetc(src_fp),dest_fp);
	}
	else
	 while(!feof(src_fp))
	  fputc(fgetc(src_fp),dest_fp);
}

} // namespace odf
