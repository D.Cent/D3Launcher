/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file defines.h
	\brief general macros and typedefs
	
	All types, defines(macros), typedefs etc. are defined here!
*/

#ifndef ODF_DEFINES_H
#define ODF_DEFINES_H ODF_DEFINES_H

namespace odf {

//! \def _MIN(a,b)
//! Returns minimum of \a a and \a b with operator<
#define _MIN(a,b) (((a)<(b)) ? (a) : (b))

//! \def _MAX(a,b)
//! Returns maximum of \a a and \a b with operator>
#define _MAX(a,b) (((a)>(b)) ? (a) : (b))

#ifdef __LINUX__
//! \todo remove that
	#define stricmp(a,b) strcasecmp(a,b)
	#define strnicmp(a,b,c) strncasecmp(a,b,c)
#endif

#ifdef LIBODF_DEBUG
	//! \def DEBUG(x)
	//! puts out a string if LIBODF_DEBUG is set
	#define DEBUG(x) fputs(x, stderr);
	//! \def DEBUGF(x)
	//! puts out a formatted string if LIBODF_DEBUG is set
	#define DEBUGF(x...) fprintf(stderr, x);
#else
	//! \def DEBUG(x)
	//! puts out a string if LIBODF_DEBUG is set
	#define DEBUG(x) /* fputs(x, stderr); */
	//! \def DEBUGF(x)
	//! puts out a formatted string if LIBODF_DEBUG is set
	#define DEBUGF(x...) /* fprintf(stderr, x); */
#endif

} // namespace odf

#endif // ODF_DEFINES_H

