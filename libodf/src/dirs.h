/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file dirs.h
	\brief File that contains classes/functions to handle directories
*/

#ifndef DIRS_H
#define DIRS_H DIRS_H

#include <list>
#include <string>

using namespace std;

namespace odf {

enum FilterSpec
{
	Dirs		= 0x001,
	Files		= 0x002, // Regular files
	Drives		= 0x004,
	SymLinks	= 0x008,
	AllTypes	= 0x00F,
	TypeMask	= 0x00F,
	
	Readable	= 0x010,
	Writable	= 0x020,
	Executable	= 0x040,
	RWEMask		= 0x070,
	AllRWE		= 0x070,
	
	Modified	= 0x080,
	Hidden		= 0x100,
	System		= 0x200,
	NoDotAndDotDot	= 0x400,
	AccessMask	= 0x780,
	AllAccess	= 0x780,
	
	All		= AllTypes | AllRWE | AllAccess,
	DefaultFilter = -1 // -1 = all bits set
};

enum SortSpec
{
	Name		= 0x01,
	Time		= 0x02,
	Size		= 0x04,
	Unsorted	= 0x00,
	SortByMask	= 0x07,
	
	DirsFirst	= 0x08,
	Reversed	= 0x10,
	IgnoreCase	= 0x20,
	LocaleAware	= 0x40, 
	DefaultSort	= -1
};

/**
	stores all files (and directories, which are files, too - on unix) of a directory into a list
	@param fList pointer to a list where the filenames shall be stored, which can be NULL - than nothing is stored
	(this may be useful if you just want to know how many files are in the directory)
	 if fList is not empty, the new content is appended, so the old content will not get lost
	@param dirname the directory's name where you look for files
	@param contains file filter - only files that contain this string are appended to the list - set to NULL if no filter useful
	@param filterSpec currently unused
	@param sortSpec currently unused
	@param prepend_prefix set to false if fList shall only the filenames, without path
	@return number of suiting entries
*/
unsigned int get_dir_entries( list<string> *fList, const char* dirname, const char* contains = NULL, bool prepend_prefix=true, int filterSpec=DefaultFilter, int sortSpec=0 );

} // namespace odf

#endif // DIRS_H

