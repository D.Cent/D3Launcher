/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/*! \file file_funcs.h
    \brief Functions for easy file handling
    
    file_funcs.h provides easy file handling, i.e. it checks whether files are printable,
    provides you with functions that map files (move/remove/insert space in files), reading and writing
    from/into files etc.
*/

#include <cstring>
#include <new>
#include <string> // C++ string
using namespace std;

#include "error_class.h"
#include "t_funcs.h"

#ifndef FILE_FUNCS_H
#define FILE_FUNCS_H FILE_FUNCS_H

namespace odf {

//! \def DIR_SEP
//! directory seperator, depending on your system
#if defined(__LINUX__) || defined(MACINTOSH)
	#define DIR_SEP '/'
#else
	#define DIR_SEP '\\'
#endif

//! returns length of file \a _filename
inline unsigned int length_of_file(const char* _filename) {
	struct stat statbuf;
	t_stat(_filename, &statbuf);
	return statbuf.st_size;
}

//! Checks whether file named @a filename is printable
bool file_is_print(const char* _filename);

/**
	Checks if a specified block (i.e. a part of a file) is printable.
	@param fp file pointer to the file, which marks the beginning of the block
	@param _block_len length of the block
*/
bool block_is_print(FILE* fp, int _block_len);

/**
	Copies a file from @a _src to file @a _dest
	@param _src path to source file
	@param _dest path to destination file
	@param is_bin set to false if the file is a text file (Windows)
	@param overwrite specify whether the file from _dest can be overwritten if it already exists
	@return always true if overwrite is true, otherwise false if file can not be overwritten
*/
bool cp(const char* _src, const char* _dest, bool is_bin=true, bool overwrite=true);

/**
	Copies block of @a length from @a src_fp to @a dest_fp.
	\note @a src_fp and @a dest_fp may not overlap! use block_copy() instead.
	\note @a dest_fp must be opened for writing, otherwise, an exception is thrown.
*/
void file_copy (FILE* src_fp, FILE* dest_fp, int length);

/**
	Moves a block (part of a file) in the file @a move bytes forwards or backwards in the file. The src block and the destination block may overlap.
	@param fp the file pointer that marks the beginning of the block
	@param length Length of the block. Should be greater or equal 0, negative values might 
	@param move how many bytes the block shall be moved (greater than 0 causes forward movement, smaller than 0 backward movement)
        \note After being moved, the file pointer is set to the beginning of where the block is now (so, the file pointer is moved @a move bytes). This will also be done if move or lengt is 0.
	\note @a fp must be opened for writing, otherwise, an exception is thrown.
*/
void block_copy(FILE *fp, int length, int move);

/**
	Removes a block of a file, i.e. the block is cut and the file size will decrease.
	@param fp the file pointer that marks the beginning or the end of the block (depending on @a block_length)
	@param block_length
	if < 0, the fp must be positioned after the marked block (faster)
	if > 0, the fp will be positioned before the marked block (fp pos will be const)
	@param filelength length of the whole file before edit
	\note @a fp must be opened for writing, otherwise, an exception is thrown.
*/
void block_remove(FILE *fp, int block_length, int filelength);

/**
	Inserts a block at the position of fp. The file size will increase.
	@param fp the file pointer that marks where the new block will be inserted
	@param block_length
	if < 0, the fp will be positioned at the end of the new block (faster)
	if > 0, the fp will be positioned at the beginning of the new block (fp pos const)
	@param filelength length of the whole file before edit
	\note @a fp must be opened for writing, otherwise, an exception is thrown.
*/
void block_insert(FILE *fp, int block_length, int filelength);

/**
        Searches for the first occurence of a string in a file and moves the file pointer behind it.
	Works on non-seekable strings.
        @param fp the file pointer to the file containing the string
        @param comp_str string that shall be found in the file. must not be NULL!
        @return false if no occurence was found before EOF, otherwise true
*/
bool seek_after_str(FILE* fp, const char* comp_str);

// DEPRECATED
// bool seek_after_str(iostream& seek_stream, const char* comp_str);

/**
        Searches for the first occurence of a string in a file and moves the file pointer behind it.
	Details see seek_after_str().
        \note Does not work on non-seekable streams.
*/
bool seek_to_str(FILE* fp, const char* comp_str);

// DEPRECATED
// bool seek_to_str(iostream& seek_stream, const char* comp_str);

//! Checks if directory named @a path exists and if it is a directory
bool checkdir ( const char *path ) throw();

/**
	Reads line to str (i.e. until \\n). str will be allocated with new[]. delete[] is necessary after use.
	@param fp must be a seekable stream!
	@param str pointer that will point to allocated space
	@param trailer char which marks the end of a line, usually a newline 
	@param maxsize is the maximum length of the string, inluding \\0 (set it to 0 to specify no max size)
	@return true on success, otherwise (if the line ends with EOF) false
*/
bool read_line_seek(FILE* fp, char*& str, char trailer='\n', unsigned int maxsize = 0); 

/**
	This function should not be used.
	\note @a fp must be opened for writing, otherwise, an exception is thrown.
*/
void dump_file(FILE* src_fp, FILE* dest_fp, unsigned int maxlen = 0);

} // namespace odf

#endif // FILE_FUNCS_H

