/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "paths.h"

namespace odf {

void append_dir_sep( string& dirstring ) throw()
{
	string::iterator itr = dirstring.end();
	if(*(itr-1)!=DIR_SEP)
	 dirstring.insert(itr, 1, DIR_SEP);
}

const char* get_homepath( void )
{
#ifdef __LINUX__
	const char* ptr = getenv(HOME_VAR);
	if(ptr == NULL)
	 throw NEW_ERROR("Enviroment variable for " HOME_VAR " not found!",EC_STOP_EXECUTION);
	else
	 return ptr;
#else
	return ".";
#endif
}

string Path::operator+( const char* appendix ) const throw()
{
	string temp(pathname);
	append_dir_sep(temp);
	temp.append(appendix);
	return temp;
}

const string& Path::operator+=( const char* appendix ) throw()
{
	append_dir_sep(pathname);
	pathname.append(appendix);
	return pathname;
}

} // namespace odf
