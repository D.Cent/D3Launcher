/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cctype>

using namespace std;

namespace odf {

#ifdef __WIN32__

	#include <cstdlib>
	#include <unistd.h>
	#include <sys/time.h>
	
	#ifdef __WIN32__
		#include <winsock2.h>
	#endif
	
	unsigned int sleep(unsigned int secs)
	{
		struct timeval tv;
		tv.tv_sec=secs;
		tv.tv_usec=0;
		select(0,NULL,NULL,NULL,&tv);
		return 0;
	}

#endif

#ifndef __WIN32__
	#include <unistd.h>
#endif

bool msleep(unsigned short msecs)
{
#ifdef __WIN32__
	if(msecs >= 1000)
	 return false;
	else {
		Sleep(msecs);
		return true;
	}
#else
	return (usleep(1000*msecs) == 0);
#endif
}

} // namespace odf
