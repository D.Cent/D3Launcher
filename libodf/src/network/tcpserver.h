/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file server.h
	\brief File containing the Server class.
	
	This file contains the Server class, which is used to set up a server.
*/

#ifndef TCPSERVER_H
#define TCPSERVER_H TCPSERVER_H

#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>

#ifdef __WIN32__
//#include <ws2tcpip.h>
 #include "../os_fix.h"
#endif

#include "server.h"

#include <map>
using namespace std;

namespace odf {

// important for the running procedure:
#define STATUS_PAUSE	0	// server is up, but currently not running
#define STATUS_RUNNING	1	// server is running (run_procedure() is executed)
#define STATUS_RESTART	2	// server is asked to restart the run procedure
#define STATUS_SHUTDOWN	4	// whole server is asked to shutdown

typedef map<__SOCKET,struct sockaddr_in> clientmap;

//#define foreach_client_as(_itr_) ()

/*
TODO:
 -MSG_PEEK !
*/

/**
	\class Server
	\brief The Server class is a class, which is used to set up a server.
	
	The server class provides easy server handling. The constructor sets up everything you need to start and the
	desctructor cleans up everything.
	
	You can run the server in two modes:
	 - Your program code decides when the client shall receive or send something and calls those functions itself
	   (not suggested).
	\code
		MyClient.recv(buf, len);
        	if(strcmp(buf,"hi")==0)
        	 MyClient.send("hi", 3);
        	else
        	 MyClient.send("...",4);
	\endcode
	 - Your inherited server has virtual functions (beginnining with on_) which contain information
	   about how to handle data.
	\code
		class MyServer : public Server
		{ public:
			MyServer(int _port):Server(_port) {}
			// This function will be called whenever a client sends a message to the server
			virtual void on_recv(unsigned int sock_id, void* buf, size_t length)
			{
				printf("MyServer: on_recv: %s\n",(const char*)buf); // print out
				send_client(client_num,"answering",10); // send the client an answer
				if(strcmp((const char*)buf,"quitserver")==0)
				 shutdown(); // shut down the server, quit the run function (see above))
			}
		};
	\endcode
	  Such interactions may be started with the run() function, later.

	We suggest you to inherit from the Server class to set up your individual server.
*/
class TCPServer : public Server
{
	
	/*
		DATA
	*/
	
	protected:
		char status; //!< current status of server, see STATUS flags
		clientmap ClientInfo; //!< Map of all clients that are connected to the server.
	
	private:
		pthread_t accept_p, run_p;
		fd_set rfds;
		/*unsigned*/ int highest_fd;
	
	/*
		BASIC FUNCTIONS
	*/
	
	public:
		/**
			First, the constructor does all initialization stuff from the Basis class.
			Then, the server itself will be initialized, i.e. all members will be reset,
			the socket will be bound and listen() will be called. Additionally, a thread
			will be started that accepts the clients (see accept_clients())
			@param port the port to be opened
			@param _RCVBUFSIZE maximum size for recv()'s callbacks
		*/
		TCPServer(int port, unsigned int _RCVBUFSIZE=1024);
		
		//! The desctructor shuts down the server. See also: shutdown().
		virtual ~TCPServer() { shutdown(); }
		
		/**
			returns pointer to clients IP
			\note The pointer is valid as long as the connection
		*/
		inline const char* get_client_ip(__SOCKET sock_id)  throw() {
			return inet_ntoa(ClientInfo[sock_id].sin_addr);
		}
		
		/**
			\brief This function shuts down the server.
			
			You can call it to stop it.
			The server will be stopped from accepting (accept_clients() will be terminated)
			If the server is running, the run() function will return.
			All memory will be cleaned up;
		*/
		void shutdown(void);
		
		/**
			\brief run the client
				
			This function calls the run_procedure() function continous. Your programme
			will not return from calling run() until the shutdown() function was called.
			Your code will recv() messages, and afterwards, on_recv() may send messages.
			\warning do not tell on_recv() to send something AND on_send() to recv something!
		*/
		void run(void);
		
	/*
		RECEIVE/SEND FUNCTIONS
	*/
		
		/**
			This function can be called to receive a message from any client,
			which will be stored in a buffer with a length of RCVBUFSIZE bytes (See basis.h).
			For every client that sent something, on_recv() will be called.
			@return false if no clients are connected (ClientInfo is empty), otherwise true.
		*/
		virtual bool recv_any(void);

		/**
			\brief This function can be called to receive a message from a specified client.
			
			After receiving, on_recv() will be called. See Basis::recv() for details on parameters.
			@return False if \a sock_id was not found in Server::ClientInfo or if recv() failed,
			otherwise false.
			\todo next 3 return values
		*/
		//bool recv_client(__SOCKET sock_id, void * buf, size_t length, int flags=0); // TODO: flags shall be default, so client_num to the first
		
		/**
			\brief Sends a message to all clients.
			For every client on_send() will be called after sending the message.
			\note The parameters are similar to the last 3 of the Basis::send() function's parameters.
			@return False if send() failed.
			otherwise false.
		*/
		//bool send_all(const void * buf, size_t length, int flags=0);
		
		/**
			\brief Sends a message to a specified client.
			
			After sending the message, on_send() will be called.
			See Basis::send() for details on parameters.
			@return False if \a sock_id was not found in Server::ClientInfo or if send() failed,
			otherwise false.

		*/
		bool send_client(unsigned int sock_id, const void * buf, size_t length, int flags=0);
		
	/*
		SLOTS
	*/
		
		/**
			\brief This slot function will be called after a client connected.
			
			\note Change this function in inherited classes to change the server's behaviour.
			You can get the current number if clients calling ClientInfo.size().
		*/
		virtual void on_client_connected(unsigned int sock_id) {
#ifndef LIBODF_DEBUG
			(void)sock_id;
#endif
			DEBUGF(	"on_client_connected: Client with address %s (sock fd %d) connected.\n",
				get_client_ip(sock_id),sock_id);
		}
		
		/**
			\brief This slot function will be called after a client has disconnected from the server.
			
			\note Change this function in inherited classes to change the server's behaviour.
			You can get the current number if clients calling ClientInfo.size().
		*/
		virtual void on_client_disconnected(unsigned int sock_id) {
#ifndef LIBODF_DEBUG
			                        (void)sock_id;
#endif
			DEBUGF( "on_client_disconnected: Client with address %s (sock fd %d) disconnected.\n"
				,get_client_ip(sock_id), sock_id);
		}
		
		/**
			\brief This slot function will be called after the server has recveived a message by a client
			
			@param sock_id The fd of the socket from which the server has received the message.
			@param buf Points to the buffer containing the received message.
			@param length The received lenght. NOT the length in bytes of the buffer pointed to by the \a buf argument.
			\note Change this function in inherited classes to change the server's behaviour.
		*/
		virtual void on_recv(unsigned int sock_id, void* buf, size_t length) { // TODO: const?
			(void) buf;
#ifndef LIBODF_DEBUG
			(void)sock_id; (void)length;
#endif
			DEBUGF("on_recv: I have received a message of %d bytes by client with fd %d.\n", length, sock_id);
		}
		
		/**
			\brief This slot function will be called after the server has sent a message to a client.
			
			@param sock_id The fd of the socket where the server has sent the message to.
			@param buf Points to the buffer containing the sent message.
			@param length The length in bytes of the buffer pointed to by the \a buf argument.
			\note Change this function in inherited classes to change the server's behaviour.
		*/
		virtual void on_send(unsigned int sock_id, const void* buf, size_t length) {
			(void) buf;
#ifndef LIBODF_DEBUG
			(void)sock_id; (void)length;
#endif
			DEBUGF("on_send: I have sent a message of %d bytes to client with fd %d.\n", length, sock_id);
		}
	
	/*
		PROCEDURES, CALLED BY PTHREADS
	*/
	
	protected:
		/**
			\brief This slot function is continous called by the server once the run() function is started (called by call_run_procedure)
			
			\note Change this function in inherited classes to change the server's behaviour.
			Only change this function if you know what you are doing. If you just want to set up a simple
			"Client sends -> Server responds" server, you do not need to change this function.
		*/
		virtual void* run_procedure(void*);
		
		//! function that contains a loop that accepts new clients (called by call_ac)
		void* accept_clients(void*); // TODO! -> ??
	
	/*
		PRIVATE FUNCTIONS
	*/
	
	private:
		//! Fills the FD SET again with all fds from the ClientInfo map struct
		void restore_rfds();
		
		//! Static function to call client accept procedure (pthread needs static functions)
		static void* call_ac(void *S){ ((TCPServer*)S)->accept_clients(NULL); return NULL; }
		
		//! Static function to call running procedure (pthread needs static functions)
		static void* call_run_procedure(void *S){ ((TCPServer*)S)->run_procedure(NULL); return NULL; }

};

} // namespace odf

#endif // TCPSERVER_H

