/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "tcpclient.h"

using namespace std;

namespace odf {

void* connect_thread(void* ptr)
{
//	TODO
	return NULL;
}

TCPClient::TCPClient( const char *IP, int port, unsigned char timeout,
	unsigned int _RCVBUFSIZE) :
	Client(IP, port, eTCP, timeout, _RCVBUFSIZE)
	// only initialize port and IP, no connection for UDP!
{
	/* Connect to server */
	
	/*	if(timeout)
	{
	pthread_t connect_thread;
	pthread_create(&connect_thread, NULL, , NULL);
	pthread_kill(connect_thread,SIGINT);
}
	*/	//else
	
	if(connect(sock,(struct sockaddr*)&sock_addr,sizeof(sock_addr)) <0)
	 throw NEW_ERROR("Cannot connect to server");
}

} // namespace odf
