/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file server.h
	\brief File containing the Server class.
	
	This file contains the Server class, which is used to set up a server.
*/

#ifndef UDPSERVER_H
#define UDPSERVER_H UDPSERVER_H

#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <pthread.h>

#ifdef __WIN32__
//#include <ws2tcpip.h>
 #include "../os_fix.h"
#endif

#include "server.h"

//#define foreach_client_as(_itr_) ()

/*
TODO:
 -MSG_PEEK !
*/

namespace odf {

/**
	\class Server
	\brief The Server class is a class, which is used to set up a server.
	
	The server class provides easy server handling. The constructor sets up everything you need to start and the
	desctructor cleans up everything.
	
	You can run the server in two modes:
	 - Your program code decides when the client shall receive or send something and calls those functions itself
	   (not suggested).
	\code
		MyClient.recv(buf, len);
        	if(strcmp(buf,"hi")==0)
        	 MyClient.send("hi", 3);
        	else
        	 MyClient.send("...",4);
	\endcode
	 - Your inherited server has virtual functions (beginnining with on_) which contain information
	   about how to handle data.
	\code
		class MyServer : public Server
		{ public:
			MyServer(int _port):Server(_port) {}
			// This function will be called whenever a client sends a message to the server
			virtual void on_recv(unsigned int sock_id, void* buf, size_t length)
			{
				printf("MyServer: on_recv: %s\n",(const char*)buf); // print out
				send_client(client_num,"answering",10); // send the client an answer
				if(strcmp((const char*)buf,"quitserver")==0)
				 shutdown(); // shut down the server, quit the run function (see above))
			}
		};
	\endcode
	  Such interactions may be started with the run() function, later.

	We suggest you to inherit from the Server class to set up your individual server.
*/
class UDPServer : public Server
{
	protected:
		bool quit_udp_srv;
	
	/*
		BASIC FUNCTIONS
	*/
	
	public:
		/**
			First, the constructor does all initialization stuff from the Basis class.
			Then, the server itself will be initialized, i.e. all members will be reset,
			the socket will be bound and listen() will be called. Additionally, a thread
			will be started that accepts the clients (see accept_clients())
			@param port the port to be opened
			@param _RCVBUFSIZE maximum size for recv()'s callbacks
		*/
		UDPServer(int port, unsigned int _RCVBUFSIZE = 1024);
		
		//! The desctructor shuts down the server. See also: shutdown().
		virtual ~UDPServer() { shutdown(); }
		
		/**
			\brief run the client
				
			This function calls the run_procedure() function continous. Your programme
			will not return from calling run() until the shutdown() function was called.
			Your code will recv() messages, and afterwards, on_recv() may send messages.
			\warning do not tell on_recv() to send something AND on_send() to recv something!
		*/
		virtual void run(void);
		
		/**
			\brief This function shuts down the server.
			
			You can call it to stop it.
			The server will be stopped from accepting (accept_clients() will be terminated)
			If the server is running, the run() function will return.
			All memory will be cleaned up;
		 */
		virtual void shutdown(void);
		
	/*
		RECEIVE/SEND FUNCTIONS
	*/
		
		/**
			This function can be called to receive a message from any client,
			which will be stored in a buffer with a length of RCVBUFSIZE bytes (See basis.h).
			For every client that sent something, on_recv() will be called.
			@return false if no clients are connected (ClientInfo is empty), otherwise true.
		*/
		virtual bool recv_any(void);

		/**
			\brief This function can be called to receive a message from a specified client.
			
			After receiving, on_recv() will be called. See Basis::recv() for details on parameters.
			@return False if \a sock_id was not found in Server::ClientInfo or if recv() failed,
			otherwise false.
			\todo next 3 return values
		*/
		inline int recv_client(__SOCKET sock_id, void * buf, size_t length, int flags, struct sockaddr *from, socklen_t* fromlen)
		{
			int ret_val = Basis::arecvfrom(sock_id, buf, length, flags, from, fromlen);
			on_recv(sock_id, buf, (size_t)ret_val, from, fromlen);
			return ret_val;
		}
		
		// TODO: flags shall be default, so client_num to the first
		
		/**
			\brief Sends a message to a specified client.
			
			After sending, on_send() will be called after sending the message.
			See Basis::send() for details on parameters.
			@return False if \a sock_id was not found in Server::ClientInfo or if send() failed,
			otherwise false.

		*/
		inline bool send_client(unsigned int sock_id, const void * buf, size_t length, int flags, const struct sockaddr *to, socklen_t tolen) {
			bool ret_val = Basis::asendto(sock_id, buf, length, flags, to, tolen);
			on_send(sock_id, buf, length, to, tolen);
			return ret_val;
		}
		
	/*
		SLOTS
	*/
		
		/**
			\brief This slot function will be called after the server has recveived a message by a client
			
			@param sock_id The fd of the socket from which the server has received the message.
			@param buf Points to the buffer containing the received message.
			@param length The received length. NOT the length in bytes of the buffer pointed to by the \a buf argument.
			@param from Points to the sending address from recvfrom() calls.
			@param fromlen Specifies the length of the sockaddr structure pointed to by \a from.
			\note Change this function in inherited classes to change the server's behaviour.
		*/
		virtual void on_recv(unsigned int sock_id, void* buf, size_t length, struct sockaddr *from, socklen_t* fromlen) { // TODO: const?
			(void) buf; (void) from;
#ifndef LIBODF_DEBUG
			(void)sock_id; (void)length; (void)from; (void)fromlen;
#endif
			DEBUGF("on_recv: I have received a message of %d bytes by client with fd %d.\n", length, sock_id);
		}
		
		/**
			\brief This slot function will be called after the server has sent a message to a client.
			
			@param sock_id The fd of the socket where the server has sent the message to.
			@param buf Points to the buffer containing the sent message.
			@param length The length in bytes of the buffer pointed to by the \a buf argument.
			@param to Points to tge destination address from sendto() calls.
			@param tolen Specifies the length of the sockaddr structure pointed to by \a to.
			\note Change this function in inherited classes to change the server's behaviour.
		*/
		virtual void on_send(unsigned int sock_id, const void* buf, size_t length, const struct sockaddr *to, socklen_t tolen) {
			(void) buf; (void)to;
#ifndef LIBODF_DEBUG
			(void)sock_id; (void)length; (void)to; (void)tolen;
#endif
			DEBUGF("on_send: I have sent a message of %d bytes to client with fd %d.\n", length, sock_id);
		}

};

} // namespace odf

#endif // UDPSERVER_H

