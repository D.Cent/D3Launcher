/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "server.h"

#ifdef __WIN32__
	#include "../os_fix.h"
	#include <ws2tcpip.h>
#endif

namespace odf {

Server::Server(int port, eproto use_proto, unsigned int _RCVBUFSIZE)
	: Socket(port, use_proto, _RCVBUFSIZE)
{
	
	// INADDR_ANY accepts every IP
	sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	if( bind( sock,(struct sockaddr*)&sock_addr, sizeof( sock_addr ) ) < 0 )
	 throw NEW_ERROR("Cannot bind socket");
	
}

} // namespace odf
