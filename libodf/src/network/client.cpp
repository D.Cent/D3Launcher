/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "client.h"

using namespace std;

namespace odf {

// initialize socket...

Client::Client(const char *_IP, int port, eproto use_proto,
	unsigned char timeout, unsigned int _RCVBUFSIZE)
	: Socket(port, use_proto, timeout, _RCVBUFSIZE), IP(_IP)
{
	struct hostent *host_info;
	unsigned long addr;

	if ((addr = inet_addr( (const char*)IP.c_str() )) != INADDR_NONE) {
		memcpy( (char *)&sock_addr.sin_addr, &addr, sizeof(addr));
	}
	else
	{
	
		/* If needed, convert address to ip (e.g. localhost to 127.0.0.1) */
		host_info = gethostbyname((const char*)IP.c_str() );
		if (host_info == NULL)
		 throw NEW_ERROR("Unknown server");
		/* socket ip-address */
		memcpy( (char *)&sock_addr.sin_addr,
                host_info->h_addr, host_info->h_length );
	}
}

}
