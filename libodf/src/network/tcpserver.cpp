/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "tcpserver.h"

#ifdef __WIN32__
	#include "../os_fix.h"
	#include <ws2tcpip.h>
#endif

namespace odf{

TCPServer::TCPServer(int port, unsigned int _RCVBUFSIZE) :
	Server(port, eTCP, _RCVBUFSIZE)
{
	//num_clients=0;
	highest_fd=0;
	FD_ZERO(&rfds);
	status = STATUS_PAUSE;
	
	signal(SIGINT, SIG_IGN);
	 
	if( listen(sock, 5) == -1 )
	 throw NEW_ERROR("Could not listen to socket");

#ifdef LIBODF_DEBUG
	printf("Server is running :-) - waiting for clients...\n");
#endif	
	pthread_create(&accept_p, NULL, this->call_ac, this);
}

void TCPServer::shutdown(void)
{
	pthread_kill(accept_p,SIGINT);
	
	status = STATUS_SHUTDOWN; // tell run() not to restart, but to shutdown!
	pthread_kill(run_p,SIGINT);

	for(clientmap::const_iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); itr++)
		close(itr->first);

	FD_ZERO(&rfds);
}

bool TCPServer::send_client(unsigned int sock_id, const void * buf, size_t length, int flags)
{
	bool ret_val = true;
	if( ClientInfo.find(sock_id) == ClientInfo.end() ) // TODO: throw?
	 return false;
#ifdef LIBODF_DEBUG	
	printf("Sending message to client with sock %d\n",sock_id);
#endif
	ret_val = ( Basis::asend(sock_id, buf, length, flags) == (signed int)length );
	if(ret_val)
	 on_send(sock_id, buf, length);		// danger -> slope! TODO!!
	return ret_val;
}

bool TCPServer::recv_any(void)
{
	
	//struct timeval tv; // time interval for select
	//tv.tv_sec = 1;
	//tv.tv_usec = 0;
	
	int answers;
	
	if(ClientInfo.empty()) {
#ifdef LIBODF_DEBUG	
		puts("No new clients, waiting 1 second.");
#endif
		sleep(1); // let CPU chill
		return false;
	}

	restore_rfds();

#ifdef LIBODF_DEBUG
	for(clientmap::iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); itr++)
		printf("before select: fd %d set...\n",itr->first);
#endif

	answers = select(highest_fd+1,&rfds,NULL,NULL,NULL);
	if(answers==-1)
		throw NEW_ERROR("select() failed");

#ifdef LIBODF_DEBUG
	for(clientmap::iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); itr++)
		printf("after select: fd %d set...\n",itr->first);
	printf("we have %d answers!\n",answers);
#endif
		
	if(answers)
	{
		for(clientmap::iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); )
		{
			if( FD_ISSET(itr->first,&rfds) ) // check if any client in ci IS SET in the FD SET
			{
				char echo_buffer[RCVBUFSIZE];
				ssize_t recv_size;
				
				recv_size = Basis::arecv(itr->first, echo_buffer, RCVBUFSIZE,0);
				
				if( recv_size == -1 ) // error
					throw NEW_ERROR("recv() failed at recv_any()",EC_CONTINUE); // TODO - really throw here? - if not : itr++
				else if( recv_size == 0 ) // client disconnected
				{
					__SOCKET old_fd = itr->first;
					if(itr->first == highest_fd)
						highest_fd=0;
					FD_CLR(itr->first,&rfds);
					on_client_disconnected(old_fd);
					close(itr->first);
					{ // erase element
						clientmap::iterator next = itr;
						next++;
						ClientInfo.erase(itr);
						itr=next;
					}
					if(highest_fd==0)
						for(clientmap::iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); itr++)
							if(itr->first>highest_fd)
								highest_fd=itr->first;
				}
				else // receive message
				{
#ifdef LIBODF_DEBUG
					//printf("A new message was received by client with sock %d:\n",itr->first);
					//puts(echo_buffer);
#endif
					on_recv(itr->first, echo_buffer, recv_size);
					itr++;
				}
			} // if( FD_ISSET ...)
		} // for
	} // if(answers)
	return true;
}

void TCPServer::run(void)
{
	while(1)
	{
		status = STATUS_RUNNING;
		pthread_create(&run_p, NULL, this->call_run_procedure, this);
		pthread_join(run_p, NULL);
		
		if( status == STATUS_SHUTDOWN ) {
#ifdef LIBODF_DEBUG
			puts("Status shutdown!");
#endif
			break;
		}
		else if( status == STATUS_RUNNING ) {
			puts("STILL RUNNNING... STRANGE!\nTELL KING LO OF THIS!!");
		}
	}

}

inline void p_sig_handler(int) {
	pthread_exit(NULL);
}

void* TCPServer::run_procedure(void*)
{
/*	struct sigaction sig_struct; // TODO: -> run ?

	//sig_struct.sa_handler = sig_bearbeiter;
	sig_struct.sa_handler = p_sig_handler;
	sigemptyset(&sig_struct.sa_mask);
	sig_struct.sa_flags = 0;

	if (sigaction(SIGINT,&sig_struct,NULL) != 0) { */
	if (signal(SIGINT, p_sig_handler)==SIG_ERR)
	 throw NEW_ERROR("Error setting up signal routine for server");
	
	recv_any();
		
	status = STATUS_PAUSE;
	return NULL;
}

void* TCPServer::accept_clients(void*)
{
/*	struct sigaction sig_struct;

	sig_struct.sa_handler = p_sig_handler;
	sigemptyset(&sig_struct.sa_mask);
	sig_struct.sa_flags = 0;

	if (sigaction(SIGINT,&sig_struct,NULL) != 0) {*/
	if (signal(SIGINT, p_sig_handler)==SIG_ERR)
	 throw NEW_ERROR("Error setting up signal routine for server");
	
	while(1)
	{
	
		int len;
		struct sockaddr_in new_sockaddr;
		
		__SOCKET new_fd;
		
		len=sizeof(struct sockaddr_in);
		new_fd = accept(sock, (struct sockaddr*)&(new_sockaddr), (socklen_t*)&len);
		
		if( new_fd < 0)
		 return NULL;
		
		if(proto == eUDP)
		 if( connect(sock, (struct sockaddr*)&new_sockaddr, len) != 0 )
		  puts("WARNING: Could not connect() to client (UDP)");
		
		ClientInfo.insert(pair<__SOCKET, struct sockaddr_in>(new_fd, new_sockaddr));
		if(new_fd>highest_fd)
		 highest_fd=new_fd;
		
		//printf("Client with address %s (sock id %d) connected\n", inet_ntoa(new_sockaddr.sin_addr), new_fd);
		//printf("highest fd is now %d\n",highest_fd);
		on_client_connected(new_fd);
		
		status = STATUS_RESTART;
		pthread_kill(run_p,SIGINT); // restart run thread to get to know all current clients
		//restore_rfds();
	}
	return NULL;
}

void TCPServer::restore_rfds()
{
	for(clientmap::const_iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); itr++)
	{
		if( ! FD_ISSET(itr->first,&rfds))
		 FD_SET(itr->first,&rfds);
	}
}

} // namespace odf
