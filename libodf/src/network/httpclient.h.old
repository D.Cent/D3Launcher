/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007, 2008                                              */
/* Johannes Lorenz                                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file httpclient.h
	\brief File containing the HTTP client
	
	This file provides you with the HTTPClient class.
*/

#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H HTTPCLIENT_H

#include "tcpclient.h"

#define RECV_BUFFER (1024*1024)

/**
	\class HTTPClient 
	\brief Client Class for HTTP Protocoll
	
	This class provides you with a Client for the HTTP protocoll. The constructor HTTPClient() will do everything for starting
	(i.e. setting up a socket and opening it), so afterwards you can simply call the famous HTTP commands (like, e.g. GET()).
	The desctructor  ~HTTPClient() will clean up everything. Hence, downloading a file can be easily done with two lines ;) ...
*/
class HTTPClient : private TCPClient { // private inheritance so user cannot omit HTTP proto

	public:
		//! Sets up a HTTP Client.
		//! @param IP The HTTP-server's IP you want to connect to.
		HTTPClient(const char IP[]) : TCPClient(IP,80) {}; // use default port 80
		
		//! Stops the HTTP Client. All cleaning up is done by the destructor.
		virtual ~HTTPClient() {}
		
		/**
			This is the famous HTTP GET command. You may handle it similar to the real HTTP GET.
			@param _filename Name of the file you want to download. The file will be saved on your system with the same name.
			@param _savedir Directory where to store the downloaded file. If none given, current directory is chosen.
			@param _version C-String for version of http protocol
		*/
		bool GET(const char* _filename, const char* savename = NULL, const char* _savedir = ".", const char* _version = "HTTP/1.1");

#if 0		
		inline bool TRACE(const char* _data, const char* _version = "HTTP/1.1") {  //TODO!
		
		char buffer[10];
		
		//send("TRACE hello HTTP/1.1\n\n", 23);
		send("TRACE HTTP/1.1 hello\n\n", 23);
		recv(buffer, 5); 
		buffer[5]='\0';
		
		printf("TRACE returned %s\n",buffer);
		return false;	
		} 
#endif
		
	private:
		
		static void* wait_for_timeout(void*);
		
		char GETstr[128];
        	char HOSTstr[128];
};

#include <cstring>
#include <string>
#include <iostream>
#include <unistd.h>
using namespace std;

// http://hans:geheim@www.example.org:80/demo/example.cgi?land=de&stadt=aa#abschnitt1

/**
        \class tURL
        \brief Class for an URL (Uniform Resource Locator)

        This class is made for storing URLs and getting parts of it. I.e. You may store a full URL here and then ask the class for
	e.g. only the protocol name or only the host name.
*/

class tURL {
	private:
		const char* fullURL; // TODO: pointer -> allocating?
		//! local function to return a string from position @a _from to @a _to,
		//! whereas the string begins at @a _from and ends BEFORE @a _from
		//! e.g., in 'http://xxx', let the _to pointer point to the :// to get 'http'
		string str_from_to(const char* _from, const char* _to)
		{
			unsigned int len=0;
			const char* ptr = _from;
			for(;ptr!=_to;ptr++,len++) ; // len gets the length of the substring
			//cout << "len= " << len << endl;
			string return_str(_from, len);
			return return_str;
		}
		
	public:
		//! This constructor informs the URL class about the URL you want to read from
		tURL( const char* URLname ):fullURL(URLname) {};
		//! Returns the protocol name without '://' (e.g. 'http' or 'ftp')
		string proto( void ) { return str_from_to(fullURL,strstr(fullURL,"://")); }
		//! Returns the hostname (which is, e.g., 'www.google.com')
		string host ( void ) {
			string s;
			if(strchr(fullURL,'@'))
			 s = str_from_to(strchr(fullURL,'@')+1, strchr ( strstr(fullURL,"://") + 3 , '/' ) ); // TODO (min())
			else
			 s = str_from_to(strstr(fullURL,"://")+3, strchr ( strstr(fullURL,"://") + 3 , '/' ) );
			unsigned int pos = s.find_last_of(':');
			if(pos != s.npos)
			 s.resize(pos); // cut at ':', because that is the port
			return s;
			
		}
		
		//! Returns a file's path on the server (e.g. in odf.sf.net/images/odf_2_small.png, it returns '/images/odf_2_small.png')
		inline string path ( void ) {
			return str_from_to( strchr ( strstr(fullURL,"://") + 3 , '/' ), fullURL+strlen(fullURL));
		}
		
		//! Returns a file's filename on the server (e.g. in odf.sf.net/images/odf_2_small.png, it returns 'odf_2_small.png')
		inline string filename (void) {
			return str_from_to( strrchr ( strstr(fullURL,"://") + 3 , '/' ) + 1, fullURL+strlen(fullURL));
		}
		
		//string user( void ) { return s(fullURL,fullURL,"://"); }


};

/**
	downloads a file from an URL (like http://www.google.com/index.html)
	@param url_name is a pointer to the c-string which contains the URL including the file on the server you want to download
	@param save_path path where to store the downloaded file
	@return true on success, false otherwise
	\todo Currently, only http download is supported
*/
inline bool download_url(const char* url_name, const char* save_path=".") { // TODO! ... full url support!
	
	tURL u(url_name);

#if 0	
	cout << "proto: " << u.proto() << endl;
	cout << "host: " << u.host() << endl; // TODO
	cout << "path: " << u.path() << endl;
#endif
	
	if( u.proto() == "http" ) {
		string host = u.host();
		HTTPClient Cl( host.c_str() );
		return Cl.GET(u.path().c_str(), save_path);
	}
	else
	 return false;
}


#endif // HTTPCLIENT_H

