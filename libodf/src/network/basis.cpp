/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <sys/select.h>

#include "basis.h"

namespace odf {

struct timeval Basis::tmp_tv;

ssize_t Basis::recv_timeouted(__SOCKET _sock, void * buf, size_t length, int flags, int timeout_sec, int timeout_usec)
{
	ssize_t ret_val = 0;
	tmp_tv.tv_sec = timeout_sec;
	tmp_tv.tv_usec = timeout_usec;
	fd_set rfds;
	FD_ZERO(&rfds);
	FD_SET(_sock,&rfds);
	if( select(_sock+1,&rfds,NULL,NULL,&tmp_tv) > 0)
#ifdef __WIN32__
	 ret_val = recv(_sock, (char*)buf, length, flags);
#else
	 ret_val = recv(_sock, buf, length, flags);
#endif
	FD_ZERO(&rfds);
	return ret_val;
}

ssize_t Basis::recvfrom_timeouted(__SOCKET _sock, void * buf, size_t length, int flags, struct sockaddr *from, socklen_t *fromlen, int timeout_sec, int timeout_usec)
{
	ssize_t ret_val = 0;
	tmp_tv.tv_sec = timeout_sec;
	tmp_tv.tv_usec = timeout_usec;
	fd_set rfds;
	FD_ZERO(&rfds);
	FD_SET(_sock,&rfds);
	if( select(_sock+1,&rfds,NULL,NULL,&tmp_tv) > 0)
#ifdef __WIN32__
	 ret_val = recvfrom(_sock, (char*)buf, length, flags, from, fromlen);
#else
	 ret_val = recvfrom(_sock, buf, length, flags, from, fromlen);
#endif

	printf("length recv(): %d\n", (int)ret_val);

	FD_ZERO(&rfds);
	return ret_val;
}

bool Basis::send_timeouted(__SOCKET _sock, const void * buf, size_t length, int flags, int timeout_sec, int timeout_usec)
{
	bool ret_val = false;
	tmp_tv.tv_sec = timeout_sec;
	tmp_tv.tv_usec = timeout_usec;
	fd_set wfds;
	FD_ZERO(&wfds);
	FD_SET(_sock,&wfds);
	if( select(_sock+1,NULL,&wfds,NULL,&tmp_tv) > 0)
#ifdef __WIN32__
	 ret_val = send(_sock, (char*)buf, length, flags);
#else
	 ret_val = send(_sock, buf, length, flags);
#endif
	FD_ZERO(&wfds);
	return ret_val;
}

bool Basis::sendto_timeouted(__SOCKET _sock, const void * buf, size_t length, int flags, const struct sockaddr *to, socklen_t tolen, int timeout_sec, int timeout_usec)
{
	bool ret_val = false;
	tmp_tv.tv_sec = timeout_sec;
	tmp_tv.tv_usec = timeout_usec;
	fd_set wfds;
	FD_ZERO(&wfds);
	FD_SET(_sock,&wfds);
	if( select(_sock+1,NULL,&wfds,NULL,&tmp_tv) > 0)
#ifdef __WIN32__
	 ret_val = sendto(_sock, (char*)buf, length, flags, to, tolen);
#else
	 ret_val = sendto(_sock, buf, length, flags, to, tolen);
#endif
	FD_ZERO(&wfds);
	return ret_val;
}

Basis::Basis(bool _use_timeout, unsigned int _RCVBUFSIZE) :
	use_timeout(_use_timeout), RCVBUFSIZE(_RCVBUFSIZE)
{
	/* initialize timeout to default value */
	timeout.tv_sec = 0;
	timeout.tv_usec = 0;
}

} // namespace odf
