/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <signal.h>
#include <fstream>

#include "../t_funcs.h" // for t_fwrite()
#include "../defines.h" // for min
#include "../paths.h" // for GET()'s savepath

#include "httpclient.h"

namespace odf {

void HTTPClient::GetLine(std::stringstream& line)
{
	for(char c; recv(&c, 1, 0) > 0; line << c)
	{
		if(c == '\n')
		 return;
	}

	throw NEW_ERROR("Couldn't receive line!", EC_CONTINUE);
}

bool HTTPClient::GET(const char* _filename, const char* _savename, const char* _savedir, const char* _version)
{
	if(_filename[0]=='/')
	 _filename++;
	
	/*
		Initializing and sending GET and Host strings
	*/
	sprintf(GETstr,"GET /%s %s\n",_filename,_version);
	sprintf(HOSTstr,"Host: %s\n\n",Client::IP.c_str());

#ifdef LIBODF_DEBUG
	printf("GETstring: %s",GETstr);
	printf("HOSTstring %s",HOSTstr);
#endif
	
        if (this->send( GETstr, strlen(GETstr)) == false) return false;
        if (this->send( HOSTstr, strlen(HOSTstr)) == false) return false;
	
	fd_set rfds;
	FD_ZERO(&rfds);
	FD_SET(sock,&rfds);
	int answers = select(sock+1,&rfds,NULL,NULL,&(Basis::timeout));
	FD_ZERO(&rfds);
	
	// answers must be > 0
	
	if( answers <= 0 ) // TODO: throw() for -1 ?
	{
		puts("Error: Server did not respond!\a");
		return false;
	}

	if(strchr(_filename,'/')!=NULL)
	 _filename = strrchr(_filename,'/')+1;

	Path savepath = _savedir;
	if(_savename)
	 savepath += _savename;
	else
	 savepath += _filename; // choose filename on server

	int code = 100; // 100 = Continue
	string used_protocol;
	stringstream firstLine;
	
	while(code == 100)
	{
		GetLine(firstLine);
		firstLine >> used_protocol;
		firstLine >> code;

		if(code == 100)
		 GetLine(firstLine);
	}

	cout << "used protocol: " << used_protocol << endl;

	if(code != 200)
	{
		firstLine.ignore();
		string msg;
		getline(firstLine, msg);
		cout << "Error #" << code << " - " << msg << endl;
		return false;
	}

	bool chunked = false;
	const int noSizeGiven = -1;
	int size = noSizeGiven;

	while(true)
	{
		stringstream sstream;
		GetLine(sstream);
		if(sstream.str() == "\r")
		 break;

		string left;
		sstream >> left;
		sstream.ignore();

		if(left == "Content-Length:")
		 sstream >> size;

		if(left == "Transfer-Encoding:")
		{
			string transferEncoding;
			sstream >> transferEncoding;
			if(transferEncoding == "chunked")
			 chunked = true;
		}
	}

#ifdef LIBODF_DEBUG
	cout << "Filename: " << savepath.c_str() << endl;
#endif

	fstream fout(savepath.c_str(), ios::binary | ios::out);
	
	if(!fout)
	{
		cout << "Could Not Create File!" << endl;
		return false;
	}

	int recvSize = 0;
	char buf[1024];
	int bytesRecv = -1;

	if(size != noSizeGiven)
	{
		cout << "0%";
		
		while(recvSize < size)
		{
			if((bytesRecv = recv(buf, sizeof(buf), 0)) <= 0)
			 throw NEW_ERROR("Error at socket", EC_CONTINUE);
		
			recvSize += bytesRecv;
			fout.write(buf, bytesRecv);
			cout << "\r" << recvSize * 100 / size << "%" << flush;
		}
	}
	else
	{
		if(!chunked)
		{
			cout << "Downloading... (Unknown Filesize)" << endl;
			while(bytesRecv != 0)
			{
				if((bytesRecv = recv(buf, sizeof(buf), 0)) < 0)
				 throw NEW_ERROR("Error at socket", EC_CONTINUE);

				fout.write(buf, bytesRecv);
			}
		}
		else
		{
			cout << "Downloading... (Chunked)" << endl;

			while(true)
			{
				stringstream sstream;
				GetLine(sstream);
				int chunkSize = -1;
				sstream >> hex >> chunkSize;
				if(chunkSize <= 0)
				 break;

				cout << "Downloading Part (" << chunkSize << " Bytes)... " << endl;
				recvSize = 0;

				while(recvSize < chunkSize)
				{
					unsigned int bytesToRecv = chunkSize - recvSize;
					if((bytesRecv = recv(buf, bytesToRecv > sizeof(buf) ? sizeof(buf) : bytesToRecv, 0)) <= 0)
					 throw NEW_ERROR("Error at socket", EC_CONTINUE);

					recvSize += bytesRecv;
					fout.write(buf, bytesRecv);
					cout << "\r" << recvSize * 100 / chunkSize << "%" << flush;
				}
				
				cout << endl;
				
				for(int i = 0; i < 2; ++i)
				{
					char temp;
					recv(&temp, 1, 0);
				}
			}
		}
	}
	
	cout << endl << "Finished!" << endl;

	return true;
}

} // namespace odf
