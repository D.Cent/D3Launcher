/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "odf_socket.h"

namespace odf {

Socket::Socket(int port, eproto use_proto, bool _use_timeout,
		unsigned int _RCVBUFSIZE) :
	Basis(_use_timeout, _RCVBUFSIZE), proto(use_proto)
{
	
#ifdef _WIN32
	/* Initialize Socket for Windohs */
	WORD wVersionRequested;
	WSADATA wsaData;
	wVersionRequested = MAKEWORD (1, 1);
	if (WSAStartup (wVersionRequested, &wsaData) != 0)
	 throw NEW_ERROR("Could not initialize Winsock");
	else
#ifdef LIBODF_DEBUG
	 printf("Winsock succeeded\n");
#endif
#endif
	/* Create socket */
	if(use_proto==eTCP)
	 sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	else if(use_proto==eUDP)
	 sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	else if(use_proto==eICMP)
	 sock = socket(PF_INET, SOCK_RAW , IPPROTO_ICMP); // TODO
	
		
	if (sock < 0)
	 throw NEW_ERROR("Could not create socket",EC_STOP_EXECUTION);

	memset( &sock_addr, 0, sizeof (sock_addr));
	
	/* IPv4-connection */
	sock_addr.sin_family = AF_INET;
	/* portnumber */
	sock_addr.sin_port = htons(port);
	
}

Socket::~Socket() {
#ifdef LIBODF_DEBUG
	puts("Closing socket...");
#endif
#ifdef _WIN32
	closesocket(sock);
	/* Cleanup winsock */
	WSACleanup();
#else
	close(sock);
#endif
}

} // namespace odf
