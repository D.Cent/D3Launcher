/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file basis.h
	\brief File defining network standards and the Basis class
	
	This file contains
	 - a platform independent usable socket type, see __SOCKET
	 - a buffer size for recevie buffers, see RCVBUFSIZE
	 - the Basis Class, a Class which is the top class of all network classes
*/

#ifndef BASIS_H
#define BASIS_H BASIS_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
using namespace std;

#ifdef __WIN32__
/* Headerfiles for Windows */
#include "ws2tcpip.h"
#include <winsock2.h>
#include <io.h>

#else
/* Headerfiles for Unix/Linux */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

#include "../defines.h" // for DEBUG, DEBUGF
#include "../error_class.h" // we will need that in every network class...

namespace odf {

//! \def __SOCKET
//! Socket file descriptor type, depending on your system.
#ifdef __WIN32__
	#define __SOCKET SOCKET
#else
	#define __SOCKET int
#endif

#ifdef __WIN32__
	//! \def MSG_WAITALL
	//! Wait for a full request. Used as a flag for std::recv()
	#define MSG_WAITALL      0x100
#endif

// uncomment this for debugging
//#define LIBODF_DEBUG true

/*
TODO:
throws()
message-function, away from constructor
*/

/**
	\class Basis
	\brief A Class which is the basis class for all network classes.

	The Basis class does tasks that are needed for OS independent networking
	and is inherited by all network classes. It provides functions ... TODO
*/
class Basis
{
	public:	
		/**
			\brief The constructor opens a socket and intializes the socket address.
			@param use_timeout Defines whether timeouts should be used for send()/recv()
				(suggested for blocking IO)
			@param _RCVBUFSIZE maximum size for recv()'s callbacks.
		*/
		Basis(bool _use_timeout=true, unsigned int _RCVBUFSIZE = 1024);
		
		//! destructor added to omit issues with virtual classes
		virtual ~Basis() {}
		
		//! auto send - with timeout if use_timeout is set to true
		inline bool asend(__SOCKET _sock, const void * buf, size_t length, int flags=0)
		{
			return (use_timeout) ? Basis::send_timeouted(_sock, buf, length, flags, timeout.tv_sec, timeout.tv_usec)
			 : Basis::send(_sock, buf, length, flags);
		}
		
		/**
			\brief Sends a message to a specified client.
			
			\note All parameters are similar to your system's send() function's parameters.
			@param _sock The fd of the socket you want to receive from.
			@param buf Points to the buffer containing the message to send.
			@param length Specifies the length of the message in bytes
			@param flags Specifies the type of message transmission.
		*/
		inline static bool send(__SOCKET _sock, const void * buf, size_t length, int flags=0) {
#ifndef __WIN32__
			return ( ::send(_sock, buf, length, flags) == (signed int)length );
#else // WIN32
			return ( ::send(_sock, (char*)buf, length, flags) == (signed int)length );
#endif
		}
		
		static bool send_timeouted(__SOCKET _sock, const void * buf, size_t length, int flags, int timeout_sec, int timeout_usec);
		
		//! auto sendto - with timeout if use_timeout is set to true
		inline bool asendto(__SOCKET _sock, const void * buf, size_t length, int flags, const struct sockaddr *to, socklen_t tolen)
		{
			return (use_timeout) ? Basis::sendto_timeouted(_sock, buf, length, flags, to, tolen, timeout.tv_sec, timeout.tv_usec)
			 : Basis::sendto(_sock, buf, length, flags, to, tolen);
		}
		
		inline static bool sendto(__SOCKET _sock, const void * buf, size_t length, int flags, const struct sockaddr *to, socklen_t tolen) {
#ifndef __WIN32__
			DEBUGF("SENDING TO: %s.\n", inet_ntoa(((sockaddr_in*)to)->sin_addr));
			return ( ::sendto(_sock, buf, length, flags, to, tolen) == (signed int)length );
#else // WIN32
			return ( ::sendto(_sock, (char*)buf, length, flags, to, tolen) == (signed int)length );
#endif
		}
		
		static bool sendto_timeouted(__SOCKET _sock, const void * buf, size_t length, int flags, const struct sockaddr *to, socklen_t tolen, int timeout_sec, int timeout_usec);
		
		//! auto recv - with timeout if use_timeout is set to true
		inline ssize_t arecv(__SOCKET _sock, void * buf, size_t length, int flags=0)
		{
			return (use_timeout) ? Basis::recv_timeouted(_sock, buf, length, flags, timeout.tv_sec, timeout.tv_usec)
			 : Basis::recv(_sock, buf, length, flags);
		}
		
		/**
			\brief This function can be called to receive a message from a specified client.
			
			\note All parameters are similar to your system's recv() function's parameters.
			@param _sock The fd of the socket you want to receive from.
			@param buf Points to a buffer where the message should be stored.
			@param length Specifies the length in bytes of the buffer pointed to by the \a buf argument.
			@param flags Specifies the type of message reception.
		*/

		inline static ssize_t recv(__SOCKET _sock, void * buf, size_t length, int flags=0) {
			//return ( ::recv(_sock, buf, len, flags) == (signed int)len ); // TODO
#ifndef __WIN32__
			return ( ::recv(_sock, buf, length, flags) );
#else			
			return ( ::recv(_sock, (char*)buf, length, flags) );
#endif
                }
		
		static ssize_t recv_timeouted(__SOCKET _sock, void * buf, size_t length, int flags, int timeout_sec, int timeout_usec);
		
		//! auto recv - with timeout if use_timeout is set to true
		inline ssize_t arecvfrom(__SOCKET _sock, void * buf, size_t length, int flags, struct sockaddr *from, socklen_t *fromlen)
		{
			return (use_timeout) ? Basis::recvfrom_timeouted(_sock, buf, length, flags, from, fromlen, timeout.tv_sec, timeout.tv_usec)
			: Basis::recvfrom(_sock, buf, length, flags, from, fromlen);
		}
		
		inline static ssize_t recvfrom(__SOCKET _sock, void * buf, size_t length, int flags, struct sockaddr *from, socklen_t *fromlen) {
			//return ( ::recv(_sock, buf, len, flags) == (signed int)len ); // TODO
#ifndef __WIN32__
			return ( ::recvfrom(_sock, buf, length, flags, from, fromlen) );
#else			
			return ( ::recvfrom(_sock, (char*)buf, length, flags, from, fromlen) );
#endif
		}
		
		static ssize_t recvfrom_timeouted(__SOCKET _sock, void * buf, size_t length, int flags, struct sockaddr *from, socklen_t *fromlen, int timeout_sec, int timeout_usec);
		
/*#else // WIN32
		#warning "Not tested yet!" // TODO; MSG_WAITALL IS NO DAFAULT FLAG!
		inline ssize_t recv(__SOCKET _sock, void * buf, size_t len,  int flags) // TODO: no-inline -> basis.cpp
		{
			register size_t received = 0, next;
			do {
				next=(::recv(sock, (char*)buf+received, len-received, flags));
				received+=next;
			} while (received<len && next); // TODO ...
			return next;
		}
#endif		
*/
	
		//static void error_exit(const char *error_message) { DEBUG(error_message); exit(EXIT_FAILURE); }
		
		void set_use_timeout(bool _use = true) throw() {
			use_timeout = _use;
		}
		
		void set_timeout(int seconds, int useconds) throw() {
			timeout.tv_sec = seconds;
			timeout.tv_usec = useconds;
		}

	protected:
		bool use_timeout;
		struct timeval timeout; //!< timeout for receiving/sending (default is zero)
		const unsigned int RCVBUFSIZE; //!< maximum size for buffers in recv() callbacks
	private:
		static struct timeval tmp_tv;
	
};

} // namespace odf

#endif // BASIS_H
