/*************************************************************************/
/* server.cc - File for testing libodf's server                          */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;

//our network-header
#include <odf.h>

/*
	An example of howto use the Server Class properly
	The server will receive message from clients, print them to stdout
	and it will return an answer "answering" to the client.
	If the Client typed "quitserver", the Server will shutdown
*/

class MyServer : public Server
{
	public:
		MyServer(int _port):Server(_port) {}
		// This function will be called whenever a client sends a message to the server
		virtual void on_recv(unsigned int sock_id, void* buf, size_t length) // TODO: const?
		{
			printf("MyServer: on_recv: %s\n",(const char*)buf);
			send_client(sock_id,"answering",10); // send the client an answer
			puts("...");
			if(strcmp((const char*)buf,"quitserver")==0)
			 shutdown(); // shut down the server, quit the run function (see above))
		}
};

int main(int argc, char* argv[])
{
	
	if(argc!=2) {
		printf("Usage: %s <port>\n", argv[0]);
#ifdef __WIN32__
		puts("Get your local IP typing \"ipconfig \all\"!");
#endif
		exit(0);
	}
	int port = atoi ( (const char*)argv[1] );

	printf("Opening server with port %d...\n", port);
	
	try {
		MyServer server1(port); // set up the server
		server1.run(); // run it
	}
	catch (ErrorClass e) {
		e.print_error();
	}
	
	puts("End of server2.cpp"); // this will not be displayed until the server was shut down with the shutdown() function.
}
