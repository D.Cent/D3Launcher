/*************************************************************************/
/* client.cc - File for testing libodf's client                          */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <iostream>
#include <stdlib.h>

//our client-header
#include "../../../../odf.h"

using namespace std;

int main(int argc, char* argv[])
{

	if(argc!=3) {
		printf("Usage: %s IP PORT\n",argv[0]);
		exit(0);
	}
	
	char IP[128];
	strncpy(IP,argv[1],128);
	int port = atoi ( argv[2] );
	
	try {
		Client c(IP, port);
		
		while(1)
		{
			char message[128];
			char *echo_string;
			int echo_len;
		
			cout << "Your message (max. 128 characters): ";
			cin.getline(message, 128);
	
			echo_string = message;
			/* Length */
			echo_len = strlen(echo_string);
	
			/* Sending... */
			if (c.send( echo_string, echo_len+1, 0) == false)
			break;
			
			//if( ! strncmp(message, "quit", 4) )
			//	break;
			puts("ok, sent");
			if (c.recv( message, 10, 0) == false)
			{
				puts("client could not recv()!");
				break;
			}
			else
			printf("received answer: %s\n",message);
			
			if(!strcmp(message,"quit"))
			 break;
		}
        }
        catch (ErrorClass e) {
                e.print_error();
        }

}

