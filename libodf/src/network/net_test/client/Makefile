################################################################
# PIMake v 0.1 (Makefile)                                      #
#                                                              #
# Author: Johannes "King Lo" Lorenz                            #
#                                                              #
# This code is licensed under GPL 2.                           #
#                                                              #
# to install such Makefiles, type "odfmake"                    #
#                                                              #
# visit or contact us at http://odf.sourceforge.net !          #
################################################################

################################################################
# Fill in your options here:

# compiler
CC= g++
# compiler flags for compiling and linking
FLAGS = -ansi -Wall -g -D__LINUX__

# flags for the headers location
CFLAGS= -I../../..
# flags for the libs location
LIBS= -L../../../.. -lodf

# file endings that are recognized as source code (MUST BEGIN WITH .\)
ENDING = '\.cc'

# in which directories shall we search for source code?
SEARCH_IN_DIRS = '.'
# shall we also search in the subdirectories of those paths?
SEARCH_RECURSIVE = 'N'

# name of the file, where all the dependencies are stored in...
DEPENDFILE = .depend
# name of the outfile
OF = client
# softlink to the outfile
# (leave empty or comment out if there shall be none )
OF_SLINK =
# name of the installation Makefile
# (leave empty or comment out if there is none, standard is "Makefile.install")
#INSTALL_MAKEFILE = Makefile.install

# here are the options for a windows-crosscompiler (target: win32)

#the crosscompiler-command (can also begin with wine)
WINCC = i586-mingw32msvc-g++
# in which directories shall we search for source code?
WIN_DIRS = '.'
#the extra libs for windows
WIN_LIBS = -shared -fPIC -L/usr/i586-mingw32msvc/lib -lws2_32 
#flags for windows
WIN_FLAGS = -ansi -Wall -g
#outfile for windows
WIN_OUT = libodf.dll

################################################################
# Do not change any code under this line!
# The whole Makefile...

ifdef WIN32
CC=$(WINCC)
SEARCH_IN_DIRS=$(WIN_DIRS)
LIBS+=$(WIN_LIBS)
FLAGS=$(WIN_FLAGS)
OF=$(WIN_OUT)
endif

ifeq ($(SEARCH_RECURSIVE),'Y')
SOURCES = $(shell find $(SEARCH_IN_DIRS) | grep -w .*$(ENDING) ) # full source-file names with path (recursive)
else
SOURCES = $(shell TMP_DIRS=`echo $(SEARCH_IN_DIRS)`; for i in $$TMP_DIRS; do FNAME=`ls $$i`; for j in $$FNAME; do find . -name $$j| grep -w .*$(ENDING);done ; done )
#SOURCES = $(shell TMP_DIRS=`echo $(SEARCH_IN_DIRS)`; for i in `ls $$TMP_DIRS`; do echo find . -name $$i | grep -w .*$(ENDING); done ) # full source-file names with path
endif

OBJ = $(shell echo $(SOURCES) | sed 's/$(ENDING)/\.o/g') # full object file names with path

TARGETS = create $(OF)

all : $(TARGETS)

.SILENT : $(DEPENDFILE)
$(DEPENDFILE) :
	@echo "Checking dependencies..."
	if test -f $(DEPENDFILE); then rm -f $(DEPENDFILE); fi
	for i in $(SOURCES);do \
	$(CC) $(FLAGS) -MM $$i -MT `echo $$i | sed 's/$(ENDING)$$/.o/g'` >> $(DEPENDFILE); RES=$$?; \
	if [ $$RES==0 ]; then printf '	$(CC) $(FLAGS) $(CFLAGS) -c %b -o %b\n' $$i `echo $$i|sed 's/$(ENDING)$$/\.o/g'`>> $(DEPENDFILE); fi; \
	done

create: $(DEPENDFILE)

-include $(DEPENDFILE)

$(OF): $(OBJ)
	$(CC) $(FLAGS) $(LIBS) $(KOPETE_LIBS) $(KOPETE_FILES) $(OBJ) -o $(OF)
ifdef OF_SLINK
	ln -s $(OF) $(OF_SLINK)
endif
	@echo "...type 'make help' for more informations"

compile: $(OF)

clean:
	rm -f $(OBJ) $(OF) # remove all objective files and the outfile

cleand:
	rm -f $(DEPENDFILE) # remove the dependfile
	
tidy: clean cleand # remove everything...

remake: clean $(OF)
rebuild: tidy all

################################################################
# The installation code...

ifdef INSTALL_MAKEFILE
-include $(INSTALL_MAKEFILE) # include the installation Makefile
NO_INSTALL = 'INSTALL_MAKEFILE_SPECIFIED'
else
NO_INSTALL = 'NO_INSTALL_MAKEFILE_SPECIFIED'
endif

.SILENT : check_installing
check_installing:
	if [ $(NO_INSTALL) == 'NO_INSTALL_MAKEFILE_SPECIFIED' ]; then echo "NO INSTALL MAKEFILE SPECIFIED!"; false; fi
	#if [ $(NO_INSTALL) == 'NO_INSTALL_MAKEFILE_SPECIFIED' ]; then echo "NO INSTALL MAKEFILE SPECIFIED!"; false; fi
	
	
	
	if ! test -f $(INSTALL_MAKEFILE); then echo "SPECIFIED INSTALL MAKEFILE DOES NOT EXIST!"; false; fi # Does this Makefile exist?
	@echo "NO ERRORS, READY FOR INSTALLATION! :)"

install: check_installing install_extern
uninstall: check_installing uninstall_extern
show_install: check_installing
	$(MAKE) -n -f $(INSTALL_MAKEFILE) install_extern # -f lets make print out the results but does not execute them
show_uninstall: check_installing
	$(MAKE) -n -f $(INSTALL_MAKEFILE) uninstall_extern

################################################################
# The help code...

help:
	@echo ""
	@echo "Makefile for" $(OF)
	@echo ""
	@echo "Help for this Makefile:"
	@echo ""
	@echo "Type                   Action"
	@echo "=========================================="
	@echo ""
	@echo "make                   compile '"$(OF)"'"
	@echo "make depend            proceed or refresh depend-file '"$(DEPENDFILE)"'"
	@echo "make all               the same as make depend and make together"
	@echo ""
	@echo "make clean             remove all objective files and the outfile '"$(OF)"'"
	@echo "make cleand            remove the dependfile '"$(DEPENDFILE)"'"
	@echo "make tidy              the same as make clean and make cleand together"
	@echo ""
ifdef INSTALL_MAKEFILE
	@echo "The following 2 Options can be used by checkinstall..."
	@echo "make install           install everything (source in '"$(INSTALL_MAKEFILE)"')"
	@echo "make uninstall         uninstall everything (source in '"$(INSTALL_MAKEFILE)"')"
	@echo "make show_install      show how install would work, do not install"
	@echo "make show_uninstall    show how uninstall would work, do not uninstall"
	@echo ""
endif
	@echo "make help              show this help page"
	@echo ""
	@echo "========================================="
	@echo "If you want to specify options before compiling, try './configure' before make!"
	@echo ""

info: help

################################################################
# No comment about that :P

.SILENT : egg  # shhHHH SILENCE OR THE CHICKEN WONT SUCCEED!
egg:
	 echo "putt putt putt"; sleep 3; echo "plop!"
