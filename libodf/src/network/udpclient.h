/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file	client.h
	\brief	File containing the Client class.
	
	This file contains the Client class, which is used to set up a client.
*/


#ifndef UDPCLIENT_H
#define UDPCLIENT_H UDPCLIENT_H

#include <string> // for the IP string
using namespace std;

#include "client.h"

namespace odf {

/**
	\class	Client
	\brief	The Client class is a class, which is used to set up a server.
	
	The client class provides easy client handling. The constructor sets up everything you need to start and the desctructor cleans up everything.
*/
class UDPClient : public Client {

	public:
		/**
			Constructor. Initializes base class.
			@param IP IP of the host you want to connect to.
			@param port Port of the host you want to connect to.
			@param timeout Currently unused.
		*/
		UDPClient( const char *IP, int port, unsigned char timeout=true /*, unsigned int _RCVBUFSIZE = 1024*/ ) :
			Client(IP, port, eUDP, timeout) // only initialize port and IP, no connection for UDP!
			{}
		
		//! Currently, the destructor does nothing
		virtual ~UDPClient() {}
		
		/**
			\brief Send data to server
			\note The parameters are similar to the real send() function's last 3 parameters.
			@param buf Points to the buffer containing the message to send.
			@param length Specifies the length of the message in bytes.
			@param flags Specifies the type of message transmission.
			
		*/
		
		inline virtual bool send(const void * buf, size_t length, int flags=0) {
			return Basis::asendto(sock, buf, length, flags, (sockaddr*)&sock_addr, sizeof(sock_addr));
		}

                /**
                        \brief Recv data from server
			\note The parameters are similar to the real recv() function's last 3 parameters.
			@param buf Points to a buffer where the message should be stored.
			@param length Specifies the length in bytes of the buffer pointed to by the \a buf argument.
			@param flags Specifies the type of message reception.
                */
//		inline int recv(void * buf , size_t length, int flags=MSG_WAITALL) { // TODO: still inline?!?
		inline virtual int recv(void * buf , size_t length, int flags=0) {
		//	return Basis::arecv(sock, buf, length, flags); // TODO: security??! -> recfrom and compare address!
			socklen_t sock_len = sizeof(sock_addr);
			return Basis::arecvfrom(sock, buf, length, flags, (sockaddr*)&sock_addr, &sock_len);
		}
		
};

} // namespace odf

#endif // UDPCLIENT_H

