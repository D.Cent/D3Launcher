/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "udpserver.h"

#ifdef __WIN32__
	#include "../os_fix.h"
	#include <ws2tcpip.h>
#endif

namespace odf {

UDPServer::UDPServer(int port, unsigned int _RCVBUFSIZE)
	: Server(port, eUDP), quit_udp_srv(false)
{
}

bool UDPServer::recv_any(void)
{
#if 0	
	struct timeval tv; // time interval for select
	tv.tv_sec = 1;
	tv.tv_usec = 0;
	
	int answers;

	restore_rfds();

	answers = select(highest_fd+1,&rfds,NULL,NULL,NULL);
	if(answers==-1)
	 throw NEW_ERROR("select() failed");

#ifdef LIBODF_DEBUG
	printf("we have %d answers!\n",answers);
#endif
		
	if(answers)
	{
		 for(clientmap::iterator itr = ClientInfo.begin(); itr != ClientInfo.end(); )
		 {
		 	if( FD_ISSET(itr->first,&rfds) ) // check if any client in ci IS SET in the FD SET
			{
				char echo_buffer[RCVBUFSIZE];
				ssize_t recv_size;

				recv_size = Basis::recv(itr->first, echo_buffer, RCVBUFSIZE,0);
	
				if( recv_size == -1 ) // error
				 throw NEW_ERROR("recv() failed at recv_any()",EC_CONTINUE); // TODO - really throw here? - if not : itr++
				else if( recv_size == 0 ) // client disconnected
				 on_client_disconnected(...);
				else // receive message
				{
#ifdef LIBODF_DEBUG
					//printf("A new message was received by client with sock %d:\n",itr->first);
					//puts(echo_buffer);
#endif
					on_recv(itr->first, echo_buffer, recv_size);
					itr++;
				}
			} // if( FD_ISSET ...)
		 } // for
	} // if(answers)
	return true;
#endif
	char buf[RCVBUFSIZE];
	struct sockaddr_in sa_in;
	socklen_t len = sizeof(sa_in);
	int ret_val = Basis::arecvfrom(sock, buf, RCVBUFSIZE, 0, (struct sockaddr *)&sa_in, &len);
	if(ret_val > 0) {
		DEBUGF("BEFORE CALL RECV: RECV FROM %s (%d)\n",inet_ntoa(sa_in.sin_addr), len);
		on_recv(sock, buf, ret_val, (struct sockaddr *)&sa_in, &len);
	}
	return ret_val;
}

inline void p_sig_handler(int) {
	//quit_udp_srv = true;
	// TODO
}

void UDPServer::run(void)
{
	if (signal(SIGINT, p_sig_handler)==SIG_ERR)
	 throw NEW_ERROR("Error setting up signal routine for server");
	
	while(!quit_udp_srv) {
		recv_any();
	}
	quit_udp_srv = false;
}

void UDPServer::shutdown(void) {
	quit_udp_srv = true;
	// TO BE DONE!
}

} // namespace odf
