/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file odf_socket.h
	\brief File extending the Basis class with socket functionality
	
	This file contains the Socket Class, a class which is the base class for the Server and Client classes
*/

#ifndef ODF_SOCKET_H
#define ODF_SOCKET_H ODF_SOCKET_H

#if 0
	#include <cstdio>
	#include <cstdlib>
	#include <cstring>
	#include <cerrno>
	using namespace std;

	#ifdef __WIN32__
	/* Headerfiles for Windows */
	#include "ws2tcpip.h"
	#include <winsock2.h>
	#include <io.h>

	#else
	/* Headerfiles for Unix/Linux */
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <arpa/inet.h>
	#include <unistd.h>
	#endif
#endif

#include "basis.h"

namespace odf {

/**
	\enum eproto
	\brief This type specifies which network protocol to be used.

	Currently supported are:
	- TCP
	- UDP
*/

enum eproto {
	eTCP=0,
	eUDP,
	eICMP
};

/**
	\class Socket
	\brief A Class which is the basis class for the Server and Client classes.

	The Basis class does tasks that are needed for networking.
	It is inherited by both the Server and the Client class.
	As every network class, it contains
	 - A socket, see Basis::sock
	 - A socket address, see server
	 - A constructor which sets up a socekt, see Basis()
	 - A destructor that closes the socket, see ~Basis()
*/
class Socket : public Basis
{
	public:	
		/**
			\brief The constructor opens a socket and intializes the socket address.
			@param port The port that should be opened.
			@param use_proto The network protocol the Basis class should use.
			@param use_timeout Defines whether timeouts should be used for send()/recv()
				(suggested for blocking IO)
			@param _RCVBUFSIZE maximum size for recv()'s callbacks.
		*/
		Socket(int port, eproto use_proto = eTCP, bool _use_timeout=true, unsigned int _RCVBUFSIZE = 1024);
		
		//! The destructor closes the socket.
		virtual ~Socket();
		
		inline int socket_id(void) const throw() {
			return (int) sock;
		}
		
		inline unsigned short get_port(void) const throw() {
			return (ntohs(sock_addr.sin_port));
		}

	protected:
		__SOCKET sock; //!< the socket file descriptor
		struct sockaddr_in sock_addr; //!< the socket address
		const eproto proto; //!< protocol used
};

} // namespace odf

#endif // ODF_SOCKET_H
