/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file	client.h
	\brief	File containing the Client class.
	
	This file contains the Client class, which is used to set up a client.
*/


#ifndef TCPCLIENT_H
#define TCPCLIENT_H TCPCLIENT_H

#include <string> // for the IP string
using namespace std;

#include "client.h"

namespace odf {

/**
	\class	Client
	\brief	The Client class is a class, which is used to set up a server.
	
	The client class provides easy client handling. The constructor sets up everything you need to start and the desctructor cleans up everything.
*/
class TCPClient : public Client {

	public:
		/**
			First, the constructor does all initialization stuff from the Basis class.
			Then, it tries to connect to the server (host).
			@param IP IP of the host you want to connect to.
			@param port Port of the host you want to connect to.
			@param timeout Currently unused.
			@param _RCVBUFSIZE maximum size for recv()'s callbacks.
		*/
		TCPClient( const char *IP, int port, unsigned char timeout=0, unsigned int _RCVBUFSIZE = 1024 );
		
		//! Currently, the destructor does nothing
		virtual ~TCPClient() {}
		
		/**
			\brief Send data to server
			\note The parameters are similar to the real send() function's last 3 parameters.
			@param buf Points to the buffer containing the message to send.
			@param length Specifies the length of the message in bytes.
			@param flags Specifies the type of message transmission.
			
		*/
		inline bool send(const void * buf, size_t length, int flags=0) {
			return Basis::asend(sock, buf, length, flags);
		}

                /**
                        \brief Recv data from server
			\note The parameters are similar to the real recv() function's last 3 parameters.
			@param buf Points to a buffer where the message should be stored.
			@param length Specifies the length in bytes of the buffer pointed to by the \a buf argument.
			@param flags Specifies the type of message reception.
                */
//		inline int recv(void * buf , size_t length, int flags=MSG_WAITALL) { // TODO: still inline?!?
		inline int recv(void * buf , size_t length, int flags=0) {
			return Basis::arecv(sock, buf, length, flags);
		}

};

} // namespace odf

#endif // TCPCLIENT_H

