/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file win_fix.h
	\brief Windows only functions

	This is a completion of functions which are not available at Windows
*/

#ifndef NONBSD_FIX_H
#define NONBSD_FIX_H NONBSD_FIX_H

#ifndef __BSD__

/**
	Compares the two strings @a s1 and @a s2, ignoring the case of the characters.
	@return The return value is an integer less than, equal to, or greater han zero if s1 is found, respectively,
	to be less than, to match, or be greater than s2.
*/
int strcasemp(const char* str1, const char* str2);

//!	This function is similar to stricmp(), except it only compares the first @a n characters of @a s1.
int strncasecmp(const char* str1, const char* str2, unsigned int n);

#endif // __BSD__

#endif // NONBSD_FIX_H

