/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
        \file regfile.h
        \brief This file contains the RegFile class
*/

#ifndef REGFILE_H
#define REGFILE_H REGFILE_H

#include <cstdio>
#include <map>
#include <string>
using namespace std;

#include "optfile.h"

namespace odf {

/**
	\class RegFile
	\brief This class can handle (read and write) Windows Registry files.

	The file can look like this:
	@code
	[section1]
	"key1"="value1"
	"key2"="value2" # comment(s)
	...
	[section2]
	...
	@endcode
	
	\note Currently, 'dword:' entrys are also displayed as normal strings
*/
class RegFile : public OptFile
{
	private:
		
		static void get_relevant_data(string* str); // inherited because registries have no quotes
		
		// ignore newlines
		inline /*static*/ bool line_is_useful(const string& line) const
		{
			return ( line.length() != 0 );
		}
		
		//! Checks whether the given line marks the beginning of a new section
		inline bool line_is_section(const string& line) const {
			return (line[0]=='[' && line.find(']') != string::npos);
		}
		
		string get_section_name(const string& line) const;
		
		//! add quotes to string str - IF NECESSARY (i.e. if the string does not begin with "dword:")
		virtual bool pack_into_quotes(string* str) const;
		
	public:
		RegFile(const char* _filename, const char* _openmod = "r+b"):
		 OptFile(_filename, true, _openmod) { } // true => Registry files have ALWAYS sections

#if 0
	public:
		/**
			Constructor, which will open the optfile
			@param _filename Name of the file that should be opened. Must be a seekable file.
			@param _sections If the file is devided in sections, this should be true, otherwise false
		*/
		RegFile(const char* _filename, bool _sections = true):
		 OptFile(_filename, true) {} // Registry files have ALWAYS sections
		
		/**
			Reads the optfile or parts of it into an std::map.
			@param m Map where the optfile or its parts shall be stored in.
			@param section Section which shall be stored. Only for optfiles with sections.
		*/
		void to_map(map<string,string>& m, const char* section=NULL);

		/**
			Writes an std::map into a file. The file will be updated.
			@param m Map where the data is stored in.
			@param section Section which is represented by the data. Only for optfiles with sections.
			@param actualize unused
		*/
		void from_map(const map<string, string>& m, const char* section=NULL, bool actualize = true);
		
		/**
			Searches for a stringpair (a=b), where the left parameter (a) is equal to \a left, and 
			replaces the right parameter with \a right
			@param left Parameter that is searched for.
			@param right Replacement for the value right ot \a left
			@param section Section which shall be searched in. Only for optfiles with sections.
			@param str_in_quotes writes strings (all excapt of registry dword entrys) in quotes ("a"="b")
			@return The right value of the stringpair (b). If an error occured, an empty string is occured.
			 Errors can be read errors, end of section or EOF (which means that the left value is not found)
		*/
		void set_single_entry(const char* left, const char* rigth, const char* section=NULL, bool str_in_quotes=false);
#endif
};

} // namespace odf

#endif // OPTFILE_H

