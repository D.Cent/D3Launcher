/*************************************************************************/
/* main.cc - File for testing libodf's functions                         */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <iostream>
#include <stdexcept>
using namespace std;

#include <odf.h>
using namespace odf;

#define CALL_FUNC(func) cout << "Calling function " << (#func) << endl; func;
#define CALL_FUNC_RES(func, ret_val) cout << "Calling function " << (#func) << endl; ret_val = func; cout << "Return value: " << ret_val << endl << endl;

int main(void)
{
	int ret_int;
	bool ret_bool;
	
	cout.setf(ios::boolalpha);
	cout << endl;
	cout << "Welcome! This is the libodf test..." << endl;
	cout << "===================================" << endl << endl;
	
	cout << "TESTING STRING FUNCTIONS... (win_fix.h)" << endl;
	
	CALL_FUNC_RES(stricmp("hello","hello"), ret_int);
	CALL_FUNC_RES(stricmp("hello","hell no"), ret_int);
	CALL_FUNC_RES(strnicmp("hello","hello",2), ret_int);
	
	cout << "TESTING ERROR CLASS (error_class.h)" << endl;
	try {
		throw NEW_ERROR("Throwing an instance of ErrorClass", EC_CONTINUE);
	}
	catch (ErrorClass ec) {
		cout << "Error Class caught successfully!" << endl;
		ec.print_error();
	}
	
	cout << endl;
	
	try
	{
		cout << "TESTING PATHS (paths.h)" << endl; // TODO
		
		//CommonPath<HomePath> hp;
		HomePath hp;
		cout << "Your home path should be: " << hp << endl;
		//CommonPath<SharePath> sp;
		SharePath sp;
		cout << "Your ODF share path should be: " << sp << endl;
		CommonPath<HomePath> hp_subdir(".loki");
		cout << "Use the CommonPath<HomePath> class to use subdirs like: " << hp_subdir << endl;
		CommonPath<HomePath> hp_subdir_2( Path(".loki") );
		cout << "Concatenating with the Path class works as well: " << hp_subdir << endl;
		
		D3HomePath d3hp;
		cout << "Your Descent3 user data path should be: " << d3hp << endl << endl;
#ifdef __LINUX__
		D3SharePath d3sp;
		cout << "Your Descent3 share data path should be: " << d3sp << endl << endl;
#endif		
		
		cout << "If you would install a programm named \"d321go\", it would have the following paths: " << endl;
		AppPaths A("d321go");
		cout << "homepath: " << A.homepath.name << ", ODF share path: " << A.sharepath.name << endl;
		cout << "The file \"options.txt\" would, hence, be in" << endl;
		cout << "either: " << A.homepath + "options.txt" << ", or: " << A.sharepath + "options.txt" << endl << endl;

		cout << "TESTING FILE FUNCTIONS (t_funcs.h and file_funcs.h)" << endl;
		
		CALL_FUNC_RES(file_is_print("main.cc"),ret_bool);
		/* CALL_FUNC_RES(file_is_print("libodf_test"),ret_bool); */ // TODO
		
		CALL_FUNC(cp("test.txt","test2.txt",false));

/*		FILE* fp;
		struct stat fstats;
		
		CALL_FUNC(t_access("test2.txt",W_OK));
		
		fp = t_fopen("test2.txt","r+");
		
		CALL_FUNC(t_fseek(fp,+37,SEEK_SET));
		CALL_FUNC(block_copy(fp,4,-30));
		
		t_stat("test2.txt",&fstats);
		CALL_FUNC(t_fseek(fp,1,SEEK_SET));
		CALL_FUNC(block_remove(fp,-1,fstats.st_size));
		
		t_stat("test2.txt",&fstats);
		//CALL_FUNC(t_fseek(fp,35,SEEK_SET));
		CALL_FUNC(seek_after_str(fp,"not"));
		CALL_FUNC(seek_to_str(fp,"not"));
		
		CALL_FUNC(block_remove(fp,4,fstats.st_size));

		t_stat("test2.txt",&fstats);
		CALL_FUNC(t_fseek(fp,32,SEEK_SET));
		CALL_FUNC(block_insert(fp,3,fstats.st_size));
		fputs(":P ",fp);

		t_stat("test2.txt",&fstats);
		CALL_FUNC(block_insert(fp,-28,fstats.st_size));
		CALL_FUNC(t_fseek(fp,-28,SEEK_CUR));
		fputs("(I'm just kidding, checky!) ",fp);
		
		t_fseek(fp,0,SEEK_SET);
		char result_sentence[256];
		fgets(result_sentence,256,fp);
		const char* should_sentence =	"DF is not best foundation ever. :P "
						"(I'm just kidding, checky!) Why joining it?\n";
		puts("The file looks like that, now:");
		puts(result_sentence);
		if(strcmp(result_sentence,should_sentence)!=0) {
			printf("sentence SHOULD be:\n%s",should_sentence);
			throw NEW_ERROR("block copy functions did not succeed!",EC_STOP_EXECUTION);
		}
		else
		 puts("block copy functions SUCCEEDED!");
		
		t_fseek(fp, 0, SEEK_SET);
		char* sentence = NULL;
		if(read_line_seek(fp, sentence, '.'))
		 printf("result of read_line_seek(): \"%s\"\n\n",sentence);
		else
		 throw NEW_ERROR("Could not read a sentence ending with a point (in test2.txt)",EC_CONTINUE);
		
		t_fseek(fp, 0, SEEK_SET);
		char trunc_result[256];
		t_ftruncate(fp, strlen(sentence)+1);
		t_fread(trunc_result, 1, strlen(sentence)+1, fp); // TODO
		puts("Truncated file looks like this:");
		t_fwrite(trunc_result, 1, strlen(sentence)+1, stdout);
		puts("");
		//printf("truncated file text2.txt, content is now: \"%s\"\n");
		
		delete[] sentence;
		
		CALL_FUNC_RES(checkdir("."), ret_bool);
		CALL_FUNC_RES(checkdir("text2.txt"), ret_bool);
		// TODO: t_mkdir()
		t_fclose(fp);*/

		tFILE fp;
		struct stat fstats;
		
		CALL_FUNC_RES(tFILE::access("test2.txt",W_OK), ret_bool);
		
		fp.open("test2.txt","r+");
		
		CALL_FUNC(fp.seek(+37,SEEK_SET));
		CALL_FUNC(block_copy(fp,4,-30));
		
		fp.stat(&fstats);
		CALL_FUNC(fp.seek(1,SEEK_SET));
		CALL_FUNC(block_remove(fp,-1,fstats.st_size));
		
		fp.stat(&fstats);
		//CALL_FUNC(t_fseek(fp,35,SEEK_SET));
		CALL_FUNC(seek_after_str(fp,"not"));
		CALL_FUNC(seek_to_str(fp,"not"));
		
		CALL_FUNC(block_remove(fp,4,fstats.st_size));

		fp.stat(&fstats);
		CALL_FUNC(t_fseek(fp,32,SEEK_SET));
		CALL_FUNC(block_insert(fp,3,fstats.st_size));
		fputs(":P ",fp);

		fp.stat(&fstats);
		CALL_FUNC(block_insert(fp,-28,fstats.st_size));
		CALL_FUNC(fp.seek(-28,SEEK_CUR));
		fputs("(I'm just kidding, checky!) ",fp);
		
		fp.seek(0,SEEK_SET);
		char result_sentence[256];
		fgets(result_sentence,256,fp);
		const char* should_sentence =	"DF is not best foundation ever. :P "
						"(I'm just kidding, checky!) Why joining it?\n";
		puts("The file looks like that, now:");
		puts(result_sentence);
		if(strcmp(result_sentence,should_sentence)!=0) {
			printf("sentence SHOULD be:\n%s",should_sentence);
			throw NEW_ERROR("block copy functions did not succeed!",EC_STOP_EXECUTION);
		}
		else
		 puts("block copy functions SUCCEEDED!");
		
		fp.seek(0, SEEK_SET);
		char* sentence = NULL;
		if(read_line_seek(fp, sentence, '.'))
		 printf("result of read_line_seek(): \"%s\"\n\n",sentence);
		else
		 throw NEW_ERROR("Could not read a sentence ending with a point (in test2.txt)",EC_CONTINUE);
		
		fp.seek(0, SEEK_SET);
		char trunc_result[256];
		fp.truncate(strlen(sentence)+1);
		fp.read(trunc_result, 1, strlen(sentence)+1); // TODO
		puts("Truncated file looks like this:");
		fwrite(trunc_result, 1, strlen(sentence)+1, stdout);
		puts("");
		//printf("truncated file text2.txt, content is now: \"%s\"\n");
		
		delete[] sentence;
		
		CALL_FUNC_RES(checkdir("."), ret_bool);
		CALL_FUNC_RES(checkdir("text2.txt"), ret_bool);
		// TODO: t_mkdir()
		fp.close(); // constructor would do the same - only for demonstration
		
	}
	catch (ErrorClass ec) {
		cout << "Error Class caught!" << endl;
		ec.print_error();
	}
	/*catch (exception e) {
		cout << "std c++ exception caught!" << endl;
		cout << e.what() << endl;
	}*/
	catch (domain_error) {
		cout << "2" << endl;
	}	
	catch (invalid_argument) {
		cout << "3" << endl;
	}
	catch (length_error) {
		cout << "4" << endl;
	}
	catch (out_of_range) {
		cout << "5" << endl;
	}
	catch (overflow_error) {
		cout << "8" << endl;
	}
	catch (underflow_error) {
		cout << "8" << endl;
	}
	catch (logic_error) {
		cout << "1" << endl;
	}
	catch (range_error) {
		cout << "7" << endl;
	}
	catch (runtime_error) {
		cout << "6" << endl;
	}
	catch(...) {
		cout << "Unknown error caught!" << endl;
	}
	return 0;
}
