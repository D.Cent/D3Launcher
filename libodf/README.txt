LIBODF 0.12
A library for programs of the Open Descent Foundation
==============================================================================

Before you read this README text file, look at the README.html file to view it
in HTML. The contents are equal.

CONTENTS
::::::::

1. What is libodf?
2. Installing
3. Documentation
4. License
5. Testing
6. Contact
7. Thanx to

1. WHAT IS libodf?
::::::::::::::::::

libodf is a libary which is used by lots of programs of the Open Descent
Foundation. You may visit us at http://odf.sf.net .

libodf has nothing to do with the Open Document Format.

The main aims are:
- system independency
- safe functions, errors are throwen
- easy use of C/C++, especially file functions and networking

libodf was mostly written by King Lo. D.Cent also took part.

Read more on our wiki (on our homepage).

2. INSTALLING
:::::::::::::

First of all, you need to download either the sources or a binary from our
website. See 6. CONTACT.

For details about installation see the file 'INSTALL.txt' in this directory.

3. DOCUMENTATION
::::::::::::::::

The full documentation is in the doc directory. You may choose between
- HTML (html subdir)
- LATEX (latex subdir)
- man pages (man subdir)

4. LICENSE
::::::::::

All sources are licensed under the terms of the GPL v3. See the file
'LICENSE.txt' in this directory.

5. TESTING
::::::::::

Switch into the 'test' directory. You will find a GNU-Makefile and some sources
there. Compile the binary you want and then run it in a console. Lots of useful
debugging information will be displayed.

6. CONTACT
::::::::::

Visit the Open Descent Foundation at http://odf.sf.net . You will also find our
email addresses at our site (see imprint on the site).

7. THANX TO
:::::::::::

The Lion for all the support and the knowledge you provided us with!
Wormaus for the ODF-Server 

Linus Torvalds & Co. for Linux
Richard Stallman & Co. for GNU, GLIB etc.
Bill Gates for giving us an example of how to write bad Software :P
Kiefer Sutherland & Co. for entertaining us

...and everyone else we forgot!

