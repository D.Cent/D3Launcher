/** @file mainpage.h
* @brief Doxygen Main Page
*
* $Header: /nfs/slac/g/glast/ground/cvs/workbook/pages/advanced_doxygen/usingDoxygen.htm,v 1.1.1.1 2007/06/29 15:03:16 chuckp Exp $
*/
/** @mainpage

	@authors Johannes & Philipp Lorenz

	@section intro What is Libodf?
 
	libodf is a libary which is used by lots of programs of the Open Descent
	Foundation. You may visit us at http://odf.sf.net .

	libodf has nothing to do with the Open Document Format.

	The main aims are:
	- system independency
	- safe functions, errors are throwen
	- easy use of C/C++, especially file functions and networking
	
	libodf was mostly written by King Lo. D.Cent also took part.

	Read more about libodf in the
	<A HREF="../../html/README.html">README file</A>.

	<hr>
	@section notes release.notes
	This is the first release!
	<hr>
	@section LICENSE LICENSE
	This project is licensed under the GPL v3. Read the
	<A HREF="../../html/LICENSE.html">LICENSE file</A>.
	<hr> 

*/

// useful: @htmlinclude
