/*************************************************************************/
/* LIBODF - Library for developers of the Open Descent Foundation        */
/* Copyright (C) 2007-2010                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/* Open Descent Foundation (http://odf.sf.net)                           */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 2 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file odf.h
	\brief Main file
	
	Simply include this file, it will include all other parts of the libodf!
	\note libodf currently only supports Linux, BSD and Windows. odf.h will warn you if your system was not tested.
*/

// TODO:	signal class? (sigproc.cpp)
//		mein_execv()
//		odf namespace
//		functions throw() specification
//		LICENSE

#ifndef ODF_H
#define ODF_H ODF_H

#if defined(linux) || defined(__linux) || defined(__linux__)
	#undef __LINUX__
	#define __LINUX__       1
#endif

#if (!defined __WIN32__) && (!defined __LINUX__) && (!defined __BSD__)
	#warning "Your system is probably not supported. If it does not work or compile, contact the ODF!"
#endif

#define LIBODF_VERSION "0.12"

#include "src/os_fix.h"

#include "src/defines.h"
#include "src/typedefs.h"
#include "src/error_class.h"
#include "src/paths.h"
#include "src/file_funcs.h"
#include "src/t_funcs.h"
#include "src/tfile.h"
#include "src/dirs.h"
#include "src/optfile.h"
#include "src/regfile.h"

#include "src/network/tcpserver.h"
#include "src/network/udpserver.h"
#include "src/network/tcpclient.h"
#include "src/network/udpclient.h"
#include "src/network/httpclient.h"

#endif // ODF_H
