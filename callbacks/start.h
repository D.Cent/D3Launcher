/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "../main/globals.h"
#include "../tools/mc_string.h"
#include "d321go_execvp.h"

/**
	Start Descent 1,2,3 or Flight Back
	@param ver Descent game version as number, i.e. 1,2 or 3
	@param direct_ip IP for direct joining, NULL if none given
	@param missionname name of mn3 file - not needed for downloading,
		but d321go can check then if the mission is already available.
*/
bool func_start(unsigned char ver, const char* direct_ip, const char* missionname = NULL);

struct d3server;

/**
	Start Descent3 with detailed server info
	\note Use this function whenever you have this information
	@param srv server struct containing all the information
*/
bool func_start(const d3server* srv);


void start_dedicated_server(string dedicated_cfg, string gamespy_cfg);


inline void start_teamspeak(void)
{
	d321go_globals::PlayerOptfile->rewind();
	d321go_execvp(
		MultipleChoiceString::get_current(
			d321go_globals::PlayerOptfile->get_single_entry("ts_start","d321go_dirs")
			).c_str(),
#ifdef __LINUX__
		"/ts3client_runscript.sh"
#else
		"\\TeamSpeak.exe"
#endif
		);
}
