/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include "../network/download_level.h" // for downloading level before starting
#include "../network/server_structs.h" // for the server struct "d3server"

#include <sstream>
#include <string>
#include <map>
#include <thread>
using namespace std;

#ifdef __LINUX__
 #include <sys/wait.h>
#endif

#include <odf.h>
using namespace odf;

#include "start.h"

bool func_start_main(unsigned char ver, const char* direct_ip, const char* missionname);

//#ifdef __WIN32__
std::string start_cppstring;
std::string restore_keymap;

void* start_dx(void*)
{
	printf("Starting with this command: %s\n", start_cppstring.c_str());
	system(start_cppstring.c_str());

#ifdef __LINUX__
	if(restore_keymap.length() > 0)
	{
		if(system(("setxkbmap "+restore_keymap).c_str()) != 0)
		 printf("ARGH - Could not restore original xkbmap!\n");
	}
#endif

	return NULL;
}
//#endif

bool func_start(const d3server* srv)
{
	char ip_buf[22] = ""; // the length is correct

	strncpy( ip_buf, srv->ip.c_str(), 16 );
	snprintf( ip_buf + strlen(ip_buf), 7, ":%d", srv->hostport);
			
	return func_start_main(3, ip_buf, srv->mn3.c_str());
}


bool func_start(unsigned char ver, const char* direct_ip, const char* missionname)
{
	if(ver == 0 || ver > 3)
	 return false;
	
	return func_start_main(ver, direct_ip, missionname);
}





/*
	TODO:
		-Y/N Dialog bei start ohne IP
		d1/d2: ip ok?
*/

bool func_start_main(unsigned char ver, const char* direct_ip, const char* missionname)
{
	if( missionname != NULL && *missionname==0 )
	 missionname = NULL;

#ifdef D321GO_DEBUG	
	printf("func_main_start(): direct ip %s\n",direct_ip);
#endif

	string start_command; /* 3 commands for Descent 1, 2 ad 3 */

	OptFile *of = d321go_globals::PlayerOptfile;
	
	/* strcpy(nummer_ip,"no_direct"); */
	
	/*char **ergebnisse;
	int count; */
	
	
	char section[11] = "d0_options";
	section[1] += ver;
	char game_name[3] = "d0";
	game_name[1] += ver;
	
	// EXP_WARN
	
	/*
	start command:
	prefix command(incl. path) parameters postfix
	*/
	
	start_command = of->get_single_entry(game_name,"dx_prefixes");
	start_command.push_back(' ');
	
#ifdef __WIN32__
	start_command.push_back('\"');
#endif

	if( ver == 1 || ver == 2 )
	 start_command += MultipleChoiceString::get_current( of->get_single_entry(game_name,"d321go_dirs") ); // get prefix
#ifdef __WIN32__
	else if (ver == 3)
	 start_command += MultipleChoiceString::get_current( of->get_single_entry("d3_root","d321go_dirs") );
#endif

#ifdef __LINUX__
	if(ver != 3)
	 start_command += "/";
#else
	start_command += "/";
#endif

	start_command += MultipleChoiceString::get_current( of->get_single_entry(game_name,"dx_commands") );

#ifdef __LINUX__
	start_command.push_back(' ');
#elif __WIN32__
	start_command += "\" ";

	char old_cwd[1024];

	if( ver == 3)
	{
		start_command += " -launched ";

		getcwd(old_cwd, 1024);
		chdir(MultipleChoiceString::get_current( of->get_single_entry("d3_root","d321go_dirs").c_str()).c_str());
	}
#endif
	// TODO: what if direct_ip == NULL?!?!? SIGSEGV?!?!
	const char* direct_ip_needed = direct_ip; // IP without protocol suffix (dx://)
	if(strstr(direct_ip, "://") != NULL)
	 direct_ip_needed = strstr(direct_ip, "://") + 3;
	
	// Tell D3 about the DIRECT IP
	if(strlen(direct_ip) > 0)
	{
		switch(ver)
		{
		case 3:
		{
#ifndef __LINUX__
			start_command += "-directip ";
			start_command += direct_ip_needed;
			start_command.push_back(' ');
#endif

			break;
		}
		case 1:
		case 2: // TODO: write into dxx's ini file? what does zico want?
			std::string ip(direct_ip_needed);
			std::string port(direct_ip_needed);

			if (port.find_first_of(":") == std::string::npos)
			 port = "42424";
			else
			{
				int pos = port.find_first_of(":");
				ip = ip.substr(0, pos);

				port = port.substr(pos+1);
			}

			start_command += "-udp_hostaddr ";
			start_command += ip;
			start_command += " -udp_hostport ";
			start_command += port;
			start_command.push_back(' ');
			break;
		}
		
	}
	
	map<string,string> ArgList;
	
	of->rewind();
	of->to_map(ArgList, section);
	
	// Parse parameter map and write them into a string so descent recognizes them as arguments
	std::map<string, string>::iterator itr = ArgList.begin();
	for(; itr != ArgList.end(); itr++)
	{
		//string first_option = MultipleChoiceString::get_current(itr->second);
		//printf("substr: %s, size: %d\n",first_option.c_str(), first_option.size());
		
		MultipleChoiceString mc_arg = itr->second;
		if(mc_arg.get_current().size()==0) // only \0, so nothing
		 continue;
		
		if(mc_arg.get_type() == CHOICE_CHECKBOX) {
			if(mc_arg.get_current() != "0")
#ifdef __LINUX__
			 start_command+=((ver==3) ? " --" : " -" )+itr->first; // d1 and d2 have only one minus
#elif __WIN32__
			 start_command+=(" -")+itr->first;
#endif
		}
		else {
#ifdef __LINUX__
			start_command+=((ver==3) ? " --" : " -" )+itr->first; // d1 and d2 have only one minus
#elif __WIN32__
			start_command+=(" -")+itr->first;
#endif
			start_command+=' ' + mc_arg.get_current();
		}
		
		/*if(first_option.size()==0) // only \0, so nothing
		 continue;
		if(first_option != "0") {
			start_command+=((ver==3) ? " --" : " -" )+itr->first; // d1 and d2 have only one minus
			start_command+=' ' + first_option;
		}*/
		
		
	}

#ifdef __LINUX__
	if(strlen(direct_ip) > 0 && ver == 3)
	{
		start_command += " -- --directip ";
		start_command += direct_ip_needed;
		start_command += " ";
	}
	else if (ver == 3)
		start_command += " -- ";
#endif	

	start_command += of->get_single_entry(game_name,"dx_postfixes");
	
	// download level if descen3, and if server and missionname are given	
	if(strlen(direct_ip_needed) > 0 && (ver==3) && missionname!=NULL)
	{
		if(strchr(direct_ip_needed, ':') != NULL)
		{
			char download_ip[16];
			memset(download_ip, 0, 16);
			strncpy(download_ip, direct_ip_needed, strchr(direct_ip_needed, ':') - (const char*)direct_ip);
			download_level_by_mn3(download_ip, atoi(strchr(direct_ip_needed, ':')+1), missionname );
		}
		else // use default port
		 download_level_by_mn3(direct_ip_needed, 2092, missionname);
	}
	
#ifdef USE_KOPETE
	// send an information to #descent and then go away (if necessary)
	if(strlen(direct_ip_needed) > 0)
	{
		d321go_globals::kopete_client.playing_msg(direct_ip);
		string status_str = MultipleChoiceString::get_current( of->get_single_entry("on_gamestart","chat"));
		d321go_globals::kopete_client.change_status((status_str=="away")?STATUS_AWAY:STATUS_QUIT,direct_ip);
	}
#endif

#ifdef __LINUX__
	string myxkbmap_str = MultipleChoiceString::get_current( of->get_single_entry("myxkbmap","d321go_options") );
	string setxkbmap_str = MultipleChoiceString::get_current( of->get_single_entry("setxkbmap","d321go_options") );
	
	// setting the xkbmap to 'us' if the user wants it
	if(setxkbmap_str!=myxkbmap_str)
	{
		if(system(("setxkbmap "+setxkbmap_str).c_str()) != 0)
		 msg_box("Error", "Keyboard map couldn't be set.");
	}

	if(setxkbmap_str!=myxkbmap_str)
	 restore_keymap = myxkbmap_str;
	else
	 restore_keymap = "";
#endif

/*#ifdef __LINUX__
	string myxkbmap_str = MultipleChoiceString::get_current( of->get_single_entry("myxkbmap","d321go_options") );
	string setxkbmap_str = MultipleChoiceString::get_current( of->get_single_entry("setxkbmap","d321go_options") );
	
	// setting the xkbmap to 'us' if the user wants it
	if(setxkbmap_str!=myxkbmap_str)
	{
		if(system(("setxkbmap "+setxkbmap_str).c_str()) != 0)
		 msg_box("Error", "Keyboard map couldn't be set.");
	}

	// TODO: wait on execvp()??? is this not unuseful?!?!
	printf("start_command: %s\n",start_command.c_str() );

	d321go_execvp( "" , start_command.c_str() );
	
	// restoring the xbmap
	if(setxkbmap_str!=myxkbmap_str)
	{
		if(system(("setxkbmap "+myxkbmap_str).c_str()) != 0)
		 msg_box("Error", "Restoring keyboardmap failed.");
	}
#else*/
	start_cppstring = start_command;

	pthread_t windows_starter;
	pthread_create(&windows_starter, NULL, start_dx, NULL);

#ifndef __LINUX__
	Sleep(1000);
	chdir(old_cwd);
#endif
		
	return true;
}

void start_dedicated_server(string dedicated_cfg, string gamespy_cfg)
{
	OptFile* of = d321go_globals::PlayerOptfile;
	string command;

#ifdef __LINUX__
	command.append("xterm -e ");
#elif __WIN32__
	command.append( MultipleChoiceString::get_current(of->get_single_entry("d3_root","d321go_dirs")));
	command.append("\\");
#endif

	command.append(of->get_single_entry("d3","dx_commands"));

#ifdef __LINUX__
	command.append(" --dedicated ");
	command.append(dedicated_cfg);
	command.append(" --gspyfile ");
	command.append(gamespy_cfg);
#else
	char old_cwd[1024];

#ifndef __WIN32__
	getcwd(old_cwd);
#else
	getcwd(old_cwd, 1024);
#endif

	string ded("\"");
	ded.append(old_cwd);
	ded.append(dedicated_cfg);
	ded.append("\"");

	string gspy("\"");
	gspy.append(old_cwd);
	gspy.append(gamespy_cfg);
	gspy.append("\"");

	command.append(" -dedicated ");
	command.append(ded);
	command.append(" -gspyfile ");
	command.append(gspy);
	command.append("-useport 2098"); // on Windows we can set the port
#endif

	printf("Starting server with this command: %s\n", command.c_str());

#ifdef __LINUX__
	d321go_execvp("" , command.c_str());
#elif __WIN32__
	chdir(MultipleChoiceString::get_current(of->get_single_entry("d3_root","d321go_dirs")).c_str());

	start_cppstring = command;
	
	pthread_t windows_starter;
	pthread_create(&windows_starter, NULL, start_dx, NULL);

	Sleep(1000);

	chdir(old_cwd);
#endif
}

