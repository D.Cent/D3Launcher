/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <sys/stat.h>

#include <odf.h>
using namespace odf;

#include "player_selection.h"

#include "../main/globals.h"

// GUI specific includes for SelectionDialog
#if defined(USE_GTKMM)
	#include "../gui_gtkmm/dialogs.h"
#elif defined(USE_QT)
	#include "../gui_qt/dialogs.h"
#endif

#include "../tools/mc_string.h"
#include "../tools/langfile.h"

namespace g = d321go_globals; // we use d321go_globals very often here, so let's use a shorter name

/**
	compare function for player files (file access comparison)
	@return true if @a s1 is the latest accessed file, otherwise false
*/
bool cmp_plr_files( string s1, string s2 )
{
	struct stat statbuf1, statbuf2;
	t_stat(s1.c_str(), &statbuf1);
	t_stat(s2.c_str(), &statbuf2);
	return (bool) (statbuf1.st_atime > statbuf2.st_atime);
}

// description see header
bool player_selection_dialog( void )
{
	list<string> pilot_files;
		
	char cur_workdir[256];

	if(getcwd(cur_workdir, 256) == NULL)
	 msg_box("Error", "Could not get current working directory.");

	if(chdir(g::D321GOPaths->homepath.c_str()) != 0)
	 msg_box("Error", "Could not change directory");

	get_dir_entries(&pilot_files,".",".plr");
	pilot_files.sort(&cmp_plr_files);

	if(chdir(cur_workdir) != 0)
	 msg_box("Error", "Could not switch back to old working directory.");

	for(list<string>::iterator itr = pilot_files.begin(); itr != pilot_files.end(); itr++)
	 itr->erase(itr->find(".plr"),4);
	pilot_files.push_back("New Pilot");
	
	bool select_player;
	do
	{
		select_player=false;
		SelectionDialog player_dlg("Select a player!", pilot_files);
					
		if(player_dlg.status())
		{
			if(player_dlg.get_selected_item() == "New Pilot")
			{
				string new_name;
				bool overw_ok = false;
				do {
					new_name = input_dlg("Create New Pilot", "Insert Name");
					if(new_name.empty()) {
						select_player = true;
						break;
					}
#ifdef D321GO_DEBUG
					printf("player_selection_dialog(): pilot file: %s\n",
						(g::D321GOPaths->homepath + new_name + ".plr").c_str());
#endif
					overw_ok = !t_access((g::D321GOPaths->homepath + new_name + ".plr").c_str(), F_OK);
					if(!overw_ok)
					{
						overw_ok = question("Pilot already exists",
								"Do you want to replace the old pilot with the new one?");
					}
				} while(!overw_ok);
				if(select_player)
				 break;
#ifdef __LINUX__
				cp((g::D321GOPaths->sharepath + "lnx-sample.plr").c_str(),
#elif __WIN32__
				cp((g::D321GOPaths->sharepath + "win-sample.plr").c_str(),
#endif
				(g::D321GOPaths->homepath + new_name + ".plr").c_str(), false);
				g::player_name = new_name;
#ifdef __LINUX__ // enter location for send recv filter
				OptFile of((g::D321GOPaths->homepath + new_name + ".plr").c_str(), true);
				//of.set_single_entry(, g::D321GOPaths->homepath + "send_recv_filter.so");
				string ld_command = "env LD_PRELOAD=";
				ld_command += g::D321GOPaths->homepath + "send_recv_filter.so";
				of.set_single_entry("d3" , ld_command.c_str(), "dx_prefixes");
#endif
			}
			else
			 g::player_name = player_dlg.get_selected_item();
		}
		else
		 return false;
	} while(select_player);
	
	update_language();
	
	return true;
}

void update_language()
{
	delete g::PlayerOptfile;
	delete g::LanguageOptfile;
	delete g::HelpStrings;
	delete g::Translations;
	
	g::PlayerOptfile = new OptFile((g::D321GOPaths->homepath + g::player_name + ".plr").c_str(), true);
	g::language = MultipleChoiceString::get_current( g::PlayerOptfile->get_single_entry("language","d321go_options"));
	g::LanguageOptfile = new OptFile((g::D321GOPaths->sharepath+"lang/"+g::language + ".lang").c_str(), true, "rb");
	g::HelpStrings = new LangFile(MultipleChoiceString::
		get_current(g::PlayerOptfile->get_single_entry("language","d321go_options")), string(".hlp"));
	g::Translations = new LangFile(MultipleChoiceString::
		get_current(g::PlayerOptfile->get_single_entry("language","d321go_options")), string(".tra"));
}
