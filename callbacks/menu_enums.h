/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

/**
	\file menu_enums.h
	\brief contains menu enumerations for the GUI
*/

#ifndef MENU_ENUMS_H
#define MENU_ENUMS_H MENU_ENUMS_H

enum menu_bar {
	MENU_FILE,
	MENU_START,
	MENU_BROWSER,
	MENU_OPTIONS,
	MENU_EXTRAS,
	MENU_HELP
};

enum file_menu {
	FILE_PILOT = 0,
	FILE_UPDATE,
	FILE_QUIT
};

enum start_menu {
	START_TS = 0
	// GO
};


//	BROWSER_LINKS
	

enum options_menu {
	OPT_D321GO = 0,
	OPT_BROWSER,
	OPT_DIRS,
	//OPT_CHAT,
	OPT_PREFIXES,
	OPT_COMMANDS,
	OPT_POSTFIXES
};

enum extras_menu {
	EXTRAS_LEVEL = 0,
	//EXTRAS_CALENDAR,
	EXTRAS_START_DEDICATED
};

enum help_menu {
	HELP_ABOUT = 0,
	HELP_MANUAL
};

#endif // MENU_ENUMS_H
