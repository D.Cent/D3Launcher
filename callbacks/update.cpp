/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#if defined ( USE_GTKMM )
        #include "../gui_gtkmm/dialogs.h"
#elif defined ( USE_QT )
        #include "../gui_qt/dialogs.h"
#endif

#include <odf.h>
using namespace odf;

#include "../main/version.h"
#include "../main/globals.h"
#include "../tools/langfile.h"
#include "../callbacks/d321go_execvp.h"
using namespace d321go_globals;

enum D321GOUpdateType
{
	UPDATE_UNKNOWN,
	UPDATE_SRC,
	UPDATE_BIN,	// update with package manager - RPM, DEB etc.
	UPDATE_WINBIN
};

D321GOUpdateType get_update_type(void)
{
#if defined( __WIN32__ )	
	return UPDATE_WINBIN;
#else
	list<string> choices;
	choices.push_back("Precompiled Binary");
	choices.push_back("Download Source");

	SelectionDialog update_dlg((Translations->get_help("update_title")).c_str(), choices);

	if(!update_dlg.status())
	 return UPDATE_UNKNOWN;
	else
	 return (update_dlg.get_selected_item() == "Precompiled Binary") ? UPDATE_BIN : UPDATE_SRC;
#endif
}

// TODO: different return values?
bool update_d321go(void)
{
	string srv_version;

	try {
		HTTPClient c("www.odf-online.org");
		if( ! c.GET("/d321go/update.txt", NULL, d321go_globals::D321GOPaths->homepath.c_str()) )
		 throw NEW_ERROR("Could not get update.txt from odf-online.org", EC_CONTINUE);
		
		{
			OptFile of((d321go_globals::D321GOPaths->homepath + "update.txt").c_str(), false);
			srv_version = of.get_single_entry(d321go_globals::version.product.c_str());
		}
		
		remove((d321go_globals::D321GOPaths->homepath + "update.txt").c_str());
		
		if(srv_version == d321go_globals::version.name)
		{
			msg_box((Translations->get_help("update_sorry_title")).c_str(), (Translations->get_help("update_sorry")).c_str());
			return false;
		}
		
	} catch (ErrorClass ec) {
		msg_box("An error occured:", ec.description());
		return false;
	}
	
	if( ! question("D321GO!! - Update", (Translations->get_help("update_question")).c_str()) )
	 return false;

	D321GOUpdateType update_type = get_update_type();

	cout << "Update type: " << update_type << endl;
	
	switch(update_type)
	{
		case UPDATE_SRC:
		{
			char filename[64];
			strcpy(filename, "/d321go/src/");
			strcat(filename, d321go_globals::version.product.c_str());
			strcat(filename, "-");
			strcat(filename, srv_version.c_str());
			strcat(filename, ".tar.gz");		
		
			try
			{
				HTTPClient c("www.odf-online.org");
				bool downloaded=c.GET(filename, NULL, d321go_globals::D321GOPaths->homepath.c_str());
				
				if(! downloaded)
				{
					msg_box("Error!", (Translations->get_help("update_filenotfound")).c_str());
					return false;
				}
			}
			catch(ErrorClass ec)
			{
				msg_box("Network error!", ec.description());
				break;
			}
			
			msg_box("D321GO!! - Update", (Translations->get_help("update_source_downloaded")).c_str());
			
			d321go_execvp("/usr/bin", "xterm"); //TODO: set directory
		}
		break;
		
		case UPDATE_BIN:
		{
			list<string> choices;
			choices.push_back("RPM");
			choices.push_back("DEB");
			SelectionDialog update_dlg("Choose  Type", choices);
			
			if(!update_dlg.status())
			 return false;
			 
			string bin_type = update_dlg.get_selected_item();
			if( bin_type == "RPM" )
			{
				msg_box("RPM Update", (Translations->get_help("update_rpm")).c_str());
			}
			else if(  bin_type == "DEB" )
			{
				bool download=question("DEB Update", (Translations->get_help("update_deb_download")).c_str());
				if(! download)
				 return false;
					
				char filename[64];
				strcpy(filename, "/d321go/debs/");
				strcat(filename, d321go_globals::version.product.c_str());
				strcat(filename, "-");
					
				#if defined(USE_GTKMM)
				 strcat(filename, "gtkmm");
				#elif defined(USE_QT)
				 strcat(filename, "qt");
				#endif
				
				strcat(filename, "_");
				strcat(filename, srv_version.c_str());
				strcat(filename, "-1_i386.deb");					
				
				try
				{
					HTTPClient c("www.odf-online.org");
					bool downloaded=c.GET(filename, NULL, d321go_globals::D321GOPaths->homepath.c_str());
														
					if(! downloaded)
					{
						msg_box("Error!", (Translations->get_help("update_filenotfound")).c_str());
						return false;
					}
				}
				catch(ErrorClass ec)
				{
					msg_box("Network error!", ec.description());
					break;
				}
				
				msg_box("DEB Update", Translations->get_help("update_deb_instructions").c_str());
			}
		}
		break;
		
		case UPDATE_WINBIN:
		{
				char filename[64];
				strcpy(filename, "/d321go/win32-bin/");
				strcat(filename, d321go_globals::version.product.c_str());
				strcat(filename, "-");
					
				#if defined(USE_GTKMM)
				 strcat(filename, "gtkmm");
				#elif defined(USE_QT)
				 strcat(filename, "qt");
				#endif
				
				strcat(filename, "-");
				strcat(filename, srv_version.c_str());
				strcat(filename, ".zip");					
				
				try
				{
					HTTPClient c("www.odf-online.org");
					bool downloaded=c.GET(filename, NULL, d321go_globals::D321GOPaths->homepath.c_str());
														
					if(! downloaded)
					{
						msg_box("Error!", (Translations->get_help("update_filenotfound")).c_str());
						return false;
					}
				}
				catch(ErrorClass ec)
				{
					msg_box("Network error!", ec.description());
					break;
				}
				
				msg_box("Win32 Update", (Translations->get_help("update_win32")).c_str());
		}
		break;
		
		case UPDATE_UNKNOWN: break;
	}
	
	return true;
}
