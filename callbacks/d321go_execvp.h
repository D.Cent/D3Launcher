/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#ifndef D321GO_EXECVP_H
#define D321GO_EXECVP_H D321GO_EXECVP_H

/**
	Starts a new, seperate process
	
	@param directory path to where the binary is ("e.g. /usr/local/bin")
	@param arguments name of the binary and, if needed, arguments
		(e.g. "d321go -v")
	@return process id of the new process, 0 on windows systems
	
	@note linux users can specify an empty string for @a directory
	if it is given in the $PATH enviroment variable.
*/
pid_t d321go_execvp(const char *directory, const char *arguments);

#endif // D321GO_EXECVP_H
