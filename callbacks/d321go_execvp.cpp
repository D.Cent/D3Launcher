/*************************************************************************/
/* D3Launcher, a Vortex-like Descent 3 launcher                          */
/* Copyright (C) 2017-2018                                               */
/* Philipp Lorenz                                                        */
/* Copyright (C) 2007-2011                                               */
/* Johannes Lorenz, Philipp Lorenz                                       */
/*                                                                       */
/* This program is free software; you can redistribute it and/or modify  */
/* it under the terms of the GNU General Public License as published by  */
/* the Free Software Foundation; either version 3 of the License, or (at */
/* your option) any later version.                                       */
/* This program is distributed in the hope that it will be useful, but   */
/* WITHOUT ANY WARRANTY; without even the implied warranty of            */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      */
/* General Public License for more details.                              */
/*                                                                       */
/* You should have received a copy of the GNU General Public License     */
/* along with this program; if not, write to the Free Software           */
/* Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110, USA  */
/*************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
using namespace std;

#include <signal.h>
#include <unistd.h>

#ifdef __LINUX__
 #include <sys/wait.h>
#endif

#ifdef __WIN32__
 #include <windows.h>
#endif

#include "d321go_execvp.h"

#define ERR_QUIT(x) exit(x)

#ifdef D321GO_DEBUG
 #define DEB_MSG
#endif

pid_t d321go_execvp(const char *directory, const char *arguments)
{
#ifdef __WIN32__
	char command[2048];
	strcpy(command, directory);
	strcat(command, arguments);
	
	ShellExecute(NULL, "open", command, NULL, NULL, SW_SHOWNORMAL);
	
	return 0;
#else
	char str1[256], str2[256];
	string path;
	const char *arg_ptr;
	
	int count=3;
	
	pid_t new_pid;
	
	for(arg_ptr=arguments; (arg_ptr=strchr(arg_ptr,' '))!=NULL; arg_ptr++, count++) ; // set count to number of args
	
	//char **array;
	//if((array=(char **) malloc((size_t)((count)*sizeof(char*))))==NULL)
	// ERR_QUIT(62);
	
	//char *(arg_array[128]);
	char** arg_array;
	if((arg_array= (char**) malloc((count*sizeof(char*))))==NULL)
	 ERR_QUIT(62);

#ifdef DEB_MSG
	printf("\nexecvp() gets %d parameters for \"%s\"\n",count, arguments);
#endif

	path+=(directory!=NULL)?directory:"";
	
	for( count=0; ; count++)
	{
	
		if(strchr(arguments,' ')!=NULL)
		 sscanf(arguments,"%s %s", str1, str2);
		else
		 strcpy(str1,arguments);		
#ifdef DEB_MSG
		printf("arguments: arg[%d]=%s\n",count,str1);
#endif
		
		if( (arg_array[count]= (char*) calloc( (size_t) (strlen(str1)+1), sizeof(char))) == NULL)
		 ERR_QUIT(62);
		strcpy(arg_array[count],str1);
		
		arguments+=(strlen(str1));
		if(strncmp(arguments,"\0",1)==0)
		 break;
		arguments+=1;
	
	} /* for */
	
	arg_array[++count]=(char*)NULL;
#ifdef DEB_MSG
	printf("arguments: arg[%d]=(char*)NULL\n\n",count);
#endif
	
	path+=arg_array[0];
	
	new_pid=fork();
	if(new_pid < 0)
	 ERR_QUIT(5);
	if(new_pid == 0)
	{
		fprintf(stderr,"Child process with PID %u for process %s started.\n",getpid(),path.c_str());
		execvp(path.c_str(),arg_array);
		exit(0);
	}
	
	for( ; count>=0; count--)
	 free(arg_array[count]);
	free(arg_array);
	
	return new_pid;
#endif
}

