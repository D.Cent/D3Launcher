TEMPLATE = app
DEPENDPATH += gui_qt network
INCLUDEPATH += gui_qt network

QT+=dbus widgets
DEFINES+= __LINUX__ USE_QT USE_KONVERSATION_PLUGIN
INCLUDEPATH+= ./libodf ./libunzip
LIBS+= -L./libodf -lodf -L./libunzip -lunzip libircclient/libircclient.a
QMAKE_CXXFLAGS+= -std=c++11

# Input
HEADERS += gui_qt/menus.h \
           gui_qt/qd321go.h \
           gui_qt/server_info.h \
           gui_qt/str_funcs.h \
           network/get_servers.h \
           main/globals.h \
           callbacks/start.h \
           callbacks/d321go_execvp.h \
           gui_qt/label_d321go.h \
           gui_qt/dialogs.h \
           gui_qt/options.h \
           main/args.h \
	   network/d3_tracker.h \
	   network/request_servers.h \
	   network/server_structs.h \
	   network/download_level.h \
           gui_qt/tracker_d3.h \
           network/download_mod.h \
           callbacks/update.h \
           network/d3_server.h \
           gui_qt/dedicated.h \
           tools/langfile.h \
           callbacks/player_selection.h \
           network/d321go_server.h \
           libircclient/libircclient.h \
           libircclient/libirc_dcc.h \
           libircclient/libirc_doc_faq.h \
           libircclient/libirc_doc.h \
           libircclient/libirc_errors.h \
           libircclient/libirc_events.h \
           libircclient/libirc_options.h \
           libircclient/libirc_params.h \
           libircclient/libirc_rfcnumeric.h \
           libircclient/libirc_session.h
SOURCES += gui_qt/qd321go.cpp \
           gui_qt/server_info.cpp \
           gui_qt/str_funcs.cpp \
           network/get_servers.cpp \
           main/main.cpp \ 
           callbacks/start_d.cpp \
           callbacks/d321go_execvp.cpp \
           gui_qt/label_d321go.cpp \
           gui_qt/dialogs.cpp \
           gui_qt/options.cpp \
           main/args.cpp \
	   network/d3_tracker.cpp \
	   network/download_level.cpp \
	   network/request_servers.cpp \
           gui_qt/tracker_d3.cpp \
           gui_qt/menus.cpp \
           network/download_mod.cpp \
           callbacks/update.cpp \
           network/d3_server.cpp \
           gui_qt/dedicated.cpp \
           callbacks/player_selection.cpp \
           network/d321go_server.cpp \
           network/server_structs.cpp
