#!/bin/bash

if [ ! -e ./d321go ]; then
	echo "You should build the project before installing it. To do so, type './configure'."
	exit
fi

# check for superuser rights
AMIROOT=`whoami`

if [ $AMIROOT != "root" ]; then
	echo "This script needs to be run as superuser (root). Try 'sudo sh $0'."
	exit
fi

printf "This will install D321GO!! on your system. Continue? (y/n) "
read INSTALL

case $INSTALL in
	y)
	;;
	*)
		echo "Installation aborted ..."
		exit
	;;
esac

if ls /usr/local/share/ODF/d321go >/dev/null 2>/dev/null; then
 printf "It's necessary to uninstall the old data files before the installation. Continue? (y/n) "
 read CONTINUE

 case $CONTINUE in
  y)
	rm -r /usr/local/share/ODF/d321go
  ;;
  *)
	echo "Aborted."
	exit
  ;;
 esac
fi

echo "Creating directory structure... "

mkdir -p /usr/local/share/ODF/d321go

echo "Installing binary..."

cp d321go /usr/local/bin

echo "Installing data..."

cp -R data/* /usr/local/share/ODF/d321go

echo "Registering D3 protocol for KDE and GNOME (if available)..."

if kde4-config >/dev/null 2>/dev/null; then
 cp data/protocols/*.protocol `kde4-config --install services`
fi

if kde-config >/dev/null 2>/dev/null; then
 cp data/protocols/*.protocol `kde-config --install services`
fi

gconftool-2 -s -t string /desktop/gnome/url-handlers/d3/command "d321go -r -i %s" >/dev/null 2>/dev/null
gconftool-2 -s -t bool /desktop/gnome/url-handlers/d3/enabled "true" >/dev/null 2>/dev/null
gconftool-2 -s -t bool /desktop/gnome/url-handlers/d3/needs_terminal "false" >/dev/null 2>/dev/null

echo "Installing libraries..."

if ! ls /usr/lib/libodf.so >/dev/null 2>/dev/null; then
 if ! ls /usr/local/lib/libodf.so >/dev/null 2>/dev/null; then
  cp ../libodf/libodf.so /usr/local/lib
 fi
fi

if ! ls /usr/lib/libunzip.so >/dev/null 2>/dev/null; then
 if ! ls /usr/local/lib/libunzip.so >/dev/null 2>/dev/null; then
  cp libunzip/libunzip.so /usr/local/lib
 fi
fi

echo "All done! You can start D321GO!! by typing 'd321go' now!"

